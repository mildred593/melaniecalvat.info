---
title: L'enfance de Mélanie écrite par elle-même (index)
date: 2022-12-14T22:19:03.365+01:00
cover: ./cover.jpeg
---

Écouter :

<audio controls src="/livres/enfance-de-melanie/audio.webm">
  <em>Impossible de charger le lecteur audio, téléchargez le fichier audio
  ci-dessous</em>
</audio>

Téléchargements :

- [Livre audio](/livres/enfance-de-melanie/audio.webm) ([lien YouTube](https://www.youtube.com/watch?v=iEEi6IwMR7Y))
- [Livre en PDF](/livres/enfance-de-melanie/Vie_de_Melanie_Calvat_bergere_de_la_Salette_ecrite_par_elle-meme_en_1900.large.pdf)
