---
title: Maximin Giraud, témoin de la Belle Dame
cover: /livres/maximin-giraud-par-maurice-canioni/cover.jpeg
weight: 10
---

Petit livre très intéressant écrit par Maurice Canioni et qui retrace la vie de
Maximin et le réhabilite vis à vis des nombreuses calomnies dont il a fait
l'objet.

Contient un résumé de la vie de Maximin avec de nombreuses citations de lui,
ainsi que des annexes avec les secrets de Mélanie et Maximin.

À commander :

- en contactant son auteur&nbsp;: <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#37;&#54;&#53;&#37;&#54;&#52;&#37;&#54;&#57;&#37;&#55;&#52;&#37;&#54;&#57;&#37;&#54;&#102;&#37;&#54;&#101;&#37;&#55;&#51;&#37;&#50;&#101;&#37;&#54;&#49;&#37;&#54;&#53;&#37;&#54;&#100;&#37;&#54;&#51;&#37;&#52;&#48;&#37;&#54;&#55;&#37;&#54;&#100;&#37;&#54;&#49;&#37;&#54;&#57;&#37;&#54;&#99;&#37;&#50;&#101;&#37;&#54;&#51;&#37;&#54;&#102;&#37;&#54;&#100;">editions.aemc<img src="" alt="&commat;" />gmail.com</a>
- [commander en ligne](https://www.lulu.com/shop/maurice-canioni/maximin-giraud/paperback/product-189e75gy.html)
