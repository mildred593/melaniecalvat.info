---
title: Dernières années de sœur Marie de la Croix, bergère de la Salette
cover: /livres/dernieres-annees/cover-tequi.jpeg
weight: 3
---

Ce livre est le journal de l'abbé Combe qui a retranscrit les dernières années
de Mélanie lorsqu'elle était dans sa paroisse à Diou. L'abbé Combe était son
confesseur et a travaillé pour découvrir les talents que Dieu a donné à sa
protégée. Plus que quiconque, il a dévoilé la personnalité de cette sainte et
bien qu'ayant gardé la plus grande discretion pendant sa vie, ce journal nous
permet de mieux la connaître.

Version papier :

- [réédité chez Téqui](https://www.librairietequi.com/A-70570-dernieres-annees-de-soeur-marie-de-la-croix.aspx) (fac similé)
- [réédité chez le Joyeux Pélerin](http://www.chire.fr/A-216613-dernieres-annees-de-soeur-marie-de-la-croix-bergere-de-la-salette-journal-de-l-abbe-combe.aspx) (fac similé de moins bonne qualité que l'édition Téqui)
