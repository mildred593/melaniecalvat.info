---
title: Ma profession de foi sur l'apparition de N.-D. de la Salette (Maximin)
cover: /livres/maximin-ma-profession-de-foi/cover.jpeg
weight: 10
---

Maximin a écrit un livre *Ma profession de foi sur l'apparition de N.-D. de la Salette ou réponse aux attaques dirigées contre la croyance des témoins* dont vous pouvez ici trouver le contenu:

À télécharger :

- [Version PDF](/livres/maximin-ma-profession-de-foi/Ma_profession_de_foi_sur_l_apparition_de.pdf)
