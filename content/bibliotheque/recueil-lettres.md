---
title: Recueil de lettres
cover: /livres/recueil-lettres/cover-amc_100dpi.webp
weight: 5
---

Ce petit recueil de 93 lettres de Mélanie Calvat a été fait dans le but de donner accès à ses écrits en mettant à disposition un ouvrage moins volumineux que ceux déjà existants. Nous avons ainsi voulu les rendre accessibles avec un livre moins imposant, moins onéreux et qui s'adresserait à un public plus large.

À lire en version papier :

- [À commander chez l'imprimeur](https://www.thebookedition.com/fr/recueil-de-lettres-p-401540.html)
