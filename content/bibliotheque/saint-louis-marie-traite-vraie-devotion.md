---
title: Traité de la Vraie dévotion à la Sainte Vierge - Saint Louis Marie Grignon de Monfort
cover: /livres/saint-louis-marie-traite-vraie-devotion/cover.jpeg
weight: 100
---

Ce petit livre de Saint-Louis Marie Grignon de Montfort est en quelque sorte,
avec le fascicule « Le secret de Marie », *le manuel de l’Apôtre des derniers
temps* appelé par Notre-Dame dans la partie de « l’appel » qu’Elle donne vers la
fin du secret à Mélanie Calvat. Sa lecture nous paraît indispensable à une bonne
compréhension de l’intention de la Sainte Vierge à La Salette concernant ce
nouvel ordre religieux ainsi que du rôle essentiel donné à Mélanie Calvat en
tant que sa messagère privilégiée.

En version papier :

- [Aux éditions Saint-Rémi](https://saint-remi.fr/fr/mariologie/1018-trait-de-la-vraie-dvotion-la-sainte-vierge-9782816200157.html)
- [Chez Chiré éditions Mediaspaul](http://www.chire.fr/A-129467-traite-de-la-vraie-devotion-a-la-sainte-vierge.aspx) ou [aux éditions du Seuil](http://www.chire.fr/A-153125-traite-de-la-vraie-devotion-a-la-sainte-vierge.aspx)

À télécharger

- [PDF des A-C-R-F](/livres/saint-louis-marie-traite-vraie-devotion/St_LOUIS-MARIE_GRIGNION-Traite_vraie_devotion.pdf)

Du même auteur :

- Amour de la Divine Sagesse : [en PDF](/livres/saint-louis-marie-traite-vraie-devotion/Amour_de_la_divine_Sagesse_000001216.pdf)
- Les amis de la Croix
- le secret admirable du très saint Rosaire : [aux éditions ESR](https://saint-remi.fr/fr/piete-meditation/2241-le-secret-admirable-du-tres-saint-rosaire-9782816205824.html), [chez Chiré](https://www.chire.fr/le-secret-admirable-du-tres-saint-rosaire-p-217688)
- le secret de Marie : [aux éditions ESR](https://saint-remi.fr/fr/mariologie/1019-le-secret-de-marie-sur-lesclavage-de-la-sainte-vierge-9782816200164.html)
