---
title: Lettres de Liaison de Cap Fatima sur la Salette
weight: 4
#cover: /fatima100/logo2.jpg
---

Le site [Cap Fatima](https://www.fatima100.fr/) a fait toute une étude sur la
Salette dont voici les principales lettres les plus intéressantes:

- [lettre n°86 : L'apparition de La Salette](/fatima100/lettre_de_liaison_86-.pdf) 
- [lettre n°87 : Comment faire des sacrifices](/fatima100/lettre_de_liaison_87.pdf) 
- [lettre n°88 : Le message de La Salette](/fatima100/lettre_de_liaison_88.pdf) 
- [lettre n°89 : Le secret de La Salette](/fatima100/lettre_de_liaison_89.pdf) 
- [lettre n°90 : Le secret de La Salette a-t-il été condamné ?](/fatima100/lettre_de_liaison_90.pdf) 
- [lettre n°91 : La Salette et les tribulations de l'Eglise](/fatima100/lettre_de_liaison_91.pdf) 
- [lettre n°92 : Réparer les offenses faites à Notre-Seigneur](/fatima100/lettre_de_liaison_92.pdf) 
- [lettre n°93 : Mélanie est-elle folle ou menteuse ?](/fatima100/lettre_de_liaison_93.pdf) 
- [lettre n°94 : La Salette et l'incendie de Notre-Dame de Paris](/fatima100/lettre_de_liaison_94.pdf) 
