---
title: Journal d'une institutrice par mademoiselle des Brulais
cover: /livres/journal-d-une-institutrine-mlle-des-brulais/cover-bnf.jpeg
weight: 3
---

Mélanie disait que tout avait été écrit d'elle au moment de l'apparition par
Mademoiselle des Brulais. Ses écrits sont d'abord parus dans « L'Écho de la
Sainte Montagne » en 1852 puis en 1855. Plus tard certains passages de ces
écrits furent rassemblés dans le livre « Journal d'une institutrice ».

- L'Écho de la Sainte Montagne - 1852 : [PDF Google, première édition de 1852](/livres/journal-d-une-institutrine-mlle-des-brulais/L_écho_de_la_sainte_montagne-1852-google.pdf) ou [troisième édition de 1854](/livres/journal-d-une-institutrine-mlle-des-brulais/L_écho_de_la_sainte_montagne_visitée_p-1854-google.pdf)
- Suite de L'Écho de la Sainte Montagne - 1855 : [PDF Gallica de 1855](/livres/journal-d-une-institutrine-mlle-des-brulais/Suite_de_L'écho_de_la-Des_Brûlais_bpt6k6531431g.pdf), [Google](/livres/journal-d-une-institutrine-mlle-des-brulais/L_écho_de_la_sainte_montagne-1855-google.pdf), [Gallica, réédité en 1904](/livres/journal-d-une-institutrine-mlle-des-brulais/L'écho_de_la_sainte_montagne-Des_Brûlais_bpt6k65339891.pdf)

---

Un ouvrage plus récent reprenant des anecdotes et des extraits des écrits
originaux intitulé « Journal d'une institutrice » aux Nouvelles Éditions Latines
se trouve [chez Chiré](http://www.chire.fr/A-116961-la-salette-1847-1855-journal-d-une-institutrice.aspx) et d'occasion assez facilement.

