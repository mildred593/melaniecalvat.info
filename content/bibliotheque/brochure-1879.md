---
title: L’apparition de la Très Sainte Vierge sur la montagne de La Salette le 19 septembre 1846
cover: /livres/brochure-1879/cover.webp
weight: 1
---

Voici la brochune originale telle qu'elle a été imprimée en 1879 et qui a reçu
l'imprimatur de Mgr Zola, évêque de Lecce. Elle est reprise entièrement sur la
page dédiée à [l'apparition]({{< relref "/apparition" >}})

<center>{{< imgresize "/livres/brochure-1922/imprimatur2.jpeg" "300x" >}}</center>

Á télécharger :

- [Brochure PDF de 1879](/livres/brochure-1879/brochure.pdf)
- [Brochure de 1922](/livres/brochure-1922/Secret%20de%20La%20Salette%20-%20Zola%20Lepidi%202.pdf) avec l'imprimatur supplémentaire du R. P. A. Lepidi, maître du sacré palais et assistant perpétuel de la congrégation de l'Index.
- [Brochure mise en forme au format A5](/apparition/livret%20apparition%20FR_A5.pdf) et une [version A4 paysage à plier pour en faire un livret](/apparition/livret%20apparition%20FR_A4_booklet.pdf)
