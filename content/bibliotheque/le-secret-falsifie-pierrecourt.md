---
title: Le secret de Mélanie falsifié de Paul-Étienne Pierrecourt
cover: /livres/le-secret-falsifie-pierrecourt/cover.jpeg
mediatypes:
- autres_livres
weight: 20
---

Livre réfutant les faux secrets de manière magistrale par plusieurs arguments
imparables même si tout le monde ne s'accordera pas sur les différents
développements présentés en annexes.

En version papier :

- [sur le site des éditions Saint-Rémi,
    l'éditeur](https://saint-remi.fr/fr/propheties/2243-le-secret-de-melanie-falsifie-preuves-que-le-texte-date-de-1851-est-un-faux-9782958114596.html)

Voir également [la vidéo de présentation](https://www.youtube.com/watch?v=jL2AV5C-tos) de l'auteur.
