---
title: L'enfance de Mélanie écrite par elle-même
cover: /livres/enfance-de-melanie/cover-amc_100dpi.webp
weight: 2
---

Ceci est l'histoire de Mélanie écrite par elle-même sur ordre de l'abbé Combe
qui a voulu transmettre ce trésor de sainteté aux futurs chrétiens. Ce devoir
d'obéissance lui a coûté mais nous permet de découvrir quelle sainteté se cache
derière cette humble fille aimée du Bon Dieu.

Version papier :

- [auprès de notre association, expédié par nos soins](https://buy.stripe.com/eVa3eIeuk7bEcxO7st)
- À commander [chez Chiré](https://www.chire.fr/lenfance-de-melanie-ecrite-par-ellememe-p-284729) et en vente à [l'association de la maison de Mélanie](http://melaniecalvat.org/galerie.asp?ref_cat=49&lg=fr) à Corps (15km avant le sanctuaire de la Salette).


À écouter :

<audio controls src="/livres/enfance-de-melanie/audio.webm">
  <em>Impossible de charger le lecteur audio, téléchargez le fichier audio
  ci-dessous</em>
</audio>

Téléchargements :

- [Livre audio](/livres/enfance-de-melanie/audio.webm) ([lien YouTube](https://www.youtube.com/watch?v=iEEi6IwMR7Y))
- [Livre en PDF](/livres/enfance-de-melanie/vie_de_Mélanie_pour_impression.pdf)
- [À lire en ligne sur WikiSource](https://fr.wikisource.org/wiki/Vie_de_M%C3%A9lanie%2C_berg%C3%A8re_de_la_Salette) ([version PDF](/livres/enfance-de-melanie/Vie_de_Melanie_Calvat_bergere_de_la_Salette_ecrite_par_elle-meme_en_1900.large.pdf))
