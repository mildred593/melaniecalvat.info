---
title: Défense et explications du secret par Amédée Nicolas
cover: /livres/defense-secret-amedee-nicolas/cover.jpeg
related:
  - /bibliotheque/la-femme-et-le-dragon-maurice-canioni
weight: 3
---

Défense et explication du Secret de la Salette publié en novembre 1879 par la
bergère Mélanie Calvat Sœur Marie de la Croix, par Amédée Nicolas, avocat, avec
deux lettres de Mgr Sauveur Louis Zola évêque de Lecce. Nimes 1880.

Télécharger :

- [PDF Gallica](/livres/defense-secret-amedee-nicolas/Défense_et_explication_du_secret_...Nicolas_Amédée_bpt6k56839792.pdf)

Note&nbsp;: le texte se trouve aussi dans le livre
«&nbsp;[La Femme et le dragon](/bibliotheque/la-femme-et-le-dragon-maurice-canioni/)&nbsp;»
de Maurice Canioni, édité en format papier.

