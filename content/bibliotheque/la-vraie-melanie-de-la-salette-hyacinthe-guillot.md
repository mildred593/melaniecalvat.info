---
title: La vraie Mélanie de la Salette de Hyacinthe Guillot
cover: /livres/la-vraie-melanie-de-la-salette-hyacinthe-guillot/cover.jpeg
mediatypes:
- autres_livres
weight: 10
---

Un livre de référence, mais domage que la dernière page de l'éditon révisée soit
à jeter au feu car elle reprend le faux secret.

En version papier :

- [chez Pierre Téqui, l'éditeur](https://www.librairietequi.com/A-5290-la-vraie-melanie-de-la-salette.aspx)
- [chez Chiré](https://www.chire.fr/la-vraie-melanie-de-la-salette-p-119118)
