---
title: La Femme et le dragon
cover: /livres/la-femme-et-le-dragon-maurice-canioni/cover.jpeg
mediatypes:
  - autres_livres
related:
  - /bibliotheque/defense-secret-amedee-nicolas
weight: 30
---

Livre dense de 500 pages reprenant [la défense du secret de Amédée Nicolas](/bibliotheque/defense-secret-amedee-nicolas/).

> Malheur aux prêtres et aux Princes de l’Eglise ! Rome perdra la foi et deviendra le siège de l’Antéchrist. L’Eglise sera éclipsée (par  Vatican II revêtue des apparences et des apparats de la véritable Eglise). Depuis leur première divulgation en 1860, ces prophéties n’ont cessé d’être l’objet des attaques du clergé, malgré le soutien de 6 papes, de Pie IX à Pie XII. Les adeptes de Vatican II les ont dénaturées et censurées. À son tour, la « famille de la Tradition », se déjugeant en niant leur réalisation sous nos yeux, crache sur elles son venin. Ces gens prouvent ainsi qu’ils sont visés par le blâme de l’Oracle divin. Riche de documents irréfutables, ce livre détruit magistralement et leur mensonge selon lequel le contenu du Secret de La Salette a été condamné par Rome et les odieuses calomnies à l’égard de la messagère choisie par Notre-Dame. Cette guerre à la Vierge de La Salette est suscitée et menée par Satan. À lire absolument pour ne pas être dévoyé par des faux-frères ensoutanés ou non.

À commander :

- en contactant son auteur&nbsp;: <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#37;&#54;&#53;&#37;&#54;&#52;&#37;&#54;&#57;&#37;&#55;&#52;&#37;&#54;&#57;&#37;&#54;&#102;&#37;&#54;&#101;&#37;&#55;&#51;&#37;&#50;&#101;&#37;&#54;&#49;&#37;&#54;&#53;&#37;&#54;&#100;&#37;&#54;&#51;&#37;&#52;&#48;&#37;&#54;&#55;&#37;&#54;&#100;&#37;&#54;&#49;&#37;&#54;&#57;&#37;&#54;&#99;&#37;&#50;&#101;&#37;&#54;&#51;&#37;&#54;&#102;&#37;&#54;&#100;">editions.aemc<img src="" alt="&commat;" />gmail.com</a>
- [commander en ligne](https://www.lulu.com/shop/maurice-canioni/la-femme-et-le-dragon/paperback/product-1q27gepg.html)
