---
title: Mémoires d'une ex palladiste de Diana Vaughan
cover: /livres/vaughan-memoires-ex-palladiste/photo.jpeg
weight: 100
---

Mélanie n'a jamais douté de la réelle existance de Diana Vaughan et son livre
mérite d'être lu.

> « — Mais pouvez-vous dire qu’on a changé Léo Taxil ? Pouvez-vous dire que les méchancetés des catholiques sont cause qu’il est retourné à la Franc-maçonnerie ? Comment ne voyez-vous pas qu’il s’est toujours moqué des catholiques ? Lui-même l’a dit en 1897, quand on le somma de produire sa Diana Vaughan, il leur a ri au nez. \
> — Diana Vaughan, mon Père, n’est pas un mythe. La courageuse femme qui avait confiance en lui, ne sachant pas qu’il était redevenu mauvais, se rendit réellement à Paris, et il la livra. \
> — Qu’est-ce que vous me racontez ! Vous l’avez vu la livrer ? \
> — Oui, mon Père. La nuit il est allé la chercher à la gare ; en route il lui dit : « J’ai des précautions à vous indiquer, entrons dans cette maison. »
> Quand elle mit le pied dans la première chambre à gauche, elle tomba dans une
> trappe. \
> — Alors, c’est plus qu’une fripouille, c’est un assassin ! \
> — Il ne l’a pas assassinée. Il fut payé pour la livrer, et on lui avait dit qu’on se contenterait de l’emprisonner. \
> — S’est-on borné à la séquestrer ? \
> — Les palladistes l’ont fait souffrir, ô ! Combien, mais celle-là n’apostasiera pas ! \
> — Vous avez vu tout cela ? \
> — Je l’ai vu se faire. »
>
> <cite>Mémoires de l'abbé Combe (p. 178, jeudi 25 juin 1903)</cite>

<!-- {{< youtube wV4gfSMZ9tc >}} -->

En version papier :

- [Sur LIESI des éditions Delacroix](https://www.liesi-delacroix.eu/revolution-et-nouvel-ordre-mondial/les-societes-secretes/les-societes-secretes-dans-l-histoire/revelations-dune-pretresse-luciferienne/memoires-d-une-ex-palladiste-diana-vaughan)
- [Aux Éditions Saint-Rémi](https://saint-remi.fr/fr/demonologie/1056-memoires-d-une-ex-palladiste-9782816201017.html), cette édition comprend également le témoignage de l'éditeur sur la véracité du personnage de Diana Vaughan, absent du fac-similé PDF de la BNF comme l'éxplique [Paul-Étienne Pierrecourt dans une vidéo de 2018](https://www.youtube.com/watch?v=wV4gfSMZ9tc).

À télécharger :

- [Version PDF, sans les explications de l'éditeur](/livres/vaughan-memoires-ex-palladiste/Vaughan%20Diana%20-%20Mémoires%20d'une%20ex-palladiste.pdf)
- [Consulter et contribuer sur WikiSource](https://fr.wikisource.org/wiki/M%C3%A9moires_d%E2%80%99une_ex-palladiste_parfaite,_initi%C3%A9e,_ind%C3%A9pendante)

