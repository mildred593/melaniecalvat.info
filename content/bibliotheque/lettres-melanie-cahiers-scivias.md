---
title: Lettres de Mélanie dans les cahiers Scivias
weight: 5
---

Les cahiers Scivias ont receuilli les lettres de Mélanie et elles sont
disponnibles en deux tomes :

- Lettres de Mélanie Calvat à la soeur St Jean, aux abbés Le Baillif, Roubaud et Combe (bientôt republié aux éditions ESR ou [chez Nelligan au Canada](https://www.librairienelligan.com/store/modules/lucidpayment/prod_details.php?id=2126&cid=80))
- Lettres de Mélanie Calvat au Chanoine de Brandt - T 2 (bientôt republié aux éditions ESR ou [chez Nelligan au Canada](https://www.librairienelligan.com/store//modules/lucidpayment/prod_details.php?id=651&cid=80))
