---
title: Prière à réciter en temps de calamités
---

Père Éternel, voici votre Fils Jésus-Christ mis en croix pour nous ! 
En son nom et par ses mérites, ayez pitié de nous, pauvres pécheurs,
parce que repentants nous recourons à votre infnie miséricorde.
Laissez-vous toucher, ayez pitié de nous qui sommes son héritage. Ne violez pas, Seigneur, le pacte que vous avez fait : d’exaucer la prière que vous font vos enfants. Il est vrai que par nos grandes iniquités nous avons
irrité votre Justice, mais vous, mon Dieu, qui êtes bon par nature, faites resplendir la grandeur de votre infnie miséricorde.
Seigneur, si vous voulez faire attention à nos iniquités, qui pourra subsister en votre présence ? Seigneur, nous confessons que nous sommes très coupables et que ce sont nos péchés qui ont attiré ces féaux sur nous. Mais vous, Seigneur, qui avez bien voulu que tous les jours nous vous appelions Notre Père,regardez à présent la grande afliction de vos enfants, et épargnez de si grands féaux. Oh ! faites grâce ; ô mon Dieu,par les mérites de Jésus-Christ, faites grâce par l’amour que vous avez pour vous-même ; par l’amour de la Vierge Marie, «notre Maman»,
pardonnez-nous !

Souvenez-vous, ô Seigneur, que nous sommes appelés votre peuple, ayez pitié de la folie humaine. Envoyez un rayon de votre divine lumière qui dissipe les ténèbres de notre intelligence et que notre âme amendée change ses voies et ne sature plus d’amertume le cœur de son Dieu !
Seigneur, la main seule de votre infnie miséricorde peut nous sauver de tant de féaux. Seigneur, nous sommes enivrés d’afictions intérieures et extérieures, ayez pitié de nous ! Détournez, ô Seigneur votre face de nos péchés et regardez Jésus-Christ qui vous a donné satisfaction en soufrant et en mourant pour nous,il est votre Fils ! Et ainsi nous célébrerons votre infnie miséricorde.
Vite, exaucez-nous, Seigneur, autrement notre courage sera bien amoindri, car nous sommes tombés dans un état si misérable ! Vite Seigneur,faites sentir votre miséricorde, car nous n’espérons plusqu’en vous seul, qui êtes notre Père, notre Créateur, et qui devez conserver et sauver ceux qui sont vôtres pour toujours.

Sœur Marie de la Croix, née Mélanie Calvat, Bergère de la Salette.

Prière ayant bénéfciée d’une indulgence partielle de 40 jours par l’évêque d’Altamura,
le 8 septembre 1905.

