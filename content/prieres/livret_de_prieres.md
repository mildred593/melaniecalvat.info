---
title: Livret de prières
cover: /livres/livret-de-prieres/cover.jpeg
weight: 1
---

Voici un petit livret de prières essentielles à Notre-Dame de la Salette et de Mélanie Calvat. Il est disponible à l'achat à Corps à la maison natale de Mélanie, et vous pouvez l'imprimer vous-même.

- [Document à imprimer sur papier A4](/livres/livret-de-prieres/livret_pour_impression.pdf). Ce document peut être imprimé sur une imprimante de bureau au format A4 et ensuite plié pour en faire un livret. Vous devez l'imprimer en recto-verso avec l'option «bord court - retourné».
- [Document A5](/livres/livret-de-prieres/livret_de_prieres_V0100223-148x210.pdf)

Ce livret de prière contient :

- une neuvaine à Notre-Dame de la Salette
- la prière du «souvenez-vous», indulgenciée
- les invocations à Notre-Dame de la Salette
- les litanies en l'honneur de Notre-Dame de la Salette, indulgenciées
- une prière indulgenciée à réciter en temps de calamités composée par Mélanie
  Calvat
- le chapelet à Notre-Dame de la Salette
- les litanies de l'amour de la Sainte Vierge
- la prière au Père Éternel pour apaiser sa divine justice
- le psaume 125
- la prière à Jésus-Christ pour une bonne mort
- la prière à Notre Seigneur Jésus-Christ
- prière à Jésus
- prière à Marie notre Mère en temps d'affliction générale
- prière à Marie notre douce Mère


