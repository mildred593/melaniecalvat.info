---
title: Mélanie a-t-elle menti ?
date: 2022-12-14T19:13:48.713+01:00
---

Certains voient en Mélanie une personne instable, illuminée ou affabulatrice,
mais il n'en est rien. Au contraire Mélanie est une véritable Sainte quoi que
non encore reconnue par l'Église. Mais comment en avoir la conviction face à
tout ce qu'on raconte sur elle ? Il est important de chercher la vérité.

Ceux qui prétendent que Mélanie est une folle ou une affabulatrice n’ont jamais pu en apporter la démonstration de façon convaincante. Le plus souvent, ils connaissent mal sa vie et se contentent d’affirmations péremptoires sans vraiment de preuves à l’appui.

Au contraire ceux qui la connaissaient ont un avis tout autre, et on dispose de
nombreuses confirmations dans ce sens car nous disposons de beaucoup de ses
lettres et d'écrits de ses différents confesseurs.

<!--more-->

Hyacinthe Guilhot, dans son remarquable livre *La vraie Mélanie de La Salette*
conclut sur ce point de la façon suivante :

> On l’a accusée de mensonge. Or, ni son tempérament, ni son caractère, ni le
> ton de ses lettres, ni la mission surnaturelle dont elle a été investie
> n'autorisent l'ombre même d'une semblable accusation.
>
> On l'a accusée de déséquilibre mental, voire d'hystérie. Or, d'après de
> nombreux témoins, il y avait dans son langage, comme dans sa conduite et dans
> sa volumineuse correspondance, trop de bon sens, trop de logique, trop
> d'équilibre et trop de maîtrise de soi pour qu'on puisse, honnêtement,
> s'arrêter à une telle appréciation.

Le Ciel semble avoir répondu lui-même aux prélats qui étaient opposés à La Salette. Onze sont morts de façon subite et souvent dans des conditions tragiques, voir le détail dans [la lettre de liaison n°93]. Au contraire les témoignages en faveur de Mélanie proviennent de personneés édifiantes (certains sont canonisés et d'autres n'y sont pas loin).

Mais ce qui achèvera de vous convaincre, c'ést le récit de son enfance que vous
pourrez trouver dans la partie livres et documents de ce site.

Tiré en partie de [la lettre de liaison n°93] de [Cap Fatima](https://www.fatima100.fr).

[la lettre de liaison n°93]: /fatima100/lettre_de_liaison_93.pdf
