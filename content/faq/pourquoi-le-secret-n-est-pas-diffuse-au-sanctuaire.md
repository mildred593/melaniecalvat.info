---
title: Pourquoi le secret n'est-il pas diffusé au sanctuaire ?
---

Le clergé a pendant longtemps été hostile au secret, en particulier le clergé
visé par la Sainte Vierge. Il faut savoir que le clergé français du temps de
Mélanie était nommé par le pouvoir politique hostile à l'Église et que les
avertissements du Ciel ne leur plaisaient pas. la rumeur s'est installée comme
quoi le secret était condamné, ce qui est faux, et il n'a pas été diffusé.

> Les prêtres, ministres de mon Fils, les prêtres, par leur mauvaise vie, par leurs irrévérences et leur impiété à célébrer les saints mystères, par l’amour de l’argent, l’amour de l’honneur et des plaisirs, les prêtres sont devenus des cloaques d’impureté. Oui, les prêtres demandent vengeance, et la vengeance est suspendue sur leurs têtes. Malheur aux prêtres, et aux personnes consacrées à Dieu, lesquelles, par leurs infidélités et leur mauvaise vie, crucifient de nouveau mon Fils ! Les pêchés des personnes consacrées à Dieu crient vers le Ciel et appellent la vengeance, et voilà que la vengeance est à leurs portes ; car il ne se trouve plus personne pour implorer miséricorde et pardon pour le peuple ; il n’y a plus d’âmes généreuses, il n’y a plus personne digne d’offrir la Victime sans tache à l’Éternel en faveur du monde.
>
> <cite>Notre-Dame le 19 septembre 1846</cite>

<!--more-->
