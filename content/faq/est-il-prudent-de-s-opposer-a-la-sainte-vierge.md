---
title: Est-il prudent de s'opposer à notre-Dame et son secret ?
---

Le Ciel nous prouve par la mort spectaculaire des prélats qui se sont opposés à la Salette que ce n'est pas prudent.

<!--more-->

---

<!-- TODO: retranscrire -->

Le Ciel a répondu aux ecclésiastiques qui se sont opposés au message de Mélanie.
Bon nombre d'entre eux ont rendu compte de leur opposition frontale à la Très
Sainte Vierge. Citons par exemple :

1. Monseigneur Ginoulhiac, évêque de Grenoble et successeur de Mgr de Bruillard,
   fut le premier à persécuter Mélanie et maximin au sujet de leurs secrets. Il
   espérait la faveur du pouvoir politique et un archevêcher avec un chapeeau de
   cardinal, et pour plaire à l'empereur, il déclara que la mission des voyants
   était terminée. Par ailleurs il traita Mélanie de folle. Il est mort fou,
   jouant à la poupée et avec ses excréments...

2. Mgr Fava, évêque de Grenoble et successeur de Mgr Ginoulhiac a prétendu
   imposer sa règle de préférence à celle donnée par la Très Sainte Vierge à
   Mélanie. En inaugurant les bureaux de la Croix de l'Isère, il y installa
   Notre-Dame de Lourdes, méconnaissant de nouveau, vu les circonstances, la
   faveur que la Très Sainte Vierge avait faite à son diocèse. La soirée fut
   joyeuse dans les bureaux et il se retira tard. Le lendemain il fut retrouvé
   mort sur son plancher, dévêtu, les bras tordus, les poings crispés, les yeux,
   le visage, exprimant l'effroi d'une horrible vision.

Suite dans le document [la réponse du Ciel aux détracteurs de Mélanie Calvat](/bibliotheque/réponse%20du%20ciel%20aux%20détracteurs%20de%20Mélanie%20Calvat.pdf)

---

### La mission de Mélanie : faire accepter le message de la Sainte Vierge par le clergé

Face a toutes ces « manœuvres ecclésiastiques » pour discréditer le message de
Notre-Dame à la Salette, Mélanie a mis toute son énergie pour faire accepter au
contraire par le clergé...

D;es son enfance, le Ciel signifie à Mélanie Calvat que sa mission concerne
**directement le chergé**. Dans son récit autobiographique, Mélanie nous
rapporte que, lors d'une des visions qu'elle eut :

> La très grande reine et impératrice Marie, Vierge Mère de Dieu, (parut) toute
> resplandisance de gloire et de majesté, vêtue et revêtue d'amour !... qui avec
> une ineffable bonté et douceur me dit :
>
> « — ma fille, la grande miséricorde de Dieu est avec vous, je veillerai sur
> vous comme Mère et Maîtresse, ne craignez rien lorsque, avec droite intention,
> l'œil de votre âme sera appliqué pour remplir le désir de Dieu. Il faut, unit
> aux mérites de Jésus-Christ, vous offrir continuellement pour l'exaltation de
> la sainte Église et surtout pour le clergé. »

