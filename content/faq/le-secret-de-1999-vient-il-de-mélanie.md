---
title: Le secret de 1999 découvert par Michel Corteville vient-il de Mélanie ?
date: 2022-12-14T19:19:36.822+01:00
related:
  - /bibliotheque/le-secret-falsifie-pierrecourt
---

Le secret découvert en 1999 n'est pas le secret de Mélanie tel que la Sainte
Vierge l'a dit.

Mélanie connaissait le secret par cœur par un don divin. Elle l'a reçu en
français alors qu'elle ne connaissait que son patois, et a pu le restituer
toujours avec la même exactitude. Son secret n'a jamais varié contrairement a ce
que certains ont voulu insinuer.

De plus les preuves matérielles concernant le format du secret, sa longueur, le
nombre de pages et l'écriture est incohérent sur le manuscrit de 1999 qui ne
correspond en rien aux différentes rédactions du secret de l'époque de Mélanie
attesté par différents témoins.

Voir à ce sujet le livre de Paul-Étienne Pierrecourt « Le secret de Mélanie falsifié - Preuves que le texte daté de 1851 est un faux et que Michel Corteville a été berné ».

<!--more-->

