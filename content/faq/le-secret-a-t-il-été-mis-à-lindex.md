---
title: Le secret a-t-il été mis à l'index ?
date: 2022-12-14T19:13:13.650+01:00
---

Le secret a beaucoup été critiqué. en effet, le secret en lui même critique les
mauvais prêlats, et ceux-ci n'ont pas attendu pour dénigrer ce secret qui leur
faisait de l'ombre. Plusieurs tentatives ont eu lieu afin de le discréditer, les
ennemis du secret ne se lassant pas de chercher à le cacher.

1. Une lettre non officielle et obtenue sous contrainte du secrétaire de la
   congrégation de l'Inquisition (l'Index se refusant à condamner) expliquant
   sont déplaisir face à cette brochure et enjoignant à la retirer des mains des
   fidèles tout en la gardant comme lecture édifiante pour le clergé.

2. Certains écrits de l'abbé Combe (*Le grand coup* et *Le secret de Mélanie,
   bergère de la Salette, et la crise actuelle*) furent mis à l'Index et
   certains en profitèrent pour dire que le secret lui même était à l'Index, or
   il n'en était rien.

3. Le décret du Saint-Office du 21 décembre 1915 ne porte la signature d'aucun
   des cardinaux dignitaires ou membres du Saint Office, mais uniquement celle
   de son notaire Louis Castellano. Il s'oppose au secret en demandant aux
   fidèles de s'abstenir de discuter de celui-ci mais ne condamne nullement la
   brochure ni n'interdit sa lecture ou sa diffusion.

4. Le décret du 9 mai 1923 fait suite à une réimpression l'année précédante de
   la brochure de 1878, ayant reçu imprimatur le 6 juin 1922 du R.P. A. Lépidi,
   O.P., Maître du Sacré-Palais, assistant perpétuel de la Congrégation de
   l’Index. Le décret de condamnation comprend plusieurs mentions qui rendent
   invalide son interprétation concernant la brochure republiée l'année
   précédente :

    - Le titre de la brochure condamné est différent. La brochure condamnée
      parle de l'apparition du *19 septembre 1845* alors que l'apparition était
      le *19 septembre 1846* comme l'indiquait son titre.
    - La condamnation ne fait pas mention de l'imprimatur du 6 juin 1922

   Cette condamnation s'applique en réalité à une brochure différente imprimée
   par le docteur Grémillon à 1000 exemplaires, au même format que la brochure
   de 1922 mais en ajoutant une douzaines de pages d'absurdités.

5. La lettre du cardinal Pizzardo en 1957 tenta de faire passer le décret de
   1923 pour une condamnation du secret mais sans en apporter aucune preuve.


<!--more-->

### Sur la brochure

Le 20 février 1878, Léon XIII fut élu au pontificat. Mélanie rédigea alors une brochure intitulée *L’apparition de la Très Sainte Vierge sur la montagne de La Salette le 19 septembre 1846* dans laquelle elle racontait en détail l’apparition de La Salette. Elle y inséra une version complète du secret (la dernière qu’elle rédigea), exception faite de la règle de l’Ordre de la Mère de Dieu. La brochure fut achevée le 21 novembre 1878 et envoyée au Vatican.

Le 3 décembre 1878, neuf mois après son accession au pontificat, Léon XIII reçut Mélanie en audience privée. Le pape aurait pu saisir l’occasion pour exprimer un profond désaccord sur le secret et ordonner à Mélanie de se taire. Il fit exactement le contraire : il lui demanda d’aller à La Salette pour y diffuser le message et fonder l’ordre de la Mère de Dieu.

Enfin, le 15 novembre 1879, Mgr Zola, l’ancien confesseur de Mélanie devenu évêque de Lecce, accorda son imprimatur à la brochure que Mélanie avait achevée de rédiger l’année précédente.

Mgr Cortet, évêque de Troyes, a vainement tenté de faire mettre à l'index cette brochure mais la sacré congrégation de l'Index s'est révélé incompétente *« C'est une question de fait [et non de doctrine] de savoir si oui ou non le clergé et les ordres religieux sont aussi corrompus. »* et il fut renvoyé vers la congrégation de l'Inquisition. L'Index ne pouvait en effet se déjuger car il avait déjà examiné la brochure quelques mois avant son impression, et la congrégation des Rites avait également approuvé les constitutions de l'ordre de la Mère de Dieu écrites par Mélanie.

Suite à l'insistance de Mgr Cortet et ses menaces de retirer son diocèse du
Denier de Saint-Pierre, il finit par obtenir une réponse du secrétaire de la
congrégation de l'Inquisition :

> Révérendissime Seigneur,
>
> La Sacrée Congrégation de l'Inquisition a reçu de la Congrégation de l'Index les lettres de votre Grandeur relatives à l'opuscule intitulé : L'Apparition de la Très Sainte Vierge sur la montagne de la Salette. Les très Éminents Cardinaux avec moi Inquisiteurs Généraux de la Foi, jugent digne des plus grands éloges le zèle que vous avez déployé en leur dénonçant cet opuscule. Ils veulent que vous sachiez que le Saint-Siège a vu, avec le plus grand déplaisir, la publication qui en a été faite et que sa volonté expresse est que les exemplaires répandus déjà parmi les fidèles soient retirés de leurs mains partout où la chose sera possible, mais maintenez-le entre les mains du clergé pour qu’il en profite.

Or la phrase *mais maintenez-le entre les mains du clergé pour qu’il en profite*
a été tronquée dans les publications dans la presse car elle indique que le
secret en lui même n'est pas condamné.

De plus cette lettre n'émanne pas du préfêt de la congrégation (le Pape en
personne) mais de son secrétaire, sur du papier ordinaire ne portant pas
l'en-tête de la congrégation ni aucune autre marque officielle. Il s'agit donc
d'une opinion personnelle de ces cardinaux comme l'indique la phrase *les très Éminents Cardinaux avec moi* et non d'un jugement officiel.

Plus tard le secrétaire du cardinal Caterini, qui avait rédigé la lettre, présenta ses excuses à Mgr Zola, en ajoutant qu'il avait eu la main forcée. Ceci est expliqué par Mgr Zola lui même (voir [la lettre de liaison n°90] page 4).

### Sur les livres de l'abbé Combe

Le cardinal Luçon demanda à la congrégation de l'Index une explication sur la mise à l'Index des écrits de l'abbé Combe au révérend père Lépidi, Maître du Sacré Palais et membre des congrégations du Saint-Office et de l’Index.

> Le 16 décembre 1912, le père Lépidi répondit au cardinal Luçon :
>
> Voici ce qui m'a été donné de recueillir par des informations séreuses sur l'affaire du secret de La Salette vis-à-vis des Congrégations Romaines, Index et Saint-Office :
>
> 1. Le secret de La Salette n'a jamais été condamné d'une manière directe et formelle par les Sacrées Congrégations de Rome.
>
> 2. Deux livres de M. Gilbert-Joseph-Émile Combe ont été condamnés par l'Index :
>
>     - l'un en 1901 : Le grand coup avec sa date probable, étude sur le secret de La Salette, augmenté de la brochure de Mélanie et autres pièces justificatives.
>
>     - l'autre livre en 1907 : Le secret de Mélanie et la crise actuelle.
>
>    Ces condamnations regardent directement et formellement les deux livres écrits par M. Combe et nullement le secret.
>
> Je prie Votre Excellence d'agréer, etc...
> Vatican, 16 décembre 1912. Albert LEPIDI, O. P.

---

Vous pourrez trouver la suite dans [la lettre de liaison n°90](/fatima100/lettre_de_liaison_90.pdf) de [Cap Fatima](https://www.fatima100.fr)
