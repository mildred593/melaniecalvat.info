---
menu: true
menutitle: L'APPARIZIONE
title: L'APPARIZIONE della SANTISSIMA VERGINE SULLA MONTAGNA DE LA SALETTE sabato 19 settembre 1846
showinsubmenu: false
showindexlist: false
weight: 3
---

- [Scarica il testo](/apparition/apparition%20anglais%20A5.odt)
- [Opuscolo in formato A5](/apparition/livret%20apparition%20IT_A5.pdf) con [una versione pronta per la stampa in A4 orizzontale da piegare in un opuscolo](/apparition/livret%20apparition%20IT_A4_booklet.pdf)

Dalla pastorella di la Salette, con l'imprimatur del Vescovo di Lecce


---

## I

Il 18 settembre 1846, vigilia della Santa Apparizione della Madonna, ero sola, come d'abitudine; facevo la guardia alle mucche dei miei pa­droni. Verso le 11 del mattino, vidi venire verso di me un bambino. A questa vista mi spaventai, perché mi sembrava che tutti dovessero sape­re che sfuggivo qualsiasi compagnia. II bambino mi si avvicinò e mi disse: “Piccola, vengo con te, anch'io sono di Corps.” A queste parole risaltò subito la mia cattiva indole, e indietreggiando di qualche passo gli risposi: "Non voglio nessuno. Voglio rimanere sola". Ma il bambi­no, seguendomi sempre mentre mi allontanavo, mi disse: “Su, lasciami con te, il mio padrone mi ha detto di far pascolare le mie mucche con le tue; sono di Corps.”

Mi allontanai da lui, facendogli segno che non volevo nessuno; e dopo essermi allontanata, mi sedetti sull'erba. Là, conversavo con i fio­rellini del Signore. Dopo un momento, guardo dietro di me, e vedo Massimino che mi stava seduto vicino. Mi disse subito: “Lasciami stare, sarò buono.” Ma il mio cattivo carattere non volle sentire ragioni. Mi alzai precipito­samente e me ne scappai un po' più lontano senza dirgli niente, e mi rimisi a giuocare coi fiori del Buon Dio. Un momento dopo, Massimino stava ancora lì a dirmi che sarebbe stato buono, che non avrebbe parlato e che si annoiava da solo, che il suo padrone lo mandava vicino a me, ecc... Questa volta ne ebbi pietà, gli feci segno di sedersi e continuai con i fiorellini del Buon Dio. 

Massimino non tardò a rompere il silenzio, si mise a ridere, (credo si burlasse di me); lo guardai ed egli mi disse: “Divertiamoci un po', fac­ciamo un gioco.” Non risposi nulla, ero così ignorante che non capivo come poter giocare con un'altra persona, essendo sempre stata sola. Giocavo solo coi fiori, e Massimino, avvicinandosi ancora di più, non finiva di ridere e mi diceva che i fiori non hanno orecchie per sentirmi, e che dovevamo giocare assieme. Io però non avevo nessuna inclina­zione per il gioco che mi proponeva. Tuttavia cominciai a parlargli, e lui mi disse che i dieci giorni che doveva passare col suo padrone sta­vano per finire, e che dopo sarebbe andato a Corps da suo padre, ecc...

Mentre ancora mi parlava, si fece sentire la campana de La Salette, era l'Angelus; feci segno a Massimino di rivolgere la sua anima a Dio. Lui si scoprì il capo e per un momento rimase in silenzio. Dopo gli dissi: “uoi fare colazione? Sì, - mi rispose – andiamo.” Ci sedemmo, tirai fuori dalla borsa le provviste datemi dai miei padroni e, secondo la mia abitudine, prima di cominciare a mangiare il mio panino tondo, vi feci sopra una crocetta col mio coltellino, e nel mezzo un forellino di­cendo: "Se c'è il diavolo che esca; se c'è il Buon Dio che vi rimanga!”, e subito ricoprii il piccolo foro. Massimino scoppiò a ridere, e dette una pedata a1 mio pane, che mi sfuggì di mano, rotolando giù per la monta­gna e si perse. 

Avevo un altro pezzo di pane, ce lo mangiammo insieme; dopo ci mettemmo a giocare; capendo però che Massimino doveva ancora avere fame, gli indicai un posto sulla montagna coperto di piccoli frutti. Lo spinsi ad andarli a mangiare, e lui lo fece subito; ne mangiò e ritornò dopo averne riempito il cappello. In serata discendemmo dalla monta­gna promettendoci di ritornare a pascolare le nostre mucche assieme.

L'indomani, 19 settembre, mi ritrovai sul cammino con Massimino; salimmo assieme il monte. M'accorsi che Massimino era buonissimo, molto semplice, e volentieri parlava di quello che m'interessava; era molto duttile, senza ostinarsi sulle sue idee; era soltanto un po' curio­so poiché quando mi allontanavo da lui e vedeva che mi fermavo, cor­reva subito per vedere quel che facevo e sentire quel che dicevo ai fio­rellini del Buon Dio; e se non arrivava in tempo, mi chiedeva cosa avessi detto. Massimino mi aveva anche chiesto che gli insegnassi un gioco. La mattina era inoltrata; gli dissi di raccogliere dei fiori per co­struire il “paradiso”. 

Ci mettemmo tutti e due all'opera; e ben presto avevamo a nostra disposizione una bella quantità di fiori di colori diversi. Sentimmo suonare 1’Angelus del villaggio poiché il cielo era terso e senza nubi. Dopo aver detto al Signore quel che sapevo, dissi a Massimino che dovevamo condurre le nostre mucche su un piccolo pianoro presso il ruscelletto, dove avremmo trovato delle pietre per costruire il "paradiso". Avviammo le mucche verso il luogo indicato, e dopo facemmo colazione; poi incominciammo a trasportare pietre e a costruire la nostra piccola casa, consistente in un pianterreno, che avrebbe dovuto servirci da abitazione, e in un primo piano che, secondo noi, doveva essere il “paradiso”. 

Questo piano era ornato di fiori multicolori e di corone sospese ai gambi. Il "paradiso" era coperto da un'unica larga pietra, essa pure ricoperta di fiori; altre corone pendevano tutt'intorno. Ultimato il “paradiso”, lo guardavamo; quando ci sorprese il sonno, ci allontanam­mo di appena due passi, e ci addormentammo sull'erba.

---

## II


Svegliandomi, e non vedendo le mucche, chiamai Massimino e co­minciai a salire sulla piccola altura. Da lì vedevo che le mucche se ne stavano tranquillamente distese; ridiscesi mentre Massimino saliva, allorché all'improvviso vidi una bella luce, più splendente del sole, ed ebbi appena il tempo di dire queste parole: “Massimino, vedi laggiù? Ah! Dio mio!” Nello stesso istante lasciai cadere il bastone che avevo in mano. Non so cosa sia passato di delizioso in me in quel momento, ma mi sentivo attratta, ero presa da un grande rispetto pieno d'amore, ed il mio cuore avrebbe voluto correre più presto di me. 

Guardavo con attenzione profonda quella luce che era immobile, e come se fosse aperta; ne scorsi un'altra molto più brillante e che si muoveva, ed in quest'ultima una bellissima Signora, seduta sul nostro “paradiso” con la testa fra le mani. La bella Signora si alzò ed incrocia­te appena le braccia, guardandoci, ci disse: *“Venite avanti, figli miei, non temete, son qui per annunciarvi una grande notizia.”* Queste dolci e soavi parole mi fecero volare fino a Lei ed il mio cuo­re avrebbe voluto attaccarsi a Lei per sempre. Arrivata vicinissima alla bella Signora, proprio davanti a Lei, alla sua destra, Ella iniziò il suo discorso, e le lacrime cominciarono a scendere dai suoi begli occhi: 

> “Se il mio popolo non vuole sottomettersi, sono obbligata a lasciare andare la mano di mio Figlio Essa è così grave e così pesante che non riesco più a sostenerla.
> 
> Da quanto tempo sto soffrendo per voi! Se voglio che mio Figlio non vi abbandoni, devo pregarLo incessantemente. Quan­to a voi, non ve ne curate nemmeno. Avete un bel pregare e darvi da fare, voi non potrete mai ricompensare la pena che mi sono presa per voi.
> 
> Dio vi ha dato sei giorni per lavorare, si è riservato il set­timo, e non glielo si vuole concedere, ed è proprio questo che appesantisce tanto il braccio di mio Figlio. 
> 
> Quelli che conducono i carri, non sanno parlare senza frapporvi il nome di mio Figlio, Sono queste due cose che appe­santiscono tanto il braccio di mio Figlio.
> 
> Se il raccolto si guasta, è per causa vostra. L'anno scorso ve l'ho fatto vedere con le patate; ma voi non ve ne siete curati; anzi, quando le trovavate marcite, bestemmiavate, usando il no­me di mio Figlio. Le patate si guasteranno ancora e, per Natale, non ce ne saranno più.” 

A questo punto io cercai di capire la parola pommes de terre; mi sembrava di capire che volesse significare ‘mele’. - (NB: Melania e Massimino non capivano il francese, ma solo il loro dialetto) - La bella e buona Signora, indovinando il mio pensiero, ripigliò il discorso (in dialetto) così: 

> “Voi non mi capite, figli miei? Ve lo dirò in altro modo.”

Ed ecco la traduzione (dal francese): 

> “Se il raccolto si guasta, è per causa vostra; ve l 'ho fatto ve­dere l'anno scorso con le patate; ma voi non ve ne siete curati; anzi, quando le trovavate marcite, bestemmiavate, usando il no­me di mio Figlio. Le patate si guasteranno ancora e, per Natale, non ce ne saranno più.
> 
> Se avete del grano, non lo seminate. Tutto quello che semi­nerete, sarà mangiato dai parassiti, e quello che crescerà si ri­durrà in polvere quando lo batterete. Verrà una grande care­stia, ma prima ancora, i bambini al di sotto dei sette anni mori­ranno di tremito in grembo a coloro che li terranno in braccio; l'uva marcirà.”

A questo punto la bella Signora, che mi affascinava, rimase un mo­mento senza farsi sentire; tuttavia vedevo che continuava, come se par­lasse, a muovere graziosamente le labbra. In quel momento Massimino riceveva il segreto. Poi, rivolgendosi a me, la Vergine SS.ma mi parlò e mi dette un segreto in francese. Eccolo qui per intero, tale e quale come me l'ha dato:

---

## III

> “Melania, ciò che ti dirò adesso, non sarà sempre un segre­to: lo potrai pubblicare nel 1858.
>
> I Sacerdoti, ministri di mio Figlio, i Sacerdoti dico, per la loro cattiva condotta, le loro irriverenze e la loro empietà nel celebrare i santi misteri, per l'amore del denaro, l'amore degli onori e dei piaceri, i Sacerdoti sono diventati cloache d'impuri­tà. Sì, i Sacerdoti chiedono vendetta, e la vendetta è sospesa sul­le loro teste. Guai ai preti e alle persone consacrate a Dio che per la loro infedeltà e la loro cattiva condotta, crocifiggono di nuovo mio Figlio! I peccati delle persone consacrate a Dio gri­dano verso il Cielo, attirano la vendetta, ed ecco che questa batte alla loro porta, perché non vi sono più anime generose, non vi è più alcuno degno di offrire all'Eterno la Vittima senza macchia in favore del mondo.
>
> Dio colpirà in modo esemplare. 
>
> Guai agli abitanti della terra! Dio sfogherà la sua collera e nessuno potrà sfuggire a tanti mali messi insieme. I capi, i con­dottieri del popolo di Dio, hanno trascurato la preghiera e la penitenza e il demonio ha ottenebrato la loro intelligenza; sono diventati quelle stelle erranti che il vecchio diavolo trascinerà con la sua coda per farli perire. Dio permetterà al vecchio ser­pente di mettere divisione fra i regnanti, in ogni società e in ogni famiglia; soffrirete pene fisiche e morali, Dio abbandone­rà gli uomini a se stessi, e manderà dei castighi che si sussegui­ranno per più di trentacinque anni. 
>
> La società è alla vigilia dei più tremendi flagelli e di grandi avvenimenti; ci si deve aspettare di essere governati con verga di ferro e bere il calice della collera di Dio.
>
> Il Vicario di mio Figlio, il Sovrano Pontefice Pio IX che non esca da Roma dopo l'anno 1859, ma che sia fermo e generoso, che combatta con le armi della fede e dell'amore; io sarò con lui. Che non si fidi di Napoleone; il cuore di costui è doppio, e quando vorrà essere contemporaneamente Papa e Imperatore, Dio si ritirerà da lui; egli è quell'aquila che volendo innalzarsi sempre di più, cadrà sulla spada di cui voleva servirsi per obbli­gare i popoli ad innalzarlo. 
>
> L'Italia sarà punita per l'ambizione di voler scuotere il gio­go del Signore dei Signori; per questo finirà in guerra ed il san­gue colerà da tutte le parti; le chiese saranno chiuse o profana­te; i preti, i religiosi, saranno cacciati via; li faranno morire, e morire di morte crudele. Parecchi abbandoneranno la fede, e il numero dei preti e religiosi che si separeranno dalla vera reli­gione sarà grande; fra costoro vi saranno anche dei Vescovi. 
>
> E che il Papa si guardi dagli operatori di miracoli, poiché è venuto il tempo in cui i prodigi più strabilianti avranno luogo in terra e nell'aria. 
>
> Nell'anno 1864 Lucifero, con un gran numero di demoni, sarà staccato dall'inferno: aboliranno poco a poco la fede per­fino nei consacrati a Dio; li accecheranno in modo tale che solo per una grazia particolare essi non saranno investiti dallo spiri­to di questi angeli cattivi; alcune case religiose perderanno in­teramente la fede e molte anime. 
>
> I libri cattivi abbonderanno sulla terra, e gli spiriti delle tenebre diffonderanno dappertutto un totale rilassamento per quel che riguarda il servizio di Dio; avranno un gran potere sulla natura; vi saranno delle chiese per servire questi spiriti. Alcune persone saranno trasportate da un luogo all'altro da questi spiriti cattivi, anche dei preti, non avendo seguito lo spiri­to buono del Vangelo che è spirito di umiltà, carità e zelo per la gloria di Dio. Faranno risuscitare alcuni morti e alcuni giusti” *(ciò vuol dire che queste anime somiglieranno alle anime giuste che erano vissute su questa terra, e tutto ciò per sedurre meglio gli uomini; questi sedicenti morti risuscitati, che non saranno altro che il demonio stesso sotto simile aspetto, predicheranno un altro Vangelo contrario a quello del vero Gesù Cristo, ne­gando l'esistenza del Cielo ed anche quella delle anime dannate. Tutte queste anime appariranno come unite ai loro corpi)*.
>
> “Vi saranno in tutti i luoghi dei prodigi straordinari, poiché la vera fede s'è spenta, mentre la falsa fede rischiara il mondo. Guai ai Principi della Chiesa che si saranno occupati ad accu­mulare ricchezze su ricchezze, a salvaguardare la loro autorità e che hanno dominato con orgoglio! 
>
> Il Vicario di mio Figlio soffrirà molto, poiché per un certo tempo la Chiesa sarà abbandonata a grandi persecuzioni sarà il tempo delle tenebre.
>
> La Chiesa si troverà in una crisi orrenda. 
>
> Essendo dimenticata la santa fede di Dio, ogni individuo vorrà guidarsi da solo, ed essere superiore ai suoi simili. I po­teri civili ed ecclesiastici saranno aboliti, ogni ordine ed ogni giustizia sarà messa sotto i piedi, non si vedranno che omicidi, odii, gelosie, menzogne e discordie, senza amore per la patria e per la famiglia. 
>
> Il S. Padre soffrirà molto. Io sarò con lui fino alla fine per ricevere il suo sacrificio. I cattivi attenteranno più volte alla sua vita senza però poter nuocere ai suoi giorni; ma né lui, né il suo successore” *(N.B.: Nel suo originale di Lecce, Melania faceva seguire a queste parole la seguente parentesi: "che non regne­rà molto")* “vedranno il trionfo della Chiesa di Dio. 
>
> I governanti civili avranno tutti il medesimo programma, di abolire cioè e far scomparire ogni principio religioso, per dar posto al materialismo, all'ateismo, allo spiritismo, e ad ogni specie di vizio. 
>
> Nell'anno 1865 si vedrà l'abominazione nei luoghi santi; nei conventi, i fiori della Chiesa saranno putrefatti e il demonio sarà il re dei cuori. Quelli che sono a capo delle comunità reli­giose, stiano attenti a quelli che ricevono, perché il demonio userà tutta la sua malizia per introdurre negli ordini religiosi delle persone viziose, perché il disordine e l'amore ai piaceri carnali saranno diffusi su tutta la terra.
>
> La Francia, l'Italia, la Spagna e l'Inghilterra, saranno in guerra, il sangue scorrerà per le strade; il francese si batterà col francese, l'italiano con l'italiano. Infine vi sarà una guerra generale, che sarà spaventosa. Per un certo tempo, Dio non si ricorderà più della Francia, né dell'Italia, perché il Vangelo di Gesù Cristo non sarà più conosciuto. I cattivi useranno tutta la loro malizia; si uccideranno, mi massacreranno a vicenda, per­fino nelle case.
>
> Al primo colpo della sua spada sfolgorante, le montagne e tutta la natura tremeranno dallo spavento, perché i disordini e i delitti degli uomini squarceranno la volta del cielo. Parigi sarà bruciata e Marsiglia inghiottita; diverse grandi città saranno scosse e inghiottite dai terremoti, si crederà che tutto è perduto; non si vedranno che omicidi, non si sentirà che rumore di armi e bestemmie. I giusti soffriranno molto; le loro preghiere, le lo­ro penitenze e le loro lacrime saliranno fino al Cielo, e tutto il popolo di Dio domanderà perdono e misericordia, e chiederà il mio aiuto e la mia intercessione. Allora Gesù Cristo, con un atto della sua giustizia e della sua grande misericordia per i giusti, comanderà ai suoi Angeli, che tutti i suoi nemici siano messi a morte. Ad un tratto, i persecutori della Chiesa di Gesù Cristo e tutti gli uomini votati al peccato periranno e la terra diventerà come un deserto. Allora vi sarà la pace, la riconciliazione di Dio con gli uomini; Gesù Cristo sarà servito, adorato e glorificato; la carità fiorirà dappertutto. I nuovi Re saranno il braccio della Santa Chiesa, che sarà forte, umile, pia, povera, piena di zelo e imitatrice delle virtù di Gesù Cristo. Il Vangelo sarà predicato dappertutto, e gli uomini faranno grandi progressi nella fede, poiché vi sarà unione fra gli operai di Gesù Cristo e gli uomini vivranno nel timor di Dio.
>
> Questa pace fra gli uomini non durerà a lungo; venticinque anni di raccolto abbondante saranno sufficienti per far loro di­menticare che i peccati degli uomini sono la causa di tutte le pene che piombano sulla terra.
>
> Un precursore dell'Anticristo, con le truppe di diverse na­zioni, combatterà il vero Cristo, il solo Salvatore del mondo; spargerà molto sangue e vorrà annientare il culto di Dio per farsi considerare come Dio.
>
> La terra sarà colpita da ogni specie di piaghe” *(oltre la pe­ste, la fame)* “che saranno generali; le guerre si susseguiranno fino all'ultima che sarà condotta dai dieci re dell’Anticristo, i quali avranno tutti un unico intento e saranno i soli a governare il mondo. Prima che ciò si verifichi, vi sarà una specie di falsa pace nel mondo, si penserà soltanto a divertirsi, i cattivi si ab­bandoneranno ad ogni specie di peccato; ma i figli della Santa Chiesa, i figli della Fede, i miei veri imitatori, cresceranno nel­l'amor di Dio e nelle virtù che mi sono più care. Beate le anime umili, condotte dallo Spirito Santo! Io combatterò con loro fin­ché non saranno arrivate alla pienezza della loro età.
>
> La natura chiede vendetta per gli uomini, e freme di spaven­to in attesa di ciò che deve accadere alla terra insozzata di de­litti!!. 
>
> Tremate, terra, e voi che fate professione di servire Gesù Cristo e che al di dentro adorate voi stessi! Tremate; poiché Dio vi consegnerà al suo nemico, perché i luoghi santi sono nella corruzione; molti conventi non sono più case di Dio, ma pasco­lo di Asmodeo e dei suoi.
>
> Durante questo tempo nascerà l'Anticristo, da una religio­sa ebrea, da una falsa vergine che sarà in comunicazione con il vecchio serpente, il maestro di impurità; suo padre sarà Vesc. ; nascendo vomiterà bestemmie, avrà dei denti; in una parola sa­rà l'incarnazione del diavolo, emetterà grida spaventose, farà prodigi, non si nutrirà che d'impudicizie. Avrà dei fratelli che, sebbene non saranno come lui dei demoni incarnati, saranno figli del male; a 12 anni si faranno notare per le valorose vitto­rie che riporteranno; in poco tempo ciascuno sarà alla testa di armate, assistiti da legioni infernali.
>
> Le stagioni cambieranno; la terra produrrà soltanto frutti cattivi; gli astri perderanno i loro movimenti regolari, la luna metterà solo una debole luce rossastra; l'acqua ed il fuoco da­ranno al globo terrestre dei movimenti convulsi ed orribili ter­remoti che faranno inghiottire montagne, città *(ecc...)*.
>
> Roma perderà la fede e diverrà la sede dell'Anticristo.
>
> I demoni dell'aria con l'Anticristo faranno grandi prodigi sulla terra e nell'aria, e gli uomini si pervertiranno sempre più. Dio avrà cura dei suoi fedeli servitori e degli uomini di buona volontà; il vangelo sarà predicato ovunque, tutti i popoli e tutte le nazioni avranno conoscenza della verità!'"Rivolgo un pressante appello alla terra: chiamo i veri di­scepoli del Dio vivente e regnante nei Cieli; chiamo i veri imita­tori del Cristo fatto uomo, il solo vero Salvatore degli uomini, chiamo i miei figli, i miei veri devoti, coloro che si sono dati a me perché li conduca al mio divin Figlio, coloro che io porto per così dire nelle mie braccia, coloro che sono vissuti del mio spiri­to; chiamo infine gli Apostoli degli Ultimi Tempi, i fedeli disce­poli di Gesù Cristo, che sono vissuti nel disprezzo del mondo e di se stessi, nella povertà e nell'umiltà, nel disprezzo e nel silenzio, nella preghiera e nella mortificazione, nella castità e nell'unione con Dio, nella sofferenza e sconosciuti dal mondo. E’ tempo che escano e vengano a rischiarare la terra. Andate, e mostratevi come i miei figli prediletti; io sono con voi, perché la vostra fede sia la luce che vi rischiari in questi giorni di sventura. Che il vostro zelo vi renda come degli affamati della gloria e dell'ono­re di Gesù Cristo. Combattete, figli della luce, piccolo numero che ci vedete; poiché ecco il tempo dei tempi, la fine delle fini. " "La Chiesa sarà eclissata, il mondo sarà nella costernazio­ne: Ma ecco Enoch ed Elia ripieni dello Spirito di Dio; essi pre­dicheranno con la forza di Dio, e gli uomini di buona volontà crederanno in Dio e molte anime saranno consolate; faranno grandi progressi in virtù dello Spirito Santo e condanneranno gli errori diabolici dell’ Anticristo.
>
> Guai agli abitanti della terra! Vi saranno guerre sanguino­se e carestie, epidemie di peste e di malattie contagiose; vi sa­ranno delle piogge di una spaventosa grandine di animali, dei tuoni che scuoteranno le città, dei terremoti che inghiottiranno i paesi; si sentiranno delle voci nell'aria; gli uomini sbatteranno la testa contro i muri; invocheranno la morte, e d'altro canto, la morte farà il loro supplizio; il sangue scolerà ovunque. Chi po­trà vincere, se Dio non accorcia il tempo della prova? Per mezzo del sangue, delle preghiere e delle lacrime dei giusti, Dio si la­scerà piegare; Enoch ed Elia saranno uccisi; Roma pagana sparirà; cadrà il fuoco dal cielo e consumerà tre città; l'intero universo sarà colpito dal terrore, e molti si lasceranno sedurre, perché non hanno adorato il vero Cristo vivente in mezzo a loro. E' il tempo; il sole si oscura; la fede soltanto vivrà.
>
> Ecco il tempo; l'abisso si apre. Ecco il re delle tenebre. Ec­co la bestia coi suoi sudditi, che si dice salvatore del mondo. S'innalzerà con orgoglio nell'aria per andare fino al cielo, sarà però soffocato dal soffio di San Michele Arcangelo. Cadrà, e la terra che, da tre giorni sarà in continue evoluzioni, aprirà il suo seno pieno di fuoco; egli sarà scaraventato per sempre, con tutti i suoi, negli abissi eterni dell'inferno. Allora l'acqua ed il fuoco purificheranno la terra, e consumeranno tutte le opere dell'or­goglio degli uomini, e tutto sarà rinnovato: Dio sarà servito e glorificato.”

---

## IV


Poi la Madonna mi diede, sempre in francese, **la Regola di un nuo­vo Ordine religioso.**

Dopo avermi dato la Regola di questo nuovo Ordine religioso, la Madonna riprese così il seguito del suo discorso: 

> “Se si convertono, le pietre e le rocce si cambieranno in grano e le patate si troveranno disseminate sulla terra .
>
> La preghiera, la fate bene, figli miei?” 

Rispondemmo insieme: “Oh, no, Signora, non molto”. 

> “Ah, figli miei, bisogna farla bene, sera e mattina, e quando non potete fare meglio, dite un "Pater' ed un "Ave Maria', e quando avrete tempo e potrete far meglio, ne direte di più .
>
> C'è solo qualche donna piuttosto anziana che va a Messa; gli altri lavorano la Domenica, per tutta l'estate; e durante l'inverno, se non sanno cosa fare, vanno a Messa, ma solo per scherno verso la religio­ne. Durante la quaresima vanno in macelleria come dei cani .
>
> Non avete visto del grano guasto, figli miei?”

Tutti e due rispondemmo: “Oh! no, Signora.”

> *La Madonna, indirizzandosi a Massimino:* “Ma tu, figlio mio, devi averlo visto una volta, nei pressi di Coin, con tuo padre. Il padrone del locale disse a tuo padre: - Venite a vedere come si guasta il mio grano. - E voi ci andaste. Tuo padre prese due o tre spighe in mano, lo strofi­nò, e si ridusse in polvere, poi, durante il ritorno, quando eravate a mezz'ora da Corps, tuo padre ti diede un pezzo di pane dicendoti: - Tieni, figlio mio, mangialo ancora per quest'anno, poiché l'anno ven­turo non so chi potrà mangiare, se il grano si guasta così .”

Massimino rispose: "E’ proprio vero, Signora, non ci pensavo più ".

> *La Madonna termina il suo discorso in francese:* “Ebbene, figli miei,  voi lo farete conoscere a tutto il mio popolo.”

La bellissima Signora attraversò il ruscello; e a due passi dal ruscel­lo, senza voltarsi verso di noi che la seguivamo (perché attirava a Lei per il suo splendore ed ancor più per la sua bontà che m'inebriava e sembrava facesse fondere il mio cuore) ci disse ancora:

> “bbene, figli miei, voi lo farete conoscere a tutto il mio popolo.”

Poi continuò a camminare fino al posto in cui ero salita per vedere dove si trovassero le nostre mucche. I suoi piedi toccavano appena la punta dell'erba, senza piegarla. Arrivata sulla piccola altura, la bella Signora si fermò e subito mi posi davanti a Lei per guardarla ben bene e poter capire in quale direzione volesse maggiormente andare: poiché, per me, era fatta, avevo dimenticato e le mucche e i padroni preso i quali ero in servizio; mi ero legata per sempre e senza condizioni alcuna alla mia Signora; sì, non volevo più, mai più, lasciarla; la seguivo senza pregiudizio alcuno, e disposta a servirla per tutta la vita.

Con la mia Signora mi sembrava aver dimenticato il "paradiso"; non pensavo ad altro che a servirla, per il mio meglio, in tutto; e pensavo che sarei anche riuscita a poter fare tutto quello che mi avrebbe detto di fare, perché avevo l'impressione che fosse molto potente. Lei mi guar­dava con una tenera bontà che mi attirava a Lei; avrei voluto, ad occhi chiusi, lanciarmi nelle sue braccia. Lei non mi diede il tempo di farlo. Si alzò da terra, in modo insensibile, all'altezza di circa un metro e più; e restando così sospesa in aria un momentino, la mia bella Signora guar­dò il cielo, poi la terra a destra e a sinistra e poi mi guardò con degli occhi così dolci, così amabili e così buoni che mi sembrò come se mi attirasse nel suo intimo ed il mio cuore si aprisse nel suo. 

E mentre il mio cuore si fondeva in una dolce dilatazione, la bella immagine della mia Signora poco a poco spariva: mi sembrava come se la luce, in movimento, si moltiplicasse e si condensasse attorno alla Vergine SS.ma per impedirmi di contemplarla ancora. Così, la luce sostituiva le forme del corpo che sparivano ai miei occhi; ovvero sem­brava che il corpo della mia Signora si cambiasse in luce, fondendosi. Così, sotto forma di globo, la luce dolcemente andava diritto verso l'alto. 

Non riesco a dire se era il volume di luce che diminuiva man mano che si alzava, o se invece era l'allontanamento che dava l'impressione di veder diminuire la luce mentre si alzava; ciò che so è che sono rima­sta con la testa in su e gli occhi fissi sulla luce, anche dopo che quella luce, che si allontanava e sempre più diminuiva di volume, finì per spa­rire del tutto. 

I miei occhi si staccarono dal firmamento, mi guardai attorno, ed os­servai Massimino che mi guardava, e gli dissi: “Massimino, sarà stato il Signore di mio padre, o la Madonna, o qualche gran Santo.” E Mas­simino, slanciando la mano in aria, disse: “Ah, se l'avessi saputo!”

---

## V


La sera del 19 settembre ci ritirammo un po' più presto del solito. Appena arrivata dai miei padroni, mi occupai ad attaccare le mucche e a mettere tutto in ordine nella stalla. Non avevo ancora finito che la mia padrona venne verso di me piangendo e mi disse: “Perché, figlia mia, non vieni a dirmi ciò che è accaduto sulla montagna?” (Massimino, non avendo trovato i suoi padroni perché non erano ancora tornati dai lavori, era venuto dai miei ed aveva raccontato tutto quanto aveva visto ed inteso). Risposi: “Volevo ben dirvelo, ma prima volevo finire il mio lavoro.”

Dopo un momento entrai in casa e la padrona mi disse: “Racconta tutto quello che hai visto; il pastorello di Bruite (soprannome di Pietro Selme, padrone di Massimino) mi ha raccontato tutto.” 

Incomincio e, verso la metà del racconto, i miei padroni arrivano dai campi; la mia padrona, che stava piangendo sentendo le lamentele e le minacce della nostra tenera Madre, disse: “Ah, voi avete intenzione di andare a raccogliere il grano domani; non ve lo permettete, venite a sentire cosa è capitato oggi a questa figliola e al pastorello di Selme.” E, voltandosi verso di me, disse: “Incomincia daccapo tutto quello che mi hai detto.” Io ricomincio; e dopo aver finito, il mio padrone mi dis­se: “Era la Madonna, oppure una grande Santa, venuta da parte di Dio; ma è come se fosse venuto il Signore stesso; bisogna fare tutto quello che ha detto questa Santa. Come farete per dire tutto al popo­lo?” Io gli risposi: “Voi mi direte come devo fare, ed io lo farò.”  Dopo, guardando sua madre, sua moglie e suo fratello, aggiunse: “Bisogna pensarci”. Ed ognuno si ritirò per i fatti suoi. 

Si era dopo cena. Massimino e i suoi padroni vennero presso i miei per raccontare ciò che aveva detto loro Massimino e per sapere cosa si sarebbe dovuto fare: "Poiché - dissero - ci sembra che sia la Madonna che sia stata inviata dal Signore; le parole che ha detto ce lo fanno credere. E Lei ha anche detto loro di farlo conoscere a tutto il suo po­polo; forse bisognerà che questi piccoli percorrano il mondo intero per far sapere che bisogna che tutti osservino i comandamenti di Dio, al­trimenti grandi disgrazie cadranno su di noi". Dopo un momento di silenzio, il mio padrone disse, voltandosi verso di me e di Massimino: “apete, figli miei, cosa dovete fare? Domani alzatevi presto, andate tutti e due dal signor Curato e raccontategli tutto quello che avete visto e sentito; ditegli con cura come si sono svolte le cose: sarà lui a dirvi cosa bisogna fare.” 

Il 20 settembre, l'indomani della apparizione, di buon mattino me ne andai con Massimino. Arrivati in parrocchia, busso alla porta. Mi viene ad aprire la domestica del signor Parroco, domandandoci cosa volessi­mo. Io le dissi (in francese, pur non avendolo mai parlato): “Desidere­remmo parlare col Parroco.” “Cosa volete dirgli?”, ci chiese. “Signo­rina, vorremmo dirgli che ieri siamo andati a pascolare le mucche sulla montagna delle Baisses, e dopo aver fatto colazione, ecc. ecc...” Le raccontammo buona parte del discorso della Vergine SS.ma. In quel momento suonò la campana della chiesa: era l'ultimo rintocco per la Messa. Il Rev. Perrin, Parroco de La Salette, che ci aveva sentito, a­prì la porta con strepito: piangeva e si batteva il petto; ci disse: “Fi­gli miei, sianto perduti, il Signore ci punirà. Ah! mio Dio, è la Ma­donna che vi è apparsa!”. E se ne andò per celebrare la S. Messa. Noi ci guardammo in faccia con Massimino e la domestica; poi Massimino mi disse: “Io me ne vado da mio padre a Corps.” E ci separammo. 

Non avendo ricevuto ordine dai miei padroni di ritornare subito do­po aver parlato col Parroco, non credetti far male ad assistere alla Mes­sa. Entrai dunque in chiesa. Incomincia la Messa e, dopo il primo Van­gelo, il signor Parroco si volta verso il popolo e prova a raccontare ai suoi parrocchiani l'apparizione che era appena avvenuta, la vigilia, su una delle loro montagne, esortandoli a non più lavorare di domenica; la sua voce era interrotta da singhiozzi, e tutta la popolazione era commossa. Dopo la S. Messa me ne ritornai dai miei padroni. Il signor Peytard, che ancor'oggi è Sindaco de La Salette, venne ad interrogarmi sul fatto dell'apparizione; e dopo essersi assicurato sulla verità di quan­to dicevo, convinto si ritirò. 

Rimasi al servizio dei miei padroni fino a Ognissanti. Dopo mi mise­ro in pensione presso le Religiose della Provvidenza, nel mio paese di Corps.

---

## VI


La Vergine SS.ma era molto alta e ben proporzionata; sembrava es­sere tanto leggera, che sarebbe bastato un soffio a farla muovere, però era immobile e molto stabile. La sua fisionomia era maestosa ed im­ponente come sono i signori di questa terra. Imponeva una timidezza rispettosa. Mentre la Sua Maestà imponeva rispetto misto ad amore, attirava a Lei. 

Il suo sguardo era dolce e penetrante; i suoi occhi sembrava che parlassero coi miei, ma la conversazione proveniva da un profondo e vivo sentimento d'amore verso questa attraente bellezza che mi liquefa­ceva. La dolcezza del suo sguardo, l'aria di bontà incomprensibile, facevano intendere e sentire che Ella attirava a sé per donarsi; era un'e­spressione di amore che a parole non si può esprimere e nemmeno con le lettere dell'alfabeto. 

L'abito della Vergine SS.ma era bianco argento, molto splendente; non aveva nulla di materiale: era fatto di luce e di gloria, scintillante e variato; sulla terra non vi sono espressioni né paragoni da poter fare. 

La Vergine SS.ma era tutta bella e tutta fatta d'amore; guardan­dola, io languivo per fondermi in Lei. Dai suoi ornamenti, come dalla sua persona, da tutto trapelava la maestà, lo splendore, la magnificenza splendente, celeste, fresca, nuova come una Vergine; sembrava che la parola Amore sfuggisse dalle sue labbra argentee e pure. Aveva l'appa­renza di una Mamma affettuosa, piena di bontà, di amabilità, di amore per noi, di compassione e di misericordia. 

La corona di rose che portava sulla testa era così bella e brillante, da non potersene fare un'idea; le rose di diversi colori non erano di questa terra; era un insieme di fiori che circondava il capo della SS.ma Vergi­ne, proprio in forma di corona; ma le rose cambiavano e si ricambiava­no, poi dal centro di ogni rosa usciva una luce così bella che rapiva, e faceva sì che la loro bellezza risplendesse. Dalla corona di rose usciva­no come dei rami d'oro e tanti altri piccoli fiori misti e brillanti. Il tutto formava un diadema che da solo brillava più del nostro sole terreno. 

La Vergine portava una graziosissima Croce sospesa al collo. Questa croce sembrava d'oro, dico d'oro per non dire un pezzo d'oro; a volte ho visto degli oggetti dorati con alcune sfumature, ciò che faceva ai miei occhi un effetto più bello di un semplice pezzo d'oro. Su questa bella Croce piena di luce era un Cristo, era Nostro Signore con le brac­cia stese sulla Croce. Quasi alle due estremità della Croce, c'erano: da una parte un martello e dall'altra una tenaglia. Il Cristo era color carne naturale ma riluceva con grande splendore; e la luce che usciva da tutto il suo corpo, sembrava come dardi lucentissimi che mi infiammavano il cuore per il desiderio di perdermi in Lui. A volte il Cristo sembrava morto; aveva la testa inclinata e il corpo rilassato, quasi cadesse se non fosse trattenuto dai chiodi che lo fissavano sulla Croce. 

Io ne avevo viva compassione; avrei voluto comunicare al mondo intero il suo amore sconosciuto ed infondere nelle anime dei mortali il più sentito amore e la più viva riconoscenza, verso un Dio che non ave­va assolutamente bisogno di noi per essere quello che è, ciò che era e ciò che sempre sarà; e tuttavia, oh, amore per l'uomo incomprensibile! s'è fatto uomo, ha voluto morire, sì morire, per poter meglio scrivere nelle nostre anime e nella nostra memoria il folle amore che ha per noi! Oh! come mi sento infelice nel constatare la mia povertà di espressione nel riferire l'amore del nostro buon Salvatore per noi! Ma, d'altra parte, come siamo felici di poter sentire meglio ciò che non possiamo espri­mere! 

Altre volte il Cristo sembrava vivo; aveva la testa diritta, gli occhi aperti, e sembrava sulla Croce di sua volontà. A volte, anche, pareva che parlasse: sembrava mostrasse che era in Croce per noi, per amor nostro, per attirarci al suo amore, che ha sempre un nuovo amore per noi, che il suo amore dell'inizio - dell'anno 33 - è sempre quello di oggi e lo sarà sempre. 

Mentre mi parlava, la Vergine SS.ma piangeva ininterrottamen­te. Le sue lacrime cadevano una dopo l'altra, lentamente, fin sopra le sue ginocchia; poi, come scintille di luce, sparivano. Erano splen­denti e piene di amore. Avrei voluto consolarla e non farla piangere, ma mi sembrava che Ella avesse bisogno di mostrare le sue lacrime per meglio manifestare il suo amore dimenticato dagli uomini. Avrei voluto gettarmi fra le sue braccia e dirle: “Mia buona Madre, non pian­gete! Io voglio amarvi per tutti gli uomini della terra.” Ma sembrava che mi rispondesse: “Ve ne sono molti che non mi conoscono!”

Ero tra la morte e la vita, vedendo da un lato tanto amore, tanto de­siderio di essere amata e, dall'altro, tanta freddezza ed indifferenza... Oh! Madre mia tutta bella e tanto amabile, amore mio e cuore del mio cuore! 

Le lacrime della nostra tenera Madre, lungi dal diminuire la sua Maestà di Regina e Sovrana, sembravano invece renderla più bella, più potente, più piena d'amore, più materna, più attraente; avrei man­giato le sue lacrime che facevano sobbalzare il mio cuore di compassio­ne e d'amore. Veder piangere una Madre, ed una tale Madre, senza adoperare tutti i mezzi possibili per consolarla, per cambiare i suoi dolori in gioia, si può comprendere? O Madre più che buona! Voi siete stata formata di tutte le prerogative di cui Dio è capace; Voi avete, in un certo senso, esaurita la potenza di Dio; Voi siete buona, ed ancora buona della bontà di Dio stesso; Dio, formandovi come il suo capolavo­ro celeste e terrestre si è reso ancora più grande. 

La Vergine SS.ma aveva un grembiule giallo. Ma che dico, giallo? Aveva un grembiule più luminoso di più soli messi assieme. Non era una stoffa materiale, ma un composto di gloria, e questa gloria era splendente di una bellezza che rapiva. Tutto, nella Vergine SS.ma mi portava con forza ad adorare e ad amare il mio Gesù in tutti i det­tagli della sua vita mortale. 

La Vergine SS.ma aveva due catene, una un po' più larga dell'altra. A quella più stretta era sospesa la Croce di cui ho parlato sopra. 

Queste catene (non posso chiamarle diversamente) erano come raggi di gloria, di un gran chiarore che variava e scintillava. 

Le scarpe (poiché così bisogna chiamarle) erano bianche, ma di un bianco argenteo, brillante, ed intorno vi erano delle rose. Queste rose erano di una bellezza abbagliante, e dal centro di ognuna usciva come una fiamma di luce bellissima e gradevolissima. Sulle scarpe c'era un fermaglio d'oro, ma non oro di questo mondo, bensì di paradiso. 

La visione della SS.ma Vergine era di per sé un intero paradiso. Lei aveva con sé tutto quanto poteva dare soddisfazione, poiché si di­menticava questa terra. 

La Madonna era circondata da due luci: la prima a Lei più vicina ar­rivava fino a noi e brillava con vivissimo splendore. La seconda luce si spandeva un po' attorno alla Bella Signora, e noi ci trovavamo immersi in essa ed era immobile (cioè non brillava) e molto più luminosa del nostro sole terrestre. Tutte queste luci non facevano male agli occhi, e non affaticavano la vista. Oltre queste luci e tutto quello splendore, vi erano altri fasci di luce o altri raggi di sole, come se nascessero dal corpo della Vergine, dai suoi abiti, dappertutto. 

La voce della Bella Signora era dolce; incantava, rapiva e faceva bene al cuore; saziava, appianava ogni ostacolo, calmava, addolciva. Mi sembrava come se volessi sempre saziarmi della sua bella voce, ed il mio cuore pareva ballare o volerle andare incontro per struggersi in Lei. 

Gli occhi della Vergine SS.ma, nostra tenera Madre, non possono essere descritti da lingua umana. Per parlarne occorrerebbe un Serafino, più ancora, occorrerebbe la lingua stessa di Dio, di quel Dio che forma la Vergine Immacolata Capolavoro della sua Onnipotenza. 

Gli occhi dell'augusta Maria sembravano mille e mille volte più belli dei brillanti, dei diamanti, delle pietre preziose più ricercate; brillavano come due soli; erano dolci, come la stessa dolcezza, limpidi come uno specchio. In quei suoi occhi si vedeva il paradiso; attiravano a Lei; sembravano come se Ella volesse donarsi e attirare. Più la guardavo, più desideravo guardarla; e più la guardavo, e più l'amavo; e l'ama­vo con tutte le mie forze. 

Gli occhi della bella Immacolata erano come la porta di Dio, da dove si vedeva tutto quanto poteva inebriare l'anima. Quando i miei occhi s'incontravano con quelli della Madre di Dio e mia, sentivo dentro di me una gioiosa rivoluzione d'amore ed una protesta di amarla e di­struggermi d'amore. 

Guardandoci, i nostri occhi, a loro modo, si parlavano, e l'amavo talmente che avrei voluto abbracciarla proprio nell'intimo stesso di quegli occhi che m'intenerivano l'anima e sembravano attirarla e farla fondere con la sua. I suoi occhi comunicarono un dolce tremito a tutto il mio essere; e temevo di fare il più piccolo movimento, per paura che le potesse essere minimamente sgradevole. 

La sola vista degli occhi della più pura delle Vergini sarebbe ba­stata per costruire il Cielo di un beato; sarebbe bastata per far entrare un'anima nella pienezza della Volontà dell'Altissimo, per tutti gli av­venimenti che capitano nel corso della vita mortale; sarebbe bastata per far fare a quell'anima degli atti di lode, di ringraziamento, di riparazio­ne, di espiazione. Questa visione, da sola, concentra l'anima in Dio e la rende come una morta-vivente, che guarda tutte le cose della terra, an­che quelle che sembrano più serie, come se fossero semplici giuochi di bambini; l'anima vorrebbe soltanto sentire parlare di Dio e di tutto ciò che riguarda la sua Gloria. 

Il peccato è il solo male che Lei vede sulla terra. Se Dio non la so­stenesse, ne morirebbe di dolore. Amen.

---

Castellammare, 21 novembre 1878

Maria della Croce -Vittima di Gesù - Nata Mélania Calvat, Pastorella de la Salette




