---
menu: true
menutitle: L'apparition
title: L’Apparition de la Très Sainte Vierge sur la Montagne de la Salette le 19 septembre 1846
date: 2022-12-14T19:18:59.349+01:00
showinsubmenu: false
showindexlist: false
weight: 3
related:
  - /bibliotheque/brochure-1879
  - /videos/nd-de-la-salette
---

Par la Bergère de la Salette, avec Imprimatur de Mgr l’Évêque de Lecce. Un
second imprimatur a été également donné à une réédition avec nouvel imprimatur
le 6 juin 1922 par le R.P. A. Lépidi qui n'a jamais été condamné.

<center>{{< imgresize "/livres/brochure-1922/imprimatur2.jpeg" "300x" >}}</center>

- [Voir la brochure]({{< relref "/bibliotheque/brochure-1879" >}})
- [Se renseigner sur l'imprimatur]({{< relref "/faq/le-secret-a-t-il-été-mis-à-lindex" >}})
- [Télécharger le texte de l'apparition](/apparition/apparition%20français%20A5.odt)
- [Brochure mise en forme au format A5](/apparition/livret%20apparition%20FR_A5.pdf) et une [version A4 paysage à plier pour en faire un livret](/apparition/livret%20apparition%20FR_A4_booklet.pdf)

---

{{< related "/bibliotheque/brochure-1879" >}}

{{< related "/videos/nd-de-la-salette" >}}

## I


Le 18 septembre, veille de la sainte Apparition de la Sainte Vierge, j’étais seule, comme à mon ordinaire, à garder les quatre vaches de mes Maîtres. Vers les 11 heures du matin, je vis venir auprès de moi un petit garçon. A cette vue, je m’effrayai, parce qu’il me semblait que tout le monde devait savoir que je fuyais toutes sortes de compagnies. Cet enfant s’approcha de moi et me dit : "Petite, je viens avec toi, je suis aussi de Corps." A ces paroles, mon mauvais naturel se fit bientôt voir, et, faisant quelques pas en arrière, je lui dis : "Je ne veux personne, je veux rester seule." Puis, je m’éloignais, mais cet enfant me suivait en me disant : " Va, laisse-moi avec toi, mon Maître m’a dit de venir garder mes vaches avec les tiennes ; je suis de Corps."

Moi je m’éloignai de lui, en lui faisant signe que je ne voulais personne ; et après m’être éloignée, je m’assis sur le gazon. Là, je faisais ma conversation avec les petites fleurs du Bon Dieu.

Un moment après, je regarde derrière moi, et je trouve Maximin assis tout près de moi. Il me dit aussitôt : "Garde moi, je serais bien sage." Mais mon mauvais naturel n’entendit pas raison. Je me relève avec précipitation, et je me remis à jouer avec les fleurs du Bon Dieu. Un instant après, Maximin était encore là à me dire qu’il serait bien sage, qu’il ne parlerait pas, qu’il s’ennuierait d’être tout seul, et que son Maître l’envoyait auprès de moi, etc… Cette fois, j’en eus pitié, je lui fis signe de s’asseoir, et moi, je continuai avec les petites fleurs du Bon Dieu.

Maximin ne tarda pas à rompre le silence, il se mit à rire, (je crois qu’il se moquait de moi) ; je le regarde, et il me dit : "Amusons-nous, faisons un jeu." Je ne lui répondis rien, car j’étais si ignorante, que je ne comprenais rien au jeu avec une autre personne, ayant toujours été seule. Je m’amusais seule avec les fleurs, et Maximin, s’approchant tout à fait de moi, ne faisait que rire en me disant que les fleurs n’avaient pas d’oreilles pour m’entendre, et que les fleurs n’avaient pas d’oreilles pour m’entendre, et que nous devions jouer ensemble. Mais je n’avais aucune inclination pour le jeu qu’il me disait de faire. Cependant je me mis à lui parler, et il me dit que les dix jours qu’il devait passer avec son Maître allaient bientôt finir, et qu’ensuite, il s’en irait à Corps chez son père, etc…

Tandis qu’il me parlait, la cloche de la Salette se fit entendre, c’était l’Angélus ; je fis signe à Maximin d’élever son âme à Dieu. Il se découvrit la tête et garda un moment le silence. Ensuite, je lui dis : "Veux-tu dîner ? – Oui, me dit-il. Allons." Nous nous assîmes ; je sortis de mon sac les provisions que m’avaient données mes maîtres, et, selon mon habitude, avant d’entamer mon petit pain rond, avec la pointe de mon couteau je fis une croix sur mon pain, et au milieu un tout petit trou, en disant : "Si le diable y est qu’il en sorte, et si le Bon Dieu y est qu’il y reste ", et vite, vite, je recouvris le petit trou. Maximin partit d’un grand éclat de rire et donna un coup de pied à mon pain, qui s’échappa de mes mains, roula jusqu’au bas de la montagne et se perdit.

J’avais un autre morceau de pain, nous le mangeâmes ensemble ; ensuite nous fîmes un jeu ; puis comprenant que Maximin devait avoir besoin de manger, je lui indiquais un endroit de la montagne couvert de petits fruits. Je l’engageai à aller en manger, ce qu’il fit aussitôt ; il en mangea et en rapporta plein son chapeau. Le soir nous descendîmes ensemble de la montagne, et nous nous promîmes de revenir garder nos vaches ensemble.

Le lendemain, le 19 septembre, je me retrouve en chemin avec Maximin ; nous gravissons ensemble la montagne. Je trouvais que Maximin était très bon, très simple, et que volontiers il parlait de ce dont je voulais parler ; il était aussi très souple, ne tenant pas à son sentiment ; il était seulement un peu curieux, car quand je m’éloignais de lui, dès qu’il me voyait arrêté, il accourait vite pour voir ce que je faisais, et entendre ce que je disais avec les fleurs du Bon Dieu ; et s’il n’arrivait pas à temps, il me demandait ce que j’avais dit. Maximin me dit de lui apprendre un jeu. La matinée était déjà avancée : je lui dis de ramasser des fleurs pour faire le "Paradis".

Nous nous mîmes tous les deux à l’ouvrage ; nous eûmes bientôt une quantité de fleurs de diverses couleurs. L’Angélus du village se fit entendre, car le ciel était beau, il n’y avait pas de nuages. Après avoir dit au Bon Dieu ce que nous savions, je dis à Maximin que nous devions conduire nos vaches sur un petit plateau près du petit ravin, où il y aurait des pierres pour bâtir le "Paradis". Nous conduisîmes nos vaches au lieu désigné, et ensuite nous prîmes notre petit repas ; puis, nous nous mîmes à porter des pierres et à construire notre petite maison, qui consistait en un rez-de-chaussée, qui soi-disant était notre habitation, puis un étage au-dessus qui était selon nous le "Paradis".

Cet étage était tout garni de fleurs de différentes couleurs, avec des couronnes suspendues par des tiges de fleurs. Ce "Paradis" était couvert par une seule et large pierre que nous avions recouverte de fleurs ; nous avions aussi suspendu des couronnes tout autour. Le " Paradis "terminé, nous le regardions ; le sommeil nous vint ; nous nous éloignâmes de là à environ deux pas, et nous nous endormîmes sur le gazon.

La Belle Dame s’assied sur notre "Paradis" sans le faire crouler.


---

## II


M’étant réveillée, et ne voyant pas nos vaches,  j’appelai Maximin et je gravis le petit monticule. De là, ayant vu que nos vaches étaient couchées tranquillement, je redescendais et Maximin montait, quand tout à coup, je vis une belle lumière, plus brillante que le soleil, et à peine ai-je pu dire ces paroles : "Maximin, vois-tu, là-bas ? Ah ! Mon Dieu !" En même temps je laisse tomber le bâton que j’avais en main.          Je ne sais ce qui se passait en moi de délicieux dans ce moment, mais je me sentais attirer, je me sentais un grand respect plein d’amour, et mon cœur aurait voulu courir plus vite que moi.

Je regardais bien fortement cette lumière qui était immobile, et comme si elle fût ouverte, j’aperçus une autre lumière bien plus brillante et qui était en mouvement, et dans cette lumière une très belle Dame assise sur notre "Paradis", ayant la tête dans ses mains. Cette belle Dame s’est levée, elle a croisé médiocrement ses bras en nous regardant et nous a dit : *"Avancez, mes enfants, n’ayez pas peur ; je suis ici pour vous annoncer une grande nouvelle."* Ces douces et suaves paroles me firent voler jusqu’à elle, et mon cœur aurait voulu se coller à elle pour toujours. Arrivée, bien près de la belle Dame, devant elle, à sa droite, elle commence le discours, et des larmes commencent aussi à couler de ses beaux yeux :

> "Si mon peuple ne veut pas se soumettre, je suis forcée de laisser aller la main de mon Fils. Elle est si lourde et si pesante, que je ne puis plus la retenir.
> 
> Depuis le temps que je souffre pour vous autres ! Si je veux que mon fils ne vous abandonne pas, je suis chargée de le prier sans cesse. Et pour vous autres, vous n’en faites pas cas. Vous aurez beau prier, beau faire, jamais vous ne pourrez récompenser la peine que j’ai prise pour vous autres.
> 
> Je vous ai donné six jours pour travailler, je me suis réservé le septième, et on ne veut pas me l’accorder. C’est ce qui appesantit tant le bras de mon Fils.
> 
> Ceux qui conduisent les charrettes, ne savent pas parler sans y mettre le Nom de mon Fils au milieu. Ce sont les deux choses qui appesantissent tant le bras de mon Fils.
> 
> Si la récolte se gâte, ce n’est qu’à cause de vous autres.
> 
> Je vous l’ai fait voir l’année passée par les pommes de terre ; vous n’en avez pas fait cas ; c’est au contraire, quand vous en trouviez de gâtées, vous juriez, et vous mettiez le Nom de mon Fils. Elles vont continuer à se gâter, à la Noël il n’y en aura plus."

Ici je cherchais à interpréter la parole : pommes de terre ;  je croyais comprendre que cela signifiait pommes. La belle et bonne Dame devinant ma pensée reprit ainsi :

> "Vous ne comprenez pas, mes enfants ? Je vais vous le dire autrement."

La traduction en français est celle-ci :

> "Si la récolte se gâte, ce n’est rien que pour vous autres ; je vous l’ai fait voir l’année passée, par les pommes de terre, et vous n’en avez pas fait cas ; c’était, au contraire, quand vous en trouviez de gâtées, vous juriez, et vous mettiez le nom de mon Fils. Elles vont continuer à se gâter, et, à la Noël, il n’y en aura plus.
> 
> Si vous avez du blé, il ne faut pas le semer.
> 
> Tout ce que vous sèmerez, les bêtes le mangeront ; et ce qui viendra, tombera tout en poussière quand vous le battrez. Il viendra une grande famine. Avant que la famine vienne, les petits enfants au-dessous de sept ans prendront un tremblement et mourront entre les mains des personnes qui les tiendront ; les autres feront pénitence par la faim. Les noix deviendront mauvaises ; les raisins pourriront."

Ici la belle Dame qui me ravissait, resta un moment sans se faire entendre ; je voyais cependant qu’elle continuait, comme si elle parlait, de remuer gracieusement ses aimables lèvres. Maximin recevait alors son secret. Puis, s’adressant à moi, la Très Sainte Vierge me parla et me donna un secret en français. Ce secret, le voici tout entier, et tel qu’elle me l’a donné :

---

## III

>
> " Mélanie, ce que je vais vous dire maintenant, ne sera pas toujours secret ; vous pourrez le publier en 1858.
>
> Les prêtres, ministres de mon Fils, les prêtres, par leur mauvaise vie, par leurs irrévérences et leur impiété à célébrer les saints mystères, par l’amour de l’argent, l’amour de l’honneur et des plaisirs, les prêtres sont devenus des cloaques d’impureté. Oui, les prêtres demandent vengeance, et la vengeance est suspendue sur leurs têtes. Malheur aux prêtres, et aux personnes consacrées à Dieu, lesquelles, par leurs infidélités et leur mauvaise vie, crucifient de nouveau mon Fils ! Les pêchés des personnes consacrées à Dieu crient vers le Ciel et appellent la vengeance, et voilà que la vengeance est à leurs portes ; car il ne se trouve plus personne pour implorer miséricorde et pardon pour le peuple ; il n’y a plus d’âmes généreuses, il n’y a plus personne digne d’offrir la Victime sans tache à l’Éternel en faveur du monde.
>
> Dieu va frapper d’une manière sans exemple.
>
> Malheur aux habitants de la terre ! Dieu va épuiser sa colère, et personne ne pourra se soustraire à tant de maux réunis.
>
> Les chefs, les conducteurs du peuple de Dieu ont négligé la prière et la pénitence, et le démon a obscurci leurs intelligences ; ils sont devenus ces étoiles errantes que le vieux diable traînera avec sa queue pour les faire périr. Dieu permettra au vieux serpent de mettre des divisions parmi les régnants, dans toutes les sociétés et dans toutes les familles ; on souffrira des peines physiques et morales ; Dieu abandonnera les hommes à eux-mêmes, et enverra des châtiments qui se succéderont pendant plus de trente-cinq ans.
>
> La Société est à la veille des fléaux les plus terribles et des plus grands évènements ; on doit s’attendre à être gouverné par une verge de fer et à boire le calice de la colère de Dieu.
>
> Que le Vicaire de mon Fils, le Souverain Pontife Pie IX, ne sorte plus de Rome après l’année 1859 ; mais qu’il soit ferme et généreux, qu’il combatte avec les armes de la foi et de l’amour ; je serais avec lui.
>
> Qu’il se méfie de Napoléon ; son cœur est double, et quand il voudra être à la fois Pape et empereur, bientôt Dieu se retirera de lui : il est cet aigle qui, voulant s’élever, tombera sur l’épée dont il voulait se servir pour obliger les peuples à se faire élever.
>
> L’Italie sera punie de son ambition en voulant secouer le joug du Seigneur des Seigneurs ; aussi elle sera livrée à la guerre ; le sang coulera de tous côtés : les Églises seront fermés ou profanées ; les prêtres, les religieux seront chassés ; on les fera mourir, et mourir d’une mort cruelle. Plusieurs abandonneront la foi, et le nombre des prêtres et des religieux qui se sépareront de la vraie religion sera grand ; parmi ces personnes il se trouvera même des Évêques.
>
> Que le Pape se tienne en garde contre les faiseurs de miracles, car le temps est venu que les prodiges les plus étonnants auront lieu sur la terre et dans les airs.
>
> En l’année 1864, Lucifer avec un grand nombre de démons seront détachés de l’enfer : ils aboliront la foi peu à peu et même dans les personnes consacrées à Dieu ; ils les aveugleront d’une telle manière, qu’à moins d’une grâce particulière ces personnes prendront l’esprit de ces mauvais anges ; plusieurs maisons religieuses perdront entièrement la foi et perdront beaucoup d’âmes.
>
> Les mauvais livres abonderont sur la terre, et les esprits des ténèbres répandront partout un relâchement universel pour tout ce qui regarde le service de Dieu ; ils auront un très grand pouvoir sur la nature ; il y aura des églises pour servir ces esprits. Des personnes seront transportées d’un lieu à un autre par ces esprits mauvais et même des prêtres, parce qu’ils ne seront pas conduits par le bon esprit de l’Évangile, qui est un esprit d’humilité, de charité et de zèle pour la gloire de Dieu. On fera ressusciter des morts et des justes " *(c’est-à-dire que ces morts prendront la figure des âmes justes qui avaient vécu sur la terre, afin de mieux séduire les hommes ; ces soi-disant morts ressuscités, qui ne seront autre chose que le démon sous ses figures, prêcheront un autre Évangile, contraire à celui du vrai Christ-Jésus, niant l’existence du Ciel, soit encore les âmes des damnés. Toutes ces âmes paraîtront comme unies à leurs corps)*.  " Il y aura en tous lieux des prodiges extraordinaires, parce que la vraie foi s’est éteinte et que la fausse lumière éclaire le monde. Malheur aux princes de l’Église qui ne seront occupés qu’à entasser richesses sur richesses, qu’à sauvegarder leur autorité et à dominer avec orgueil !
>
> Le Vicaire de mon fils aura beaucoup à souffrir, parce que pour un temps l’Église sera livrée à de grandes persécutions : ce sera le temps des ténèbres ; l’Église aura une crise affreuse.
>
> La sainte foi de Dieu étant oubliée, chaque individu voudra se guider par lui-même et être supérieur à ses semblables. On abolira les pouvoirs civils et ecclésiastique, tout ordre et toute justice seront foulés aux pieds ; on ne verra qu’homicides, haine, jalousie, mensonge et discorde, sans amour pour la patrie ni pour la famille. 
>
> Le Saint-Père souffrira beaucoup. Je serai avec lui jusqu’à la fin pour recevoir son sacrifice.
>
> Les méchants attenteront plusieurs fois à sa vie sans pouvoir nuire à ses jours ; mais ni lui, ni son successeur " *(qui ne régnera pas longtemps)* ", ne verront le triomphe de l’Église de Dieu.
>
> Les gouvernants civils auront tous un même dessein, qui sera d’abolir et de faire disparaître tout principe religieux, pour faire place au matérialisme, à l’athéisme, au spiritisme et à toutes sortes de vices.
>
> Dans l’année 1865, on verra l’abomination dans les lieux saints ; dans les couvents, les fleurs de l’Église seront putréfiées et le démon se rendra comme le roi des cœurs. Que ceux qui sont à la tête des communautés religieuses se tiennent en garde pour les personnes qu’ils doivent recevoir, parce que le démon usera de toute sa malice pour introduire dans les ordres religieux des personnes adonnées au péché, car les désordres et l’amour des plaisirs charnels seront répandus par toute la terre.
>
> La France, l’Italie, l’Espagne et l’Angleterre seront en guerre ; le sang coulera dans les rues ; le Français se battra avec le Français, l’Italien avec l’Italien ; ensuite il y aura une guerre générale qui sera épouvantable. Pour un temps, Dieu ne se souviendra plus de la France ni de l’Italie, parce que l’Évangile de Jésus-Christ n’est plus connu. Les méchants déploieront toute leur malice ; on se tuera, on se massacrera mutuellement jusque dans les maisons.
>
> Au premier coup de son épée foudroyante, les montagnes et la nature entière trembleront d’épouvante, parce que les désordres et les crimes des hommes percent la voûte des cieux. Paris sera brûlé et Marseille englouti ; plusieurs grandes villes seront ébranlées et englouties par des tremblements de terre ; on croira que tout est perdu ; on ne verra qu’homicides, on n’entendra que bruits d’armes et que blasphèmes. Les justes souffriront beaucoup ; leurs prières, leur pénitence et leurs larmes monteront jusqu’au Ciel, et tout le peuple de Dieu demandera pardon et miséricorde, et demandera mon aide et mon intercession. Alors Jésus-Christ, par un acte de sa justice et de sa grande miséricorde pour les justes, commandera à ses anges que tous ses ennemis soient mis à mort. Tout à coup les persécuteurs de l’Église de Jésus-Christ et tous les hommes adonnés au péché périront, et la terre deviendra comme un désert. Alors se fera la paix, la réconciliation de Dieu avec les hommes ; Jésus-Christ sera servi, adoré et glorifié ; la charité fleurira partout. Les nouveaux rois seront le bras droit de la Sainte Église, qui sera forte, humble, pieuse, pauvre, zélée et imitatrice des vertus de Jésus-Christ. L’Évangile sera prêché partout, et les hommes feront de grands progrès dans la foi, parce qu’il y aura unité parmi les ouvriers de Jésus-Christ et que les hommes vivront dans la crainte de Dieu.
>
> Cette paix parmi les hommes ne sera pas longue : vingt-cinq ans d’abondantes récoltes leur feront oublier que les péchés des hommes sont cause de toutes les peines qui arrivent sur la terre.
>
> Un avant-coureur de l’antéchrist, avec ses troupes de plusieurs  nations, combattra contre le vrai Christ, le seul Sauveur du monde ; il répandra beaucoup de sang et voudra anéantir le culte de Dieu pour se faire regarder comme un Dieu.
>
> La terre sera frappée de toutes sortes de plaies " *(outre la peste et la famine, qui seront générales)* " ; il y aura des guerres jusqu’à la dernière guerre, qui sera alors faite par les dix rois de l’antéchrist, lesquels rois auront tous un même dessein et seront les seuls qui gouverneront le monde. Avant que ceci arrive, il y aura une espèce de fausse paix dans le monde ; on ne pensera qu’à se divertir ; les méchants se livreront à toutes sortes de péchés ; mais les enfants de la Sainte Église, les enfants de la foi, mes vrais imitateurs, croîtront dans l’amour de Dieu et dans les vertus qui me sont les plus chères. Heureuses les âmes humbles conduites par l’Esprit-Saint ! Je combattrai avec elles jusqu’à ce qu’elles arrivent à la plénitude de l’âge.
>
> La nature demande vengeance pour les hommes, et elle frémit d’épouvante dans l’attente de ce qui doit arriver à la terre souillée de crimes.
>
> Tremblez, terre, et vous qui faites profession de servir Jésus-Christ et qui au-dedans vous adorez vous-mêmes, tremblez ; car Dieu va vous livrer à son ennemi, parce que les lieux saints sont dans la corruption ; beaucoup de couvents ne sont plus les maisons de Dieu, mais les pâturages d’Asmodée et des siens.
>
> Ce sera pendant ce temps que naîtra l’antéchrist, d’une religieuse hébraïque, d’une fausse vierge qui aura communication avec le vieux serpent, le maître de l’impureté ; son père sera Ev. ; en naissant il vomira des blasphèmes, il aura des dents ; en un mot il sera le diable incarné ; il poussera des cris effrayants, il fera des prodiges, il ne se nourrira que d’impuretés. Il aura des frères qui, quoiqu’ils ne soient pas comme lui des démons incarnés, seront des enfants de mal ; à 12 ans, ils se feront remarquer par leurs vaillantes victoires qu’ils remporteront ; bientôt ils seront chacun à la tête des armées, assistés par des légions de l’enfer.
>
> Les saisons seront changées, la terre ne produira que de mauvais fruits, les astres perdront leurs mouvements réguliers, la lune ne reflétera qu’une faible lumière rougeâtre ; l’eau et le feu donneront au globe de la terre des mouvements convulsifs et d’horribles tremblements de terre, qui feront engloutir des montagnes, des villes *(etc.)*.
>
> Rome perdra la foi et deviendra le siège de l’antéchrist.
>
> Les démons de l’air avec l’antéchrist feront de grands prodiges sur la terre et dans les airs, et les hommes se pervertiront de plus en plus. Dieu aura soin de ses fidèles serviteurs et des hommes de bonne volonté ; l’Évangile sera prêché partout, tous les peuples et toutes les nations auront connaissance de la vérité !
>
> J’adresse un pressant appel à la terre : j’appelle les vrais disciples du Dieu vivant et régnant dans les cieux ; j’appelle les vrais imitateurs du Christ fait homme, le seul et vrai Sauveur des hommes ; j’appelle mes enfants, mes vrais dévots, ceux qui se sont donnés à moi pour que je les conduise à mon divin Fils, ceux que je porte pour ainsi dire dans mes bras, ceux qui ont vécu de mon esprit ; enfin j’appelle les Apôtres des derniers temps, les fidèles disciples de Jésus-Christ qui ont vécu dans un mépris du monde et d’eux-mêmes, dans la pauvreté et dans l’humilité, dans le mépris et dans le silence, dans l’oraison et dans la mortification, dans la chasteté et dans l’union avec Dieu, dans la souffrance et inconnus du monde. Il est temps qu’ils sortent et viennent éclairer la terre. Allez, et montrez-vous comme mes enfants chéris ; je suis avec vous et en vous, pourvu que votre foi soit la lumière qui vous éclaire dans ces jours de malheurs. Que votre zèle vous rende comme des affamés pour la gloire et l’honneur de Jésus-Christ. Combattez, enfants de lumière, vous, petit nombre qui y voyez ; car voici le temps des temps, la fin des fins.
>
> L’Église sera éclipsée, le monde sera dans la consternation. Mais voilà Enoch et Élie remplis de l’Esprit de Dieu ; ils prêcheront avec la force de Dieu, et les hommes de bonne volonté croiront en Dieu, et beaucoup d’âmes seront consolées ; ils feront de grands progrès par la vertu du Saint-Esprit et condamneront les erreurs diaboliques de l’antéchrist.
>
> Malheur aux habitants de la terre ! Il y aura des guerres sanglantes et des famines ; des pestes et des maladies contagieuses ; il y aura des pluies d’une grêle effroyable d’animaux ; des tonnerres qui ébranleront des villes ; des tremblements de terre qui engloutirons des pays ; on entendra des voix dans les airs ; les hommes se battront la tête contre les murailles ; ils appelleront la mort, et d’un autre côté la mort fera leur supplice ; le sang coulera de tous côtés. Qui pourra vaincre, si Dieu ne diminue pas le temps de l’épreuve ? Par le sang, les larmes, et les prières des justes, Dieu se laissera fléchir ; Enoch et Elie seront mis à mort ; Rome païenne disparaîtra : le feu du Ciel tombera et consumera trois villes ; tout l’univers sera frappé de terreur, et beaucoup se laisseront séduire parce qu’ils n’ont pas adoré le vrai Christ vivant parmi eux. Il est temps ; le soleil s’obscurcit ; la foi seule vivra.
>
> Voici le temps ; l’abîme s’ouvre. Voici le roi des rois des ténèbres. Voici la bête avec ses sujets, se disant le sauveur du monde. Il s’élèvera avec orgueil dans les airs pour aller jusqu’au Ciel ; il sera étouffé par le souffle de saint Michel Archange. Il tombera, et la terre qui, depuis trois jours sera en de continuelles évolutions, ouvrira son sein plein de feu ; il sera plongé pour jamais avec tous les siens dans les gouffres éternels de l’enfer. Alors l’eau et le feu purifieront la terre et consumeront toutes les œuvres de l’orgueil des hommes, et tout sera renouvelé : Dieu sera servi et glorifié."

---

## IV


Ensuite **la Sainte Vierge me donna, aussi en français, la Règle d’un nouvel Ordre religieux.*8

Après m’avoir donné la Règle de ce nouvel Ordre religieux, la Sainte Vierge reprit ainsi la suite du Discours :

> "S’ils se convertissent, les pierres et les rochers se changeront en blé, et les pommes de terre se trouveront ensemencées par les terres.
>
> Faites-vous bien votre prière mes enfants ? "

Nous répondîmes tous les deux :

"Oh ! non, Madame, pas beaucoup."

> "Ah ! mes enfants, il faut bien la faire, soir et matin. Quand vous ne pourrez pas mieux faire, dites un Pater et un Ave Maria ; et quand vous aurez le temps et que vous pourrez mieux faire, vous en direz davantage.
>
> Il ne va que quelques femmes un peu âgées à la Messe ; les autres travaillent tout l’été le Dimanche ; et l’hiver, quand ils ne savent que faire, ils ne vont à la Messe que pour se moquer de la religion. Le carême, ils vont à la boucherie comme les chiens.
>
> N’avez-vous pas vu du blé gâté, mes enfants ? "

Tous les deux nous avons répondu : " Oh ! non, Madame."

> *La Sainte Vierge s’adressant à Maximin :* " Mais toi, mon enfant, tu dois bien en avoir vu une fois vers le Coin, avec ton père. L’homme de la pièce dit à ton père : Venez voir comme mon blé se gâte. Vous y allâtes. Ton père prit deux ou trois épis dans sa main, il les frotta, et ils tombèrent en poussière. Puis, en vous en retournant, quand vous n’étiez plus qu’à demi-heure de Corps, ton père te donna un morceau de pain en te disant : Tiens, mon enfant, mange cette année, car je ne sais pas qui mangera l’année prochaine, si le blé se gâte comme cela."

Maximin répondit : " C’est bien vrai, Madame, je ne me le rappelais pas."

> *La Très Sainte Vierge a terminé son discours en français :* "Eh bien ! mes enfants, vous le ferez passer à tout mon peuple."

La très belle Dame traversa le ruisseau ; et, à deux pas du ruisseau, sans se retourner vers nous qui la suivions (parce qu’elle attirait à elle par son éclat et plus encore par sa bonté qui m’enivrait, qui semblait me faire fondre le cœur), elle nous a dit encore :

> "Eh bien ! mes enfants, vous le ferez passer à tout mon peuple."

Puis elle a continué de marcher jusqu’à l’endroit où j’étais montée pour regarder où étaient nos vaches. Ses pieds ne touchaient que le bout de l’herbe sans le faire plier. Arrivée sur la petite hauteur, la belle Dame s’arrêta, et vite je me plaçai devant elle, pour bien, bien la regarder, et tâcher de savoir quel chemin elle inclinait le plus à prendre : car c’était fait de moi, j’avais oublié et mes vaches et les maîtres chez lesquels j’étais en service ; je m’étais attachée pour toujours et sans condition à Ma Dame ; oui, je voulais ne plus jamais, jamais la quitter ; je la suivais sans arrière-pensée, et dans la disposition de la servir tant que je vivrai.

Avec Ma Dame je croyais avoir oublié le paradis ; je n’avais plus que la pensée de bien la servir en tout : et je croyais que j’aurais pu faire tout ce qu’Elle m’aurait dit de faire, car il me semblait qu’Elle avait beaucoup de pouvoir. Elle me regardait avec une tendre bonté qui m’attirait à Elle ; j’aurais voulu, avec les yeux fermés, m’élancer dans ses bras. Elle ne m’a pas donné le temps de le faire. Elle s’est élevée insensiblement de terre à une hauteur d’environ un mètre et plus ; et restant ainsi suspendue en l’air un tout petit instant, Ma belle Dame regarda le ciel, puis la terre à sa droite et à sa gauche, puis Elle me regarda avec des yeux si doux, si aimables et si bons, que je croyais qu’Elle m’attirait dans son intérieur, et il me semblait que mon cœur s’ouvrait au sien.

Et, tandis que mon cœur se fondait en une douce dilatation, la belle figure de Ma bonne Dame disparaissait peu à peu : il me semblait que la lumière en mouvement se multipliait ou bien se condensait autour de la Très Sainte Vierge, pour m’empêcher de la voir plus longtemps. Ainsi la lumière prenait la place des parties du corps qui disparaissaient à mes yeux ; ou bien il semblait que le corps de Ma Dame se changeait en lumière en se fondant. Ainsi la lumière en forme de globe s’élevait doucement en direction droite.

Je ne puis dire si le volume de lumière diminuait à mesure qu’elle s’élevait, ou bien si c’était l’éloignement qui faisait que je voyais diminuer la lumière à mesure qu’elle s’élevait ; ce que je sais, c’est que je suis restée la tête levée et les yeux fixés sur la lumière, même après que cette lumière, qui allait toujours s’éloignant et diminuant de volume, eut fini par disparaître.

Mes yeux se détachent du firmament, je regarde autour de moi, je vois Maximin qui me regardait, je lui dis : "Mémin, cela doit être le bon Dieu de mon père, ou la Sainte Vierge, ou quelque grande sainte". Et Maximin lançant la main en l’air, il dit : "Ah ! si je l’avais su !"



---

## V



Le soir du 19 septembre, nous nous retirâmes un peu plus tôt qu’à l’ordinaire. Arrivées chez mes maîtres, je m’occupais à attacher mes vaches et à mettre tout en ordre dans l’écurie. Je n’avais pas terminé, que ma maîtresse vint à moi en pleurant et me dit : "Pourquoi, mon enfant, ne venez-vous pas me dire ce qui vous est arrivé sur la montagne ? " (Maximin n’ayant pas trouvé ses maîtres, qui ne s’étaient pas encore retirés de leurs travaux, était venu chez les miens, et avait raconté tout ce qu’il avait vu et entendu). Je lui répondis : "Je voulais bien vous le dire, mais je voulais finir mon ouvrage auparavant." Un moment après, je me rendis dans la maison, et ma maîtresse me dit : "Racontez ce que vous avez vu ; le berger de Bruite (c’était le surnom de Pierre Selme, maître de Maximin) m’a tout raconté."

Je commence et, vers la moitié du récit, mes maîtres arrivèrent de leurs champs ; ma maîtresse, qui pleurait en entendant les plaintes et les menaces de notre tendre Mère, dit : "Ah ! vous vouliez aller ramasser le blé demain ; gardez-vous en bien, venez entendre ce qui est arrivé aujourd’hui à cette enfant et au berger de Selme." Et, se tournant vers moi, elle dit : "Recommencez tout ce que vous m’avez dit." Je recommence ; et lorsque j’eus terminé, mon maître me dit : "C’était la Sainte Vierge, ou bien une grande sainte, qui est venue de la part du bon Dieu ; mais c’est comme si le bon Dieu était venu lui-même : il faut faire tout ce que cette sainte à dit. Comment allez-vous faire pour dire cela à tout son peuple ? " Je lui répondis : "Vous me direz comment je dois faire, et je le ferai." Ensuite il ajouta, en regardant sa mère, sa femme et son frère : "Il faut y penser." Puis chacun se retira à ses affaires.

C’était après le souper. Maximin et ses maîtres vinrent chez les miens pour raconter ce que Maximin leur avait dit, et pour savoir ce qu’il y avait à faire : "Car, dirent-ils, il nous semble que c’est la Sainte Vierge qui a été envoyée par le bon Dieu ; les paroles qu’Elle a dites le font croire. Et Elle leur a dit de le faire passer à tout son peuple ; il faudra peut-être que ces enfants parcourent le monde entier pour faire connaître qu’il faut que tout le monde observe les commandements du bon Dieu, sinon de grands malheurs vont arriver sur nous." Après un moment de silence, mon maître dit, en s’adressant à Maximin et à moi : "Savez-vous ce que vous devez faire, mes enfants ? Demain, levez-vous de bon matin, allez tous deux à Monsieur le Curé, et racontez-lui tout ce que vous avez vu et entendu ; dites-lui bien comment la chose s’est passée : il vous dira ce que vous avez à faire."

Le 20 septembre, lendemain de l’apparition, je partis de bonne heure avec Maximin. Arrivés à la Cure, je frappe à la porte. La domestique de Monsieur le Curé vint ouvrir, et demanda ce que nous voulions. Je lui dis (en français, moi qui ne l’avais jamais parlé) : "Nous voudrions parler à Monsieur le Curé." - "Et que voulez-vous lui dire ? ", nous demanda-t-elle. – " Nous voulons lui dire, Mademoiselle, qu’hier nous sommes allés garder nos vaches sur la montagne des Baisses, et après avoir dîné, etc., etc." Nous lui racontâmes une bonne partie du discours de la Très Sainte Vierge. Alors la cloche de l’Église sonna ; c’était le dernier coup de la Messe. M. l’abbé Perrin, curé de la Salette, qui nous avait entendus, ouvrit la porte avec fracas : "Mes enfants, nous sommes perdus, le bon Dieu va nous punir. Ah ! mon Dieu, c’est la Sainte Vierge qui vous est apparue !" Et il est partit pour dire la Sainte Messe. Nous nous regardâmes avec Maximin et la domestique ; puis Maximin me dit : "Moi, je m’en vais chez mon père, à Corps." Et nous nous séparâmes.

N’ayant pas reçu d’ordre de mes maîtres de me retirer aussitôt après avoir parlé à Monsieur le Curé, je ne crus pas faire mal en assistant à la Messe. Je fus donc à l’Église. La Messe commence, et, après le premier Évangile, Monsieur le Curé se tourne vers le peuple et essaie de raconter à ses paroissiens l’apparition qui venait d’avoir lieu, la veille, sur une de leurs Montagnes, et le exhorte à ne plus travailler le Dimanche : sa voix était entrecoupée de sanglots, et tout le peuple était ému. Après la Sainte Messe, je me retirai chez mes maîtres, Monsieur Peytard, qui est encore aujourd’hui Maire de la Salette, y vint m’interroger sur le fait de l’apparition ; et, après s’être assuré de la vérité de ce que je lui disais, il se retira convaincu.

Je continuai de rester au service de mes maîtres jusqu’à la fête de la Toussaint. Ensuite je fus mise comme pensionnaire chez les religieuses de la Providence, dans mon pays, à Corps.



---

## VI



La Très Sainte Vierge était très grande et bien proportionnée ; elle paraissait être si légère qu’avec un souffle on l’aurait fait remuer, cependant, elle était immobile et bien posée. Sa physionomie était majestueuse, imposante, mais non imposante comme le sont les seigneurs d’ici-bas. Elle imposait une crainte respectueuse. En même temps que Sa Majesté imposait du respect mêlé d’amour, elle attirait à elle. Son regard était doux et pénétrant ; ses yeux semblaient parler avec les miens, mais la conversation venait d’un profond et vif sentiment d’amour envers cette beauté ravissante qui me liquéfiait. La douceur de son regard, son air de bonté incompréhensible faisait comprendre et sentir qu’elle attirait à elle et voulait se donner ; c’était une expression d’amour qui ne peut pas s’exprimer avec la langue de chair ni avec les lettres de l’alphabet.

Le vêtement de la Très Sainte Vierge était blanc argenté et tout brillant ; il n’avait rien de matériel : il était composé de lumière et de gloire, variant et scintillant. Sur la terre il n’y a pas d’expression ni de comparaison à donner.

La Sainte Vierge était toute belle et toute formée d’amour ; en la regardant, je languissais de me fondre en elle. Dans ses atours, comme dans sa personne, tout respirait la majesté, la splendeur, la magnificence d’une Reine incomparable. Elle paraissait belle, blanche, immaculée, cristallisée, éblouissante, céleste, fraîche, neuve comme une Vierge ; il semblait que la parole : Amour s’échappait de ses lèvres argentées et toutes pures. Elle me paraissait comme une bonne Mère, pleine de bonté, d’amabilité, d’amour pour nous, de compassion, de miséricorde.

La couronne de roses qu’elle avait sur la tête était si belle, si brillante, qu’on ne peut pas s’en faire une idée : les roses, de diverses couleurs, n’étaient pas de la terre ; c’était une réunion de fleurs qui entouraient la tête de la Très Sainte Vierge en forme de couronne ; mais les roses se changeaient ou se remplaçaient ; puis, du cœur de chaque rose, il sortait une si belle lumière, qu’elle ravissait, et rendait les roses d’une beauté éclatante. De la couronne de roses s’élevaient comme des branches d’or, et une quantité d’autres petites fleurs mêlées avec des brillants.

Le tout formait un très beau diadème, qui brillait tout seul plus que notre soleil de la terre.

La Sainte Vierge avait une très jolie Croix suspendue à son cou. Cette Croix paraissait être dorée, je dis dorée pour ne pas dire une plaque d’or ; car j’ai vu quelquefois des objets dorés avec diverses nuances d’or, ce qui faisait à mes yeux un bien plus bel effet qu’une simple plaque d’or. Sur cette belle Croix toute brillante de lumière était un Christ, était Notre-Seigneur, les bras étendus sur la Croix. Presque aux deux extrémités de la Croix, d’un côté il y avait un marteau, de l’autre une tenaille. Le Christ était couleur de chair naturelle ; mais il brillait d’un grand éclat ; et la lumière qui sortait de tout son corps paraissait comme des dards très brillants, qui me fendaient le cœur du désir de me fondre en lui. Quelquefois le Christ paraissait être mort : il avait la tête penchée, et le corps était comme affaissé, comme pour tomber, s’il n’avait pas été retenu par les clous qui le retenaient à la Croix.

J’en avais une vive compassion, et j’aurais voulu redire au monde entier son amour inconnu et infiltrer dans les âmes des mortels l’amour le plus senti et la reconnaissance la plus vive envers un Dieu qui n’avait nullement besoin de nous pour être ce qu’il est, ce qu’il était et ce qu’il sera toujours ; et pourtant, ô amour incompréhensible à l’homme ! il s’est fait homme, et il a voulu mourir, oui mourir, pour mieux écrire dans nos âmes et dans notre mémoire l’amour Fou qu’il a pour nous ! Oh ! que je suis malheureuse de me trouver si pauvre en expression pour redire l’amour, oui l’amour de notre bon Sauveur pour nous ! mais, d’un autre côté, que nous sommes heureux de pouvoir sentir mieux ce que nous ne pouvons exprimer !

D’autres fois, le Christ semblait vivant ; il avait la tête droite, les yeux ouverts, et paraissait être sur la Croix, par sa propre volonté. Quelquefois aussi il paraissait parler : il semblait vouloir montrer qu’il était en Croix pour nous, par amour pour nous, pour nous attirer à son amour, qu’il a toujours un amour nouveau pour nous, que son amour du commencement et de l’année 33 est toujours celui d’aujourd’hui et qu’il sera toujours.

La Sainte Vierge pleurait presque tout le temps qu’Elle me parla. Ses larmes coulaient, une à une, lentement, jusque vers ses genoux ; puis, comme des étincelles de lumière, elles disparaissaient. Elles étaient brillantes et pleines d’amour. J’aurais voulu la consoler, et qu’Elle ne pleurât plus. Mais il me semblait qu’Elle avait besoin de montrer ses larmes pour mieux montrer son amour oublié par les hommes. J’aurais voulu me jeter dans ses bras et lui dire : "Ma bonne Mère, ne pleurez pas ! je veux vous aimer pour tous les hommes de la terre." Mais il me semblait qu’Elle me disait : "Il y en a tant qui ne me connaissent pas !"

J’étais entre la mort et la vie, en voyant d’un côté tant d’amour, tant de désir d’être aimée, et d’un autre côté tant de froideur, tant d’indifférence… Oh ! ma Mère, Mère toute toute belle et toute aimable, mon amour, cœur de mon cœur !...

Les larmes de notre tendre Mère, loin d’amoindrir son air de Majesté, de Reine et de Maîtresse, semblaient, au contraire, l’embellir, la rendre plus aimable, plus belle, plus puissante, plus remplie d’amour, plus maternelle, plus ravissante ; et j’aurais mangé ses larmes, qui faisaient sauter mon cœur de compassion et d’amour. Voir pleurer une Mère, et une telle Mère, sans prendre tous les moyens imaginables pour la consoler, pour changer ses douleurs en joie, cela se comprend-il ! O Mère plus que bonne ! Vous avez été formée de toutes les prérogatives dont Dieu est capable ; vous avez comme épuisé la puissance de Dieu ; vous êtes bonne et puis bonne de la bonté de Dieu même ; Dieu s’est agrandi en vous formant son chef-d’œuvre terrestre et céleste.

La Très Sainte Vierge avait un tablier jaune. Que dis-je, jaune ? Elle avait un tablier plus brillant que plusieurs soleils ensemble. Ce n’était pas une étoffe matérielle, c’était un composé de gloire, et cette gloire était scintillante et d’une beauté ravissante. Tout en la Très Sainte Vierge me portait fortement, et me faisait comme glisser à adorer et à aimer mon Jésus dans tous les états de sa vie mortelle.

La Très Sainte Vierge avait deux chaînes, l’une un peu plus large que l’autre. A la plus étroite était suspendue la Croix dont j’ai fait mention plus haut. Ces chaînes (puisqu’il faut donner le nom de chaînes) étaient comme des rayons de gloire d’un grand éclat variant et scintillant.

Les souliers (puisque souliers il faut dire) étaient blancs, mais un blanc argenté, brillant ; il y avait des roses autour. Ces roses étaient d’une beauté éblouissante, et du cœur de chaque rose sortait une flamme de lumière très belle et très agréable à voir. Sur les souliers, il y avait une boucle en or, non en or de la terre, mais bien de l’or du paradis.

La vue de la Très Sainte Vierge était elle-même un paradis accompli. Elle avait en Elle tout ce qui pouvait satisfaire, car la terre était oubliée.

La Sainte Vierge était entourée de deux lumières. La première lumière, plus près de la Très Sainte Vierge, arrivait jusqu’à nous ; elle brillait d’un éclat très beau et scintillant. La seconde lumière s’étendait un peu plus autour de la Belle-Dame, et nous nous trouvions dans celle-là ; elle était immobile (c’est-à-dire qu’elle ne scintillait pas), mais bien plus brillante que notre pauvre soleil de la terre. Toutes ces lumières ne faisaient pas mal aux yeux, et ne fatiguaient nullement la vue.

Outre toutes ces lumières, toute cette splendeur, il sortait encore des groupes ou faisceaux de lumières ou des rayons de lumière, du Corps de la Sainte Vierge, de ses habits et de partout.

La voix de la Belle Dame était douce ; elle enchantait, ravissait, faisait du bien au cœur ; elle rassasiait, aplanissait tous les obstacles, calmait, adoucissait. Il me semblait que j’aurais toujours voulu manger de sa belle voix, et mon cœur semblait danser ou vouloir aller à sa rencontre pour se liquéfier en elle.

Les yeux de la Très Sainte Vierge, notre tendre Mère, ne peuvent pas se décrire par une langue humaine. Pour en parler, il faudrait un séraphin ; il faudrait plus, il faudrait le langage de Dieu même, de ce Dieu qui a formé la Vierge Immaculée, chef-d’œuvre de toute sa puissance.

Les yeux de l’auguste Marie paraissaient mille et mille fois plus beaux que les brillants, les diamants et les pierres précieuses les plus recherchées ; ils brillaient comme deux soleils ; ils étaient doux de la douceur même, clairs comme un miroir. Dans ses yeux on voyait le paradis ; ils attiraient à  Elle ; il semblait qu’Elle voulait se donner et attirer. Plus je la regardais, plus je la voulais voir ; plus je la voyais, plus je l’aimais, et je l’aimais de toutes mes forces.

Les yeux de la belle Immaculée étaient comme la porte de Dieu, d’où l’on voyait tout ce qui peut enivrer l’âme. Quand mes yeux se rencontraient avec ceux de la Mère de Dieu et la mienne, j’éprouvais au-dedans de moi-même une heureuse révolution d’amour et de protestation de l’aimer et de me fondre d’amour.

En nous regardant, nos yeux se parlaient à leur mode, et je l’aimais tant que j’aurais voulu l’embrasser dans le milieu de ses yeux, qui attendrissaient mon âme et semblaient l’attirer et la faire fondre avec la sienne. Ses yeux me plantèrent un doux tremblement dans tout mon être ; et je craignais de faire le moindre mouvement qui pût lui être désagréable tant soit peu.

Cette seule vue des yeux de la plus pure des Vierges aurait suffi pour être le Ciel d’un bienheureux ; aurait suffi pour faire entrer une âme dans la plénitude des volontés du Très-Haut parmi tous les événements qui arrivent dans le cours de la vie mortelle ; aurait suffi pour faire faire à cette âme de continuels actes de louange, de remerciement, de réparation et d’expiation. Cette seule vue concentre l’âme en Dieu et la rend comme une morte-vivante, ne regardant toutes les choses de la terre, même les choses qui paraissent les plus sérieuses, que comme des amusements d’enfants ; elle ne voudrait entendre parler que de Dieu et de ce qui touche à sa Gloire.

Le péché est le seul mal qu’Elle voit sur la terre. Elle mourrait de douleur si Dieu ne la soutenait. Amen

---

Castellamare, le 21 Novembre 1878

Marie de la Croix, Victime de Jésus, \
née Mélanie Calvat, Bergère de la Salette

<center>{{< imgresize "/livres/brochure-1879/lettre-manuscrite.jpeg" "300x" >}}</center>

---

Nihil Obstat: Imprimatur datum Lycii ex Curia Episc. \
die 15 Nov. 1879 \
Vicarius Generalis - Carmelus Archus Cosma



