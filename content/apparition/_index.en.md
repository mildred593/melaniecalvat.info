---
menu: true
menutitle: The Apparition
title: The Apparition of the Very Holy Virgin on the Mountain of la Salette on the 19th of September, 1846
weight: 3
---

- [Download the text](/apparition/apparition%20anglais%20A5.odt)
- [A5 formatted leaflet](/apparition/livret%20apparition%20EN_A5.pdf) with a [print-ready version in A4 landscape to fold into a booklet](/apparition/livret%20apparition%20EN_A4_booklet.pdf)

*Published and written by the Shepherdess of la Salette; with Imprimatur from the Bishop of Lecce.*

## I

On the 18th of September, the eve of the Holy Apparition of the Holy Virgin, I was alone, as usual, minding my Master’s four cows. Around eleven o’clock in the morning, I saw a small boy, walking towards me. I was frightened at this, for it seemed to me that everyone ought to know that I avoided all kinds of company. This child came up to me and said: “Little girl, I’m coming with you, I’m from Corps too.” At these words, my foul temper soon showed itself, and taking a few steps back, I told him: “I don’t want anybody. I want to be alone.” Then, I walked away, but this child followed me, saying to me: “Come on, let me be with you. My Master told me to come and watch over my cows together with yours. I’m from Corps.”

I walked away from him, gesturing that I wanted to be left alone; and when I was some distance away, I sat down on the grass. There, I talked with the little flowers of the Good God.

A moment later, I look behind me, and I found Maximin sitting all close to me. Straightway he said to me: “Keep me, I will be well-behaved.” But my foul temper didn’t listen to reason. I jumped to my feet, and run away a little farther off without saying a word and I started playing with the little flowers of the Good Lord again. After a short while, Maximin was there again telling me that he would be well-behaved, that he wouldn’t talk, that he would get bored all by himself, and that his Master had sent him to be at my side, etc… This time, I took pity on him, I showed him to sit, and me, I kept on playing with the little flowers of the Good Lord.

It wasn’t long before Maximin broke the silence, he started laughing (I believe he was making fun of me); I look at him and he says to me: “Let’s have fun, let’s make up a game”. I said nothing in reply because I was so ignorant that I didn’t understand what a game with another person meant, having always been by myself. I was having fun by myself with the flowers, and Maximin, getting all nearby me, was only laughing, telling me that the flowers didn’t have ears to hear me, and that we had to play together. But I had no liking for the game he was telling me to do. However, I started talking to him, and he told me that the ten days he had to spend with his Master would soon be over, and that he would then go to Corps at his father’s home, etc …

While he was talking to me, the bell of La Salette rang, it was the Angelus. I gestured to Maximin to raise his soul to God. He uncovered his head and kept silent for a while. Then, I told him: “Do you want to dine?” “Yes, he replied, let’s dine.” We sat down; I took out of my bag the provisions my Master had given me, and according to my habit, before breaking into my little round loaf, I made with the point of my knife a cross on my bread, and a little hole in the middle, saying: “If the devil’s in there, he shall leave, and if the good God is in there, he shall stay!” and quickly, quickly, I covered up the little hole. Maximin burst out laughing and kicked my bread which got out of my hands, rolled down to the bottom of the mountain and got lost. I had another piece of bread, we ate it together; afterwards we played a game; then understanding that Maximin needed to eat, I indicated to him a place on the mountain covered by little fruits. I urged him to go and eat some, which he did straight-away; he ate some and brought back a lot in his hat. In the evening we walked down the mountain together, and we promised each other to come back and watch over our cows together.

The next day, the 19th of September, I was back on the way up with Maximin; we climbed up the mountain together. I realized that Maximin was a very good, very simple guy and that he would willingly talk about what I wanted to talk about; he was also very flexible, not holding up to his feelings. He was only a little curious, for, when I was getting further away from him, as soon as he would see me stopping, he would run over to see what I was doing, and listen to what I was saying to the flowers of the good God; and if he didn’t arrive in time, he would ask me what I had said.

It was already late forenoon when Maximin asked me to teach him a game. I told him to gather some flowers to build the “Paradise”. We both got down to work and soon had a good amount of flowers from various colors. The Angelus of the Village could be heard because the sky was beautiful, there were no clouds. Having told the Good God what we knew, I said to Maximin that we had to lead our cows on a little plateau near the small ravine where there would be stones to build the “Paradise”. We led our cows to the designated place, and then we had our little meal; then, we set to carry stones and to build our little house, it comprised a ground floor, which was our so-called “home”, and then a story above which we called the “Paradise”.

This story was all garnished with flowers of different colors, with garlands hung by flower stalks. This “Paradise” was covered by a single and wide stone which we had covered with flowers; we had also hung garlands all around. Once we had finished, we contemplated repeatedly the Paradise;  we felt sleepy; we went away a few steps from there, and we fell asleep on the grass.

---

## II


When I woke up, I couldn’t see the cows, I called Maximin and climbed the little hillock. From there, I saw our cows lying down peacefully, I was coming down again and Maximin was going up, when suddenly I saw a beautiful light, brighter than the sun, and I hardly had managed to say these words “Maximin, can you see, over there? Oh! Goodness!” At the same time, I dropped the stick I was holding. I don’t know what so delightful was happening in me at that time, but I was feeling attracted, I was feeling a great respect full of love, and my heart would have wanted to run faster than me.

I looked fixedly at this light which was still, and as if it were open, I perceived another light much brighter and which was in motion, and inside this light a very beautiful Lady sitting on our “Paradise”, having her head in her hands. This beautiful Lady stood up, she nonchalantly crossed her arms while looking at us and said to us: “Come forward, my children, don’t be scared; I am here to announce a great news to you.” These soft and sweet words made me fly to her, and my heart would have wanted to stick to her forever. When I came close to the beautiful Lady, in front of her, to her right, she began the speech, and tears started also to flow from her beautiful eyes:

> “If my people do not want to submit themselves to God, I am forced to let go off the hand of my Son. It is so heavy and weighty, that I cannot withhold it anymore.
> 
> I have been suffering for you for a long time. If I do not want my Son to abandon you, I will have to take upon myself to pray to him without respite. And to all of you, it doesn’t matter. However you pray, however you do, you will never make up for the hardship I have endured for all of you.
> 
> I gave you six days to work, I kept the seventh for myself, and no one wishes to grant it to me. This is what weighs down the arm of my Son so much.
> 
> Those who drive carts cannot speak without putting the name of my Son in the middle.
> 
> These are the two things which weigh down the arm of my Son so much. If the harvest gets spoiled, it is only because of you. I showed it to you last year with the potatoes, you didn’t care about it; on the contrary when you found spoilt ones, you swore, and added the name of my Son. They will keep on going bad; and at Christmas, there will be none left.”

At this point, I was trying to interpret the word: potatoes; I thought it meant apples. The Beautiful and Good Lady, reading my thought, repeated thus:

> “You do not understand, my children? I am going to say it differently.”

The French translation is as follows:

> “If the harvest gets spoiled, it is only because of you. I showed it to you last year with the potatoes, you didn’t care about it; it was the contrary when you found spoilt ones, you swore, and added the name of my Son. They will keep on going bad; and at Christmas, there will be none left.
> 
> If you have corn, don’t sow it. All that you saw will be eaten by animals; and all that grows, will fall to dust when you thresh it. A great famine will come. Before the famine comes, the little children under the age of seven will have a tremor and will die in the arms of those holding them; the others will do penance through hunger. The walnuts will go bad, the grapes will rot.”

At this point, the Beautiful Lady, who was entrancing me, remained silent for a while; however I saw that she was going on, as if speaking, to move graciously her kind lips. Maximin was at that time receiving his secret. After, addressing me, the Very Holy Virgin spoke to me and gave me a secret in French. Here is the whole secret, and as she gave it to me:


---

## III

> “Mélanie, what I am going to tell you now will not always be secret; you can publish it in 1858.
>
> Priests, my Son’s ministers, priests, by their bad life, by their irreverence and their impiety in celebrating the holy mysteries, by love of money, love of honor and pleasures, priests have become sewers of impurity. Yes, priests call for vengeance, and vengeance is suspended over their heads. Woe to priests and to persons consecrated to God, who by their infidelities and their bad life are crucifying my son anew! The sins of persons consecrated to God scream toward heaven and call for vengeance, and here is vengeance at their very doors, because there is nobody left to implore mercy and forgiveness for the people; there are no generous souls anymore, there is no one left dignified to offer the spotless Victim to the Eternal in favor of the world.
>
> God is going to strike in an unprecedented way.
>
> Woe to the inhabitants of the earth! God will exhaust his anger, and nobody will be able to get away from so many mischief gathered.
>
> The heads, the leaders of the people of God have neglected prayer and penance, and the devil has obscured their intelligence; they have become those wandering stars that the old devil will drag with his tail to make them perish. God will enable the old snake to put divisions among rulers, in all societies and in all families; physical and moral pains will be suffered; God will abandon men to themselves, and will send chastisements which will follow one after the other during more than 35 years.
>
> Society is on the eve of the most terrible scourges and of the greatest events; one must expect to be governed by an iron birch and to drink the chalice of God’s anger.
>
> May my Son’s Vicar, the Sovereign Pontiff Pius IX, not leave Rome anymore after the year 1859; but be firm and generous, and fight with the weapons of faith and love; I will be with him.
>
> He should beware of Napoleon; his heart is double, and as he wants to be both Pope and emperor, God will abandon him; he is that eagle who, always wanting to rise, will fall on the sword which he wanted to use   to oblige the people to make himself rise.
>
> Italy will be punished for its ambition in wanting to shake the yoke of the Lord of the lords; thus it will be handed over to war; blood will flow on all sides; Churches will be closed or profaned; priests, religious will be driven away; they will be slaughtered and put to a cruel death. Several will abandon the faith and the number of priests and religious who will separate themselves from true religion will be great; there will even be Bishops among these people.
>
> May the Pope beware of miracle makers, because the time has come that the most astonishing prodigies will take place on the earth and in the air.
>
> In the year 1864, Lucifer together with a great number of devils will be untied from hell: they will abolish the faith little by little and even in persons consecrated to God; they will blind them in such a way, that without a particular grace, these persons will take on the spirit of these bad angels; several religious houses will completely lose the faith and will lose many souls.
>
> Bad books will abound on earth, and everywhere the spirits of darkness will spread universal loosening in everything regarding the service of God: they will have great power over nature; there will be churches to serve these spirits. Some persons will be transported from one place to another by these bad spirits and even some priests, because they will not have behaved according to the good spirit of the gospel, which is a spirit of humility, of charity and of zeal for the glory of God. Some people, including some righteous ones, will be brought back to life from the dead” *(it means that these dead will assume the prospect of righteous souls who once lived on earth, in order to seduce men more easily; these so-called resurrected dead, who will be nothing other than the devil under these faces, will preach another Gospel contrary to that of the true Christ Jesus, denying the existence of heaven, if these be not in fact the souls of the damned. All these souls will appear like united to their bodies.)* “In every place there will be extraordinary prodigies because true faith has fainted and fake light illuminates the world. Woe to the princes of the Church who will only be busy piling up riches upon riches, safeguarding their authority and dominating with pride!
>
> My Son’s Vicar will have much to suffer, because for a time the Church will be left to great persecutions; it will be the time of darkness; the Church will undergo an awful crisis.
>
> With God’s holy faith forgotten, each individual will want to guide himself and be superior to his fellow-men. Civil and ecclesiastical powers will be abolished; all order and all justice will be trampled underfoot; murders, hatred, jealousy, lying and discord will become commonplace, and the love of family and fatherland will disappear.
>
> The Holy Father will suffer greatly. I will be with him until the end to receive his sacrifice.
>
> The wicked will make several attempts on the pontiff’s life without being able to hinder his days; but neither he nor his successor” *(who will not reign for a long time)* will see the triumph of God’s Church.
>
> Civil rulers will all have a similar objective, which will be to abolish and make every religious principle disappear, to make way to materialism, to atheism, to Spiritism and all kinds of vices.
>
> In the year 1865, one will see the abomination in the holy places; in the convents, the flowers of the Church will be putrefied and the devil will establish himself as the king of hearts. May those who are heads of religious communities beware of the persons they have to admit, because the devil will use all his malice to introduce persons devoted to sin inside religious orders, because disorders and the love for pleasures of the flesh will be widespread all over the earth.
>
> France, Italy, Spain and England will be at war, blood will flow in the streets; Frenchmen will fight with Frenchmen, Italian with Italian; then there will be a general war which will be frightful. For a time, God will no longer remember France and Italy, because the Gospel of Jesus Christ is no longer known. The wicked will unfold all their malice; one will mutually massacre each other even inside homes.
>
> At the first strike of His crushing sword, the mountains and the whole nature will shake with fright, because the disorders and crimes of men have pierced the vault of the heavens. Paris will be burned and Marseilles swallowed up; several large cities will be shattered and swallowed up by earthquakes; all will seem lost; murders will be widespread, clash of arms and blasphemies will be heard everywhere. The righteous will suffer a lot; their prayers, their penance and their tears will rise till heaven, and all the people of God will ask for forgiveness and mercy, and will ask for my help and my intercession. So Jesus Christ, by an act of his justice and his great mercy for the righteous, will command to his angels that all his enemies be put to death. Suddenly, the persecutors of Jesus Christ’s, Church and all men devoted to sin, will perish, and the earth will become like a desert. Then there will be peace, the reconciliation between God and men; Jesus Christ will be served, adored and glorified; charity will flourish everywhere. The new kings will be the right arm of the Holy Church, which will be strong, humble, pious, poor, zealous and imitative of Jesus Christ’s virtues. The Gospel will be preached everywhere, and men will make great progress in the faith, because there will be unity among Jesus Christ’s laborers and that men will live in the fear of God.
>
> This peace among men will not be for long: 25 years of abundant harvests will make them forget that the sins of men are the cause of all the hardship which happens on earth.
>
> A precursor of the Antichrist, with his troops from several nations, will fight against the real Christ, the only Savior of the world; he will spill lots of blood and will want to annihilate the cult of God in order to be himself seen as a God.
>
> The earth will be struck by all kinds of calamities;” *(besides plague and famine, which will be global)* “there will be wars until the last war, which will then be waged by the ten kings of the Antichrist, kings who will all have the same design and will be the only one governing the world. Before this happens, there will be a kind of false peace in the world; one will only think about having fun; the wicked will indulge in all kinds of sins; but the children of the Holy Church, children of the faith, my true imitators, will grow in the love of God and in the virtues dearest to me. Blessed are the humble souls driven by the Holy Spirit! I will fight along with them until they reach maturity. 
>
> Nature asks vengeance for men, and it trembles with fright, in the waiting of what must happen to the earth stained with crimes.
>
> Tremble, earth, and you who make the vow to serve Jesus Christ, and who in your heart adore yourselves, tremble; because God is going to hand you over to his enemy, because the holy places are in a state of corruption; many convents are no longer houses of God, but the pastures of Asmodeus and his owns.
>
> It will be during this time that the Antichrist will be born, from a Hebrew nun, a false virgin who will be in communication with the old snake, the master of impurity; his father will be a Bishop; at birth he will vomit blasphemies, he will have teeth; in one word, it will be the devil incarnate; he will utter terrifying screams, he will make prodigies, he will only feed himself on impurities. He will have brothers who, although not being incarnate devils like him, will be children of evil; at the age of twelve, they will be noticed by the valiant victories they will win; soon they will each be at the head of the armies, assisted by legions from hell.
>
> The seasons will be changed, the earth will only produce bad fruits, stellar bodies will lose their regular movement, the moon will only reflect a feeble reddish light; water and fire will generate convulsive motions and horrifying earthquakes on earth, causing mountains, cities *(etc.)* to be swallowed up.
>
> Rome will lose Faith and become the seat of the Antichrist.
>
> The demons of the air, together with the Antichrist, will make great prodigies on the earth and in the air, and men will become increasingly pervert. God will take care of His faithful servants and men of good will; the Gospel will be preached everywhere, all peoples and all nations will have knowledge of the Truth.
>
> I address a pressing appeal to the earth: I call upon the true disciples of the God living and reigning in the heavens; I call upon the true imitators of Christ made man, the only true Savior of men; I call upon my children, my true devotees, those who have given themselves to me so that I may lead them to my Divine Son, those I hold, so to say, in my arms, those who have lived from my spirit; finally, I call upon the Apostles of the Latter Times, the faithful disciples of Jesus Christ who have lived in contempt of the world and of themselves, in poverty and humility, in contempt and silence, in oration and mortification, in chastity and in union with God, in suffering, and unknown to the world. It is time they went out and enlightened the earth. Go, and show yourselves as my dearest children; I am with you and in you, provided your faith is the light that enlightens you in these evil days. May your zeal make you starve for the glory and honor of Jesus Christ. Do fight, children of light, you, the few who see thereby; for here comes the time of times, the end of ends.
>
> The Church will be eclipsed; the world will be in consternation. But here come Enoch and Elias full of God’s spirit, they will preach with the strength of God, and men of good-will will believe in God, and many souls will be consoled; they will make great progress by the Holy-Spirit’s virtue and will condemn the diabolical errors of the Antichrist.
>
> Woe to the inhabitants of the earth. There will be bloody wars and famines; plagues and contagious diseases; there will be rains of a frightful hail of animals; thunders which will demolish cities; earthquakes which will swallow up countries; voices will be heard in the air; men will beat their heads against the walls; they will call on death, yet death will torture them; blood will flow on all sides. Who will be able to overcome, if God doesn’t shorten the time of trial? At the blood, the tears and the prayers of the righteous, God will relent; Enoch and Elias will be put to death; pagan Rome will disappear; the fire from the sky will fall and consume three cities; the whole universe will be struck with terror, and many will let themselves be seduced because they didn’t adore the true Christ living among themselves. It is time; the sun is darkening; Faith alone will live.
>
> Here comes the time; the abyss opens up. Here comes the king of the kings of darkness. Here is the beast with its subjects, pretending to be the savior of the world. He will rise in the air with pride in order to reach the sky; he will be stifled by the breath of St. Michael the Archangel. He will fall, and the earth, which will be in continual evolution for three days will open its fiery bosom; he will be plunged forever with all his followers in the eternal abysses of hell. Then water and fire will purify the earth and consume all the creations of men’s pride, and everything will be renewed; God will be served and glorified.”

---

## IV


Then the Holy Virgin gave me, also in French, the rule of a new religious Order.

After having given to me the rule of this new religious order, the Holy Virgin resumed the rest of the speech as follows:

> “If they become converted, stones and rocks will change into wheat and potatoes will be fertilized by the grounds.
>
> “Do you say your prayer well, my children?”

We both replied:

“Oh! No Madam, not a lot.”

> “Ah! My children, you have to do it, in the evening and in the morning. When you cannot do better, say one Pater and one Ave Maria, but when you have time and can do better, say some more.
>
> Only few old women go to Mass; the others work on Sundays the whole summer; and in the winter too, when they don’t know what to do, they only go to Mass to mock religion. On the lent period, they go to the butcher’s shop like dogs.
>
> Didn’t you see spoilt wheat, my children?”

Both of us, we replied: “Oh! No Madam.”

> *The Holy Virgin addressing herself to Maximin:* “But you, my child, you must have seen some, one time around Corner, with your father. The owner of the field said to your father: “Come to see how my wheat gets spoiled”. You went there. Your father took two or three ears of wheat into his hand, he rubbed them and they fell into dust. Then, on your way back home, when you were only half an hour away from Corps, your father gave you a piece of bread, saying to you: Here, my child, eat this year, because I don’t know who will eat any next year, if the wheat gets spoiled like this.”

Maximin replied: “That is true Madam, I didn’t remember it.”

> *The Very Holy Virgin has ended her speech in French:* “Well! My children, you will pass it on to all my people.”

The very beautiful Lady crossed the stream; and, two steps away from the stream, without turning back to us who were following her (because she was attracting to her by her brightness and even more by her kindness which was enrapturing me, and seemed to melt my heart), she told us again:

> “Well! My children, you will pass it on to all my people.”

Then she kept on walking until the place where I had gone up to look where our cows were. Her feet were only touching the tip of the grass without bending it. When she reached the top of the hillock, the beautiful Lady stopped, and quickly I came in front of her, to look at her so, so closely and try to figure out which way she was more inclined to take, for it was all over for me, I had forgotten my cows as well as the masters I worked for; I had attached myself forever and without condition to My Lady; yes, I wanted to never ever leave her; I followed her without ulterior motive, and ready to serve her as long as I would live.

In my Lady’s company, I thought I had forgotten the paradise; I had, only left, the thought of serving her well in everything; and I was convinced I could have done anything She would have told me to, for it seemed to me that She had a lot of power. She was looking at me with tender kindness which was attracting me to her; I would have liked, with my eyes closed, to rush into her arms. She didn’t give me the time to do so. She rose imperceptibly from the earth to a height of about one meter and more; and remaining like this in the air for a very short time, My beautiful Lady looked at the sky, then the earth to her right and to her left, then she looked at me with such soft, lovable and kind eyes that I felt She was attracting me inside her, and it seemed to me that my heart was opening up to hers.

And, while my heart was melting in a soft dilatation, the beautiful face of My good Lady disappeared little by little: it seemed to me that the light in motion was multiplying or was condensing around the Very Holy Virgin, in order to prevent me from seeing her any longer. Thus the light was replacing the parts of the body which were disappearing from my sight; or it seemed that the body of My Lady was changing into light by melting. Thus the globe-shaped light was rising gently in a straight direction.

I cannot say if the volume of light was diminishing as she was rising, or if it was the distance which made me see the light diminishing as it was rising; what I know, is that I stayed with the head up and the eyes fixed on the light, even after this light, which was always going away and diminishing in volume, finally disappeared.

I take my eyes away off the firmament and I look around me, I see Maximin who was looking at me, I tell him: “Mémin, this must be my father’s good God, or the Holy Virgin, or some great saint.” And Maximin throws his hand in the air and says: “Ah! If only I had known it!”

---

## V

On the evening of the 19th of September, we went back a little earlier than usual. Having arrived at my masters’ farm, I was busy tying up my cows and tiding up everything in the stable. I hadn’t finished yet when my mistress came to me crying and told me: “Why, my child, don’t you come and tell me what has happened to you on the mountain?” (Having not found his masters who had not left their work yet, Maximin had come to mine and narrated everything he had seen and heard). I replied: “I wanted to tell you about it but I wanted to finish my work first.” A moment later, I went inside the house, and my mistress said to me: “Relate what you saw; the shepherd of Bruite (it was the nickname of Pierre Selme, Maximin’s master) told me everything.”

I started and, toward the middle of the account, my masters arrived from their fields; my mistress, who was crying at hearing the complains and threats of our tender Mother, said to her husband: “Ah! You wanted to go to harvest the wheat tomorrow; keep off it, come and hear what has happened today to this child and Selme’s shepherd.” And, turning toward me, she said: “Start again everything you told me.” I started again; and when I had finished, my master said: “It was the Holy Virgin, or a great saint, who has come from the good God; but it is as if the good God had come himself: we have to do everything this Saint has told. How are you going to do, to say that to all her people? “I answered him: “You will tell me how to go about it, and I will do it.” Then he added, looking at his mother, his wife and his brother: “We have to think about it.” Then everyone went back to his business.

It was after supper, Maximin and his masters came to see mine and to narrate what Maximin had told them, and to discuss what should be done: “Because, they said, from the words She has said, it seems to us that it is the Holy Virgin who has been sent by the good God. And She has told them to pass it on to all her people; maybe these children will have to travel over the entire world to proclaim that everybody must observe the commandments of the good God, otherwise great misfortunes are going to come upon us.”

After a moment of silence, my master said, while addressing Maximin and me: “Do you know what you have to do, my children? Tomorrow, get up early in the morning, both of you go to see the Parish Priest, and tell him everything you have seen and heard; tell him how the thing has happened: he will tell you what you have to do. “

On the 20th of September, the day after the apparition, I left early with Maximin. Once we reached the vicarage, I knocked at the door. The parish priest’s servant came to open, and asked what we wanted. I told her (in French, which I had never spoken before): “We would like to speak to the parish priest.” - “And what do you want to tell him? “, she asked us. – “ We want to tell him, miss, that yesterday we went to look after our cows on the Baisses’ mountain, and after having dined, etc., etc. “ We narrated to her a big part of the Very Holy Virgin’s message. Then the bell of the church rang; it was the final call for Mass. Mr the Abbot Perrin, the parish priest of “la Salette”, who had heard us, opened abruptly the door: he was crying; he was hitting his chest; he told us: “My children, we are lost; the good God is going to punish us. Ah! My God, it is the Holy Virgin which appeared to you!” And he left to say the Holy Mass. We looked at each other with Maximin and the servant; then Maximin said to me “I’ll set off home to my father in Corps. And we separated.

Because I hadn’t received the order from my masters to leave as soon as I had talked to the parish priest, I didn’t see anything wrong in attending the Mass. So I was at the Church. Mass starts, and, after the first Gospel, the parish priest turns toward the people and try to recount to his parishioners the apparition which had just occurred, the previous day, on one of their mountains, and strongly invite them not to work anymore on Sundays: his voice was broken by sobs , and all the people were moved. After the Holy Mass, I went back to my masters’ house. Monsieur Peytard, who is still the mayor of la Salette nowadays, came there to interrogate me about the apparition; and, after he got reassured that what I was saying to him was the truth, he left, convinced.

I remained at the service of my masters until the All Saints’ Day.  Afterward, I was boarded with nuns of Providence in my home town of Corps.

---

## VI

The Very Holy Virgin was very tall and well proportioned ; she seemed to be so light that with one blow one would have made her move; however, she was still and well settled. Her physiognomy was majestic, imposing, but not imposing like are the lords down on earth. She inspired a respectful fear. Her majesty, simultaneously, compelled respect mixed with love and drew me close to her. Her look was soft and penetrating; her eyes seemed to be speaking with mine, but the conversation was coming from a deep and lively feeling of love toward this ravishing beauty liquefying me. The softness of her look, her incomprehensible kindness made me understand and feel that she attracting me to her and wanted to give herself; it was an expression of love which can neither be expressed with the tongue of the flesh nor with the letters of the alphabet.

The clothing of the Very Holy Virgin was silvery white and all shiny; it had nothing material: it was made of light and glory, varying and twinkling. On earth, there is neither expression nor comparison to give.

The Holy Virgin was all beautiful and all formed by love; while looking at her, I was languishing to melt myself within her. From her attire, as well as herself, was emanating the majesty, the splendor, the magnificence of an incomparable Queen. She seemed beautiful, white, immaculate, crystallized, dazzling, celestial, fresh, new as a Virgin; it seemed that the word: Love was slipping from her silvery and all very pure lips. She seemed to me like a good mother, full of kindness, of amiability, of love for us, of compassion, of mercy. 

The crown of roses she had on the head was so beautiful, so bright, that nobody can imagine it: the roses, of diverse colors, weren’t earthly; it was a gathering of flowers which were surrounding the head of the Very Holy Virgin in the shape of a crown; but the roses were changing or replacing themselves; then, from the heart of each rose shone a magnificent entrancing light that endowed the flowers with a dazzling beauty. From the crown of roses rose golden-like branches, and a quantity of other little flowers mixed with sparkles.

The whole thing formed a very beautiful tiara, which alone shone brighter than the sun.

The Holy Virgin had a very pretty Cross suspended at her neck. This Cross seemed to be golden; I say “golden” not to say a golden plate; for sometimes I have seen golden objects with diverse shades of gold, which had a much nicer effect on my eyes than a simple golden plate. On this beautiful Cross, all shining with light, was a Christ, was Our-Lord, the arms spread on the Cross. At almost both extremities of the Cross, there were a hammer on one side and pincers on the other. The Christ was of natural flesh color; but he was shining with great brightness and the light, which was coming out of his whole body, seemed like very shining darts which were cleaving my heart with desire to melt within him. Sometimes the Christ seemed to be dead: he had his head leaning, and the body was like sunk-down, like ready to fall, if it wasn’t for the nails withholding him to the Cross.

I had a deep compassion for him, and I would have liked to keep repeating his unknown love to the whole world, and infiltrate into mortal the most felt love and the liveliest recognition toward God who didn’t need us at all; to be what He is, what He was and what He will always be; and yet, oh love, incomprehensible to man! He became man, and he wanted to die, yes die, to better write in our souls and memories the Crazy love he has for us! Oh! How miserable am I to find myself unable to express in words the love of our good Savior for us! But, on the other side, how happy we are to be able to feel intensely what we cannot express!

At other times, the Christ seemed to be alive; he had his head straight, eyes open, and seemed to be on the Cross of his own will. Sometimes, he seemed to be speaking too: he seemed to want to show that he was on the Cross for us, out of love for us, to attract us to his love, that he always has a new love for us, that his love from the beginning and of the year 33 is still that of today and that it will always be.

The Holy Virgin was crying almost all the time she spoke to me. Her tears were flowing, one after the other, slowly, down to her knees; then, like sparks of light, they disappeared	. They were shining and full of love. I would have like to console her, and that she didn’t cry anymore. But it seemed to me that she needed to show her tears to better show her love forgotten by men. I would have liked to throw myself into her arms and tell her: “My good Mother, do not cry! I want to love you for all the men on the earth.” But it seemed to me that she was saying to me: “But so many don’t know me!”

I was between death and life, seeing on one side so much love, so much desire to be loved, and on the other so much coldness, so much indifference … Oh! My Mother, all very beautiful and lovable Mother, my love, heart of my heart! ...

The tears of our tender Mother, far from diminishing her air of Majesty, of a Queen and of a Mistress, seemed, on the contrary, to embellish her, to make her more lovable, more beautiful, more powerful, more full of love, more maternal, more ravishing; and I would have eaten her tears, which made my heart jump with compassion and love. To see a Mother cry, and such a Mother, without using all the possible means to console her, to change her pains into joy, how can that be! Oh Mother, more than good! You have been made from all the prerogatives’ God is capable of; it seems that you have exhausted the power of God; you are good and also good from the kindness of God himself; God enlarged himself by forming you, his terrestrial and celestial master-piece.

The Very Holy Virgin was wearing a yellow apron. What am I saying, yellow? She was wearing an apron brighter than several suns put together. It wasn’t material cloth, it was made of glory, and this glory was twinkling and of ravishing beauty. Everything in the Very Holy Virgin induced me strongly, and made me as to glide to adore and to love my Jesus in every states of his mortal life.

The Very Holy Virgin had two chains, one slightly wider than the other. To the thinner one was suspended the Cross I mentioned earlier. These chains (since we have to call them “chains”) were like rays of glory, of great changing and twinkling brightness.

The shoes (since we have to call them “shoes”) were white, but a silvery white, shiny; there were roses around them. These roses were of a dazzling beauty and from the heart of each rose emerged a flame of very beautiful and very pleasing light to see. On the shoes, there were golden buckles, not the earthly gold, but the truly heavenly gold.

The sight of the Very Holy Virgin was itself an accomplished paradise. She had within Her everything that could satisfy, because the earth was forgotten.

The Holy Virgin was surrounded by two lights. The first light, closer to the Very Holy Virgin, was reaching us; it was shining of a very beautiful and twinkling brightness. The second light was spreading a little more around the Beautiful-Lady, and we were inside this one; it was static (that is to say that it was not twinkling), but very much brighter than our poor sun. All these lights were not hurting the eyes, and were not tiring the eyesight.

In addition to all these lights, all this splendor, bundles of rays and light beams shone from the Body of the Holy Virgin, from her dresses and from everywhere.

The voice of the Beautiful Lady was soft; it was enchanting, delighting, and pleasing to the heart; it gave satisfaction, broke down barriers, it appeased and softened. It seemed to me that I would always have wanted to eat her beautiful voice, and my heart seemed to be dancing or wanting to go to meet her and liquefy in her.

The eyes of the Very Holy Virgin, our tender Mother, cannot be described by a human language. To speak about it, a seraph would be required: more would be required, the language of God himself would be required, of this God who has shaped the Immaculate Virgin, master piece of all his power.

The eyes of the august Marie seemed thousands and thousands of times more beautiful than the more sought after jewels, diamonds and precious stones; they were shining like two Suns; they were soft from softness itself, clear like a mirror. Inside her eyes one could see the paradise; they drew to Her; it seemed that she wanted to give herself and attract. The more I looked at her the more I wanted to see her; the more I saw her, the more I loved her, and I loved her with all my strength.

The eyes of the beautiful Immaculate were like the door of God, from where one could see everything that can enrapture the soul. When my eyes met the one of the Mother of God and mine, I felt inside myself a happy revolution of love and a protest to love her and melt with love.

While we looked at each other, our eyes were talking to each other their own way, and I loved her so much that I would have wanted to kiss her in the middle of her eyes, which touched my soul and seemed to attract it and melt it into hers. Her eyes planted a soft tremor in all my being; and I feared to make a single move which would have been slightly unpleasant to her.

Just the sight of the eyes of the purest of the Virgins would have been enough to be the Heaven of the blessed; would have been enough to submit the soul to the will of the Most High amid all the events which happen during a mortal’s life, would have been enough to make the soul continuously perform acts of praise, of thanking, of reparation and of expiation. This sight alone condenses the soul in God and makes it like a living dead, considering all earthly things, even the ones which seem the most serious, to be just playthings; the soul would only want to hear about God and what is related to his Glory.

Sin is the only evil She sees on earth. She would die of pain if God wasn’t supporting her.

Amen

---

Castellamare, on 21st November 1878

Mary of the Cross, Victim of Jesus
Born Mélanie Calvat, Shepherdess of la Salette

---

Nihil obstat: imprimatur Datum Lycii ex Curia Ep” \
die 15 Nov 1879. \
Vicarius Generalis Carmelus Arch.us Cosma

---

[Download document](/apparition/apparition%20anglais%20A5.odt)
