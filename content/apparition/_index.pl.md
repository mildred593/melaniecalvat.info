---
menu: true
menutitle: ZJAWIENIE
title: ZJAWIENIE SIĘ NAJŚWIĘTSZEJ PANNY MARYJI NA GÓRZE W LA SALETTE 19 WRZEŚNIA 1846 ROKU
showinsubmenu: false
showindexlist: false
weight: 3
---

- [Pobierz tekst](/apparition/apparition%20polonais%20A5.odt)
- [Ulotka w formacie A5](/apparition/livret%20apparition%20PL_A5.pdf) z [gotową do druku wersją w formacie A4 poziomym do złożenia w broszurę](/apparition/livret%20apparition%20PL_A4_booklet.pdf)

Sama tylko jasnowidząca Melania z Maksyminem może je opowiedzieć. Dokonawszy tego ustnie, niezliczoną ilość razy, w roku 1878 postanowiła je opisać. Opis ten, zaopatrzony w imprimatur ks. biskupa Zoli, wydrukowano po raz pierwszy w Lecce (Włochy) 15 listopada 1879 r., a następnie (bez żadnych zmian) w Lyonie (Francja) w r. 1904, na kilka miesięcy przed śmiercią Melanii. Ta francuska broszurka jest już niemal niedostępna. Podajemy tutaj jej dokładne tłumaczenie.

---

## I

"Dnia 18 września 1846 r., w przeddzień świętego ukazania się Najświętszej Dziewicy, pasąc krowy swych gospodarzy, byłam jak zwykle sama. Około godziny 11 przed południem zauważyłam zbliżającego się do mnie małego chłopca. Przestraszyłam się na ten widok, bo jak mi się wydawało - wszyscy powinni wiedzieć, że ja stronię od wszelkiego towarzystwa. Chłopczyk ten podszedł do mnie i rzekł: "Mała, zostanę z Tobą, ja też jestem z Corps." Na te słowa zaraz ujawniła się moja zła natura, bo cofając się kilka kroków, powiedziałam mu: "Nie chcę nikogo; chcę pozostać sama." Ale chłopczyk idąc za mną, odparł: "No, pozwól mi pozostać z Tobą, mój gospodarz kazał mi paść moje krowy z twoimi, ja jestem z Corps." Oddalając się dałam mu znak, że nie chcę nikogo. Odszedłszy, usiadłam w pewnym oddaleniu na trawie i rozmawiałam z kwiatkami dobrego Boga.

  W chwilę potem obejrzawszy się, widzę Maksymina siedzącego obok mnie, on zaś natychmiast odzywa się do mnie: "Pozwól mi pozostać - będę bardzo grzeczny." Ale moje złe usposobienie nie słuchało rozumu: Wstałam pospiesznie i uciekłam dalej nic nie mówiąc i znów poczęłam się bawić z kwiateczkami dobrego Boga. Za chwilę Maksymin znów znalazł się przy mnie mówiąc, że będzie bardzo grzeczny, że nic nie będzie mówił, że przykrzyło mu się samemu, że jego gospodarz przysłał go do mnie itd., itd. Tym razem ogarnęła mnie litość, dałam więc znak aby usiadł i w dalszym ciągu zajmowałam się kwiatkami dobrego Boga.

 Maksymin nie omieszkał przerwać ciszę, zaczął się śmiać (przypuszczam, że ze mnie), a gdy spojrzałam na niego, rzekł mi: "Zabawmy się, zagrajmy w co." Nic mu nie odpowiedziałam, byłam bowiem taką ignorantką, że przebywając zawsze sama, nie znałam żadnej zabawy z inną osobą. Znów zajęłam się zabawą z kwiatami, gdy nagle Maksymin, zbliżywszy się do mnie, ciągle się śmiał, mówiąc, że kwiaty nie mają uszu, aby słyszeć i że powinniśmy bawić się razem. Ja jednak nie miałam żadnej skłonności do zabawy, którą mi proponował, ale zaczęłam z nim rozmawiać, on zaś mi powiedział, że dziesięć dni, które ma przebywać u gospodarza, wkrótce upłynie, a on powróci do swego ojca w Corps itd.

   Gdy on mówił do mnie, dał się słyszeć dźwięk dzwonu w la Salette, wzywający na Anioł Pański, dałam mu znak, aby wzniósł swą duszę do Boga. Wtedy on zdjął kapelusz i przez pewien czas pozostał w milczeniu. Następnie zapytałam go: Chcesz jeść? - Tak, zjedzmy coś - odpowiedział mi. Gdyśmy usiedli, wyjęłam z torby zapasy, które dali mi gospodarze i przed ugryzieniem okrągłej bułki, którą trzymałam w ręku, czubkiem noża zrobiłam na niej - według swego zwyczaju - znak krzyża i mały otwór w środku, mówiąc: Jeśli jest tu diabeł, niech wyjdzie, jeśli dobry Bóg, niech pozostanie i szybko zakryłam dziurkę. Maksymin wybuchnął głośnym śmiechem i kopnął w moją bułkę, która wypadła mi z rąk, potoczyła się z góry na dół i zniknęła. Miałam jeszcze inny kawałek chleba, który jedliśmy razem, po czym zabawiliśmy się. Następnie, zrozumiawszy, że Maksymin jest chyba głodny, pokazałam mu miejsce na górze pokryte małymi owocami. Zachęciłam go, by ich zjadł, co natychmiast zrobił. Najadł się sam i jeszcze przyniósł mi ich pełen kapelusz. Wieczorem zeszliśmy z góry razem i umówiliśmy się, że również nazajutrz będziemy tam paść krowy razem.

  Nazajutrz, 19 września, spotkałam się z Maksyminem na drodze. Weszliśmy na górę razem. Stwierdziłam, że Maksymin był bardzo dobry, bardzo prosty, że chętnie mówił o tym, o czym ja chciałam mówić. Był także bardzo giętki i nie obstawał przy swoim. Był tylko trochę ciekawy, bo gdy tylko oddaliłam się od niego i zatrzymałam, szybko przybiegał do mnie, aby widzieć, co robiłam i słyszeć o czym rozmawiałam z kwiatkami dobrego Boga. Gdy zaś nie zdążył przybiec na czas, wypytywał mnie, co mówiłam.

   Maksymin prosił mnie, aby go nauczyć jakiej zabawy. Ranek już miał się ku końcowi. Kazałam mu zbierać kwiaty, aby zrobić raj. Oboje zabraliśmy się do dzieła, wkrótce mieliśmy mnóstwo różnobarwnego kwiecia. Niebo było jasne, bezchmurne. We wsi dzwoniono na Anioł Pański. Gdyśmy powiedzieli dobremu Bogu to, cośmy umieli, powiedziałam Maksyminowi, że trzeba zaprowadzić bydło na pastwiska nad wąwozem, gdzie jest dużo kamieni do zbudowania raju. Zapędziwszy tam bydło, zjedliśmy skromny posiłek, po czym zabraliśmy się do znoszenia kamieni i do budowy domku. Składał się on z parteru, który miał być naszym rzekomym mieszkaniem oraz piętra, które miało być rajem.

   Całe to piętro były przybrane różnobarwnymi kwiatami poutykanymi tak, że ich korony zwisały do dołu. Raj przykryliśmy jednym płaskim głazem, na którym rozłożyliśmy kwiaty i wokół którego również zwisały kwiaty. Po ukończeniu pracy przypatrywaliśmy się swemu rajowi. Wkrótce zmorzyła nas senność i zasnęliśmy w trawie.

---

## II

Obudziwszy się i nie widząc krów, zawołałam Maksymina i wybiegłam na mały pagórek. Zauważywszy, że krówki leżą sobie spokojnie, zaczęłam schodzić z pagórka a Maksymin wchodził. Nagle ujrzałam piękne światło jaśniejsze aniżeli słońce i ledwie wypowiedziałam słowa: "Maksymie, widzisz tam? Ach, mój Boże!", upuściłam kij, który miałam w ręce. W tej chwili działo się we mnie coś rozkosznego - nie umiem tego określić, ale czułam, że to światło mnie przyciąga do siebie, czułam wielki i miłością przepełniony religijny szacunek, me serce chciało biec szybciej aniżeli ja.

  Patrzyłam z natężeniem na to światło, które początkowo było nieruchome, wkrótce jednak jakby się rozwarło i wtenczas ujrzałam inne światło, o wiele jaśniejsze i ruchome, a w tym świetle przepiękną Panią siedzącą na naszym raju z twarzą ukrytą w dłoniach.

  Piękna Pani wstała, lekko skrzyżowała ramiona i patrząc na nas rzekła: *"Zbliżcie się, moje dzieci, nie bójcie się, jestem tu, aby zapowiedzieć wam wielką nowinę."* Te słodkie i łagodne słowa porwały mnie ku Niej, a serce moje chciałoby przywrzeć do Niej na zawsze.

> "Gdy stanęłam obok Niej, po Jej prawej stronie, Piękna Pani rozpoczęła mowę, ale też łzy zaczęły płynąć z Jej pięknych oczu:
>
> Skoro mój lud nie chce okazać uległości, jestem zmuszona puścić rękę swego Syna. Ona jest tak ciężka, że już nie mogę jej utrzymać.
>
> Od jak dawna już cierpię za was! Jeśli chcę, żeby mój Syn was nie opuścił, muszę Go nieustannie prosić. Wy zaś nic sobie z tego nie robicie. Zresztą daremnie byście się modlili, daremnie wysilali, nigdy nie moglibyście wynagrodzić trudu, jaki dla was podejmowałam.
>
>  Dałam wam sześć dni do pracy, a siódmy zarezerwowałam dla siebie, a ludzie nie chcą mi go dać. Z tej właśnie przyczyny ramię mego Syna staje się tak ciężkie.
>
>  Woźnice nie umieją mówić bez używania imienia mego Syna. Te dwie rzeczy najbardziej wpływają na to, że ramię mego Syna jest tak ciężkie. Gdy płody ziemi niszczeją, to tylko z waszej winy. Udowodniłam wam to zeszłego roku, jeśli chodzi o ziemniaki, wyście jednak to zlekceważyli, więcej jeszcze, gdyście znajdowali ziemniaki zepsute, przeklinaliście, szargając imię mego Syna. W dalszym ciągu będą wam się psuły, na Boże Narodzenie już nic ich nie pozostanie."

 Tutaj zastanawiałam się, co może znaczyć wyrażenie pommes de terre, myślałam, że ono znaczy jabłka. Odgadując moją myśl, Dobra Pani tak mówiła dalej :

>  Nie rozumiecie moje dzieci, będę więc mówiła do was inaczej. Jeśli macie zboże, nie trzeba go siać. Wszystko co zasiejecie, zwierzęta zjedzą, a to co wyrośnie, w pył się zamieni podczas młócenia. Przyjdzie wielki głód. Przed nadejściem głodu małe dzieci poniżej lat siedmiu zapadać będą na konwulsje i umierać w rękach osób, które będą je trzymały. Inni pokutować będą głodem. Orzechy staną się niedobre, winogrona będą się psuły."

  Teraz przez chwilę nie słyszałam tej Pięknej Pani, która mnie zachwycała, widziałam jednak, że poruszała wdzięcznie swymi kształtnymi wargami i z tego wnioskowałam, że coś mówi: to właśnie Maksyminowi powierzała jego sekret. Potem zwracając się do mnie, Najświętsza Maryja Panna wypowiedziała mi mój sekret po francusku. Oto pełna i dokładna jego treść:

---

## III

> "Melanio, to co ci teraz powiem, nie będzie zawsze tajemnicą, w roku 1858 możesz to opublikować.
> Kapłani słudzy mego Syna, przez całe swe złe życie, przez nieuszanowanie i lekceważenie, z jakim sprawują święte tajemnice, przez miłość pieniędzy, zaszczytów, przyjemności, stali się stekiem nieczystości.
>
> Tak, kapłani żądają pomsty, a pomsta wisi nad ich głowami. Biada kapłanom i osobom poświęconym Bogu, którzy swą niewiernością i złym życiem na nowo krzyżują mego Syna. Grzechy poświęconych Bogu wołają o pomstę do nieba, a oto pomsta jest u ich drzwi, bo już nie ma nikogo, kto by błagał o miłosierdzie i przebaczenie dla ludu, nie ma już dusz wspaniałomyślnych, nie ma już osoby, która byłaby godna ofiarować Przedwiecznemu za świat Żertwę bez skazy.
>
> Niezadługo uderzy Bóg w sposób bezprzykładny. Biada mieszkańcom ziemi! Cierpliwość Boża wkrótce się wyczerpie, a wtedy nikt nie zdoła się schronić od tylu klęsk nagromadzonych.
>
> Szefowie i przywódcy ludu Bożego zaniedbali mo- dlitwę i pokutę, a demon zaciemnił ich rozum, stali się tymi błędnymi gwiazdami, które stary diabeł pociągnie swym ogonem, by je zgubić. Bóg pozwoli staremu wężowi siać niezgodę między panującymi we wszystkich społeczeństwach i we wszystkich rodzinach. Ludzie znosić będą cierpienia fizyczne i moralne; Bóg zostawi ludzi ich własnemu losowi i ześle kary, które następować będą jedne po drugich, przez lat więcej niż trzydzieści pięć.
>
> Niech namiestnik mego Syna, Najwyższy Kapłan Pius IX, nie wychodzi z Rzymu po roku 1859, ale niech będzie nieugięty i wspaniałomyślny, niech walczy bronią wiary i miłości: ja będę z nim. Niech nie ufa Napoleonowi, jego serce jest dwoiste; zechce jednocześnie być papieżem i cesarzem, wkrótce Bóg odwróci się od niego. Jest on tym orłem, który chcąc zawsze się wznosić, spadnie na miecz, którym chciał zmusić narody do wywyższenia siebie samego.
>
> Włochy będą ukarane za ambicję, za chęć zrzucenia jarzma Pana panów, cały kraj zostanie uwikłany w wojnę, krew będzie się lała wszędzie, kościoły zostaną zamknięte lub sprofanowane, kapłani, zakonnicy będą wypędzeni, niektórzy z nich zginą, a zginą śmiercią okrutną. Niejeden porzuci wiarę, a liczba kapłanów i zakonników, którzy oderwą się od prawdziwej religii, będzie wielka, wśród tych osób znajdą się nawet biskupi.
>
> Niech Papież ma się na baczności przed fałszywymi cudotwórcami, nadszedł bowiem czas, kiedy najbardziej zdumiewające dziwy dziać się będą na ziemi i w przestworzach.
>
> W roku 1864 Lucyfer z wielką liczbą demonów zostanie wypuszczony z piekła, stopniowo wyrugują oni wiarę. Wygaszą ją nawet w osobach poświęconych Bogu, oślepią ich do tego stopnia, że jeśli nie będzie ich wspierać łaska nadzwyczajna, przejmą się duchem tych złych aniołów, niejeden dom zakonny utraci wiarę i doprowadzi do zguby wiele dusz.
>
> Wielkie mnóstwo złych książek będzie na całej ziemi, a duchy ciemności doprowadzą do powszechnego rozluźnienia w tym, co dotyczy służby Bożej. Będą one miały wielki wpływ na przyrodę, powstaną kościoły, w których służyć się im będzie. Będą one przenosić z miejsca na miejsce różnych ludzi, a nawet kapłanów, ponieważ nie kierowali się dobrym duchem, duchem Ewangelii, który wymaga pokory, czystości i troski o chwałę Bożą. Będzie się wskrzeszać umarłych (odrzuconych) i sprawiedliwych."
> *(To znaczy, że ci umarli, aby lepiej zwodzić ludzi, przybierać sobie będą ciało ludzi sprawiedliwych, którzy żyli na ziemi. Ci rzekomi zmartwychwstali, którzy będą tylko demonami w ludzkiej postaci, głosić będą ewangelię inną aniżeli Ewangelia Jezusa Chrystusa i przeczyć istnieniu nieba; będą to również dusze potępionych. Wszystkie te dusze będą się po, jawiać połączone ze swymi ciałami)*.
> "We wszystkich miejscach dziać się będą nadzwyczajne dziwy ponieważ prawdziwa wiara wygasła, a fałszywe światło oświeca świat. Biada książętom Kościoła, którzy będą tylko gromadzić bogactwa na bogactwach, strzec swej władzy i troszczyć się o dumne panowanie!
>
> Namiestnik mojego Syna będzie musiał dużo cierpieć, gdyż przez pewien czas Kościół będzie znosił srogie prześladowania i przechodził głęboki kryzys, będzie to czas ciemności.
>
> Ponieważ święta wiara w Boga zostanie zapomniana, każdy osobnik będzie chciał kierować się swymi własnymi zasadami i być wyższym od bliźnich. Władze świeckie i kościelne zostaną zniesione, deptać się będzie wszelki porządek i wszelką sprawiedliwość, panoszyć się będzie tylko mężobójstwo, zazdrość, nienawiść, kłamstwo, niezgoda, bez miłości ojczyzny i rodziny.
>
> Ojciec święty dużo wycierpi, ale ja będę z Nim do końca, aby przyjąć jego ofiarę. Źli zasadzać się będą na jego życie wielokrotnie, nie mogąc jednak mu szkodzić. Ale ani on, ani jego następca nie ujrzy tryumfu Kościoła Bożego.
>
> Wszyscy władcy świeccy żywić będą ten sam zamiar, mianowicie wyrwać i wyrugować wszelki pierwiastek religijny, aby zrobić miejsce materializmowi, ateizmowi, spirytyzmowi i wszelkiego rodzaju błędom.
>
> Wroku 1865 okropne rzeczy będą się działy w miejscach świętych, klasztorach zgniją kwiaty Kościoła, a demon stanie się jakby królem serc. Niech przełożeni wspólnot zakonnych odnoszą się krytycznie do osób, które mają przyjąć, gdyż demon używać będzie całej swej przebiegłości, aby wprowadzić do zakonów osoby oddane grzechowi, a rozprzężenie i zamiłowanie do przyjemności zmysłowych rozpowszechnione będą po całej ziemi.
>
> Francja, Włochy, Hiszpania i Anglia będą prowadziły wojnę; Francuz będzie się z bił z Francuzem, Włoch z Włochem, ulice spłyną krwią, potem nastąpi straszliwa wojna powszechna. Na pewien czas Bóg zapomni o Francji i o Włoszech, gdyż Ewangelia Jezusa Chrystusa. jest już nieznana. Źli rozwijać będą całą swą przebiegłość, ludzie będą się wzajemnie mordować i masakrować nawet w domach.
>
> Za pierwszym uderzeniem piorunami rażącego miecza gniewu Bożego góry i cała natura drżeć będą z przerażenia, albowiem rozprzężenie i zbrodnie ludzkie przebijają sklepienia niebios. Paryż zostanie spalony, Marsylia się zapadnie; ziemia trzęsąc się zburzy i pochłonie niejedno wielkie miasto, zdawać się będzie, że wszystko stracone, widzieć się będzie tylko mężobójców a słyszeć tylko bluźnierstwa i szczęk broni.
>
> Sprawiedliwi dużo wycierpią, ich modlitwy, pokuty i łzy do nieba się wzniosą, a wszystek lud Boży prosić będzie przebaczenia i miłosierdzia i prosić będzie mej pomocy i wstawiennictwa. Wówczas Jezus Chrystus - aktem swej sprawiedliwości i wielkiego miłosierdzia dla sprawiedliwych - rozkaże swym aniołom, aby wszyscy Jego nieprzyjaciele zostali skazani na śmierć. Nagle prześladowcy Kościoła Jezusa Chrystusa i wszyscy ludzie oddani grzechowi zginą, a ziemia stanie się jakby pustynią. Wtedy nastąpi pokój i pojednanie Boga z ludźmi. Wtedy ludzie będą służyć Jezusowi Chrystusowi oraz wielbić Go i chwalić, a miłość wszędzie kwitnąć będzie. Nowi królowie będą prawą ręką Kościoła, który będzie mocny, pokorny, pobożny, ubogi, gorliwy i naśladujący cnoty Jezusa Chrystusa. Ewangelia wszędzie będzie głoszona, a ludzie czynić będą wielkie postępy w wierze, ponieważ jedność panować będzie wśród pracowników Jezusa Chrystusa, a ludzie żyć będą w bojaźni Bożej.
>
> Ten pokój między ludźmi nie będzie długotrwały. Wskutek dwudziestu pięciu lat obfitych zbiorów rolnych ludzie zapomną, że ich grzechy są przyczyną wszystkich kar, które spotykają ziemię.
>
> Zwiastun Antychrysta, ze swymi międzynarodowymi wojskami, walczyć będzie przeciw prawdziwemu Chrystusowi, jedynemu Zbawcy świata, wyleje dużo krwi i będzie chciał wyrugować kult religijny, aby jego samego uważano za Boga.
>
> Oprócz dżumy i głodu, które będą powszechne, ziemię nękać będą wszelkiego rodzaju plagi, srożyć się będą wojny aż do ostatniej wojny prowadzonej przez dziesięciu królów Antychrysta, którzy będą mieli jeden cel i będą jedynymi władcami świata. Zanim to nastąpi, będzie pewien rodzaj fałszywego pokoju na świecie. Ludzie myśleć będą tylko o rozrywkach, źli oddawać się będą wszelkiego rodzaju grzechom, ale dzieci Kościoła świętego, dzieci wiary, prawdziwi naśladowcy, wrastać będą w miłości Bożej i w cnotach, które są mi najdroższe. Szczęśliwe dusze pokorne, prowadzone przez Ducha Świętego! Będę walczyła łącznie z nimi, dopóki nie dojdą do pełni wieku.
>
> Nawet natura woła o pomstę do nieba i drży z przerażenia w oczekiwaniu tego, co ma przyjść na ziemię splamioną zbrodnią. Drżyj, ziemio! i wy, których zawodem jest służyć Jezusowi Chrystusowi, a którzy wewnątrz adorujecie samych siebie, drżyjcie! Bóg wyda was swemu nieprzyjacielowi, ponieważ miejsca święte są w zgniliźnie, klasztory nie są domami Boga, lecz Asmodeusza i jego popleczników. W tym to czasie urodzi się Antychryst z zakonnicy hebrajskiej, fałszywej dziewicy, która będzie miała łączność ze starym wężem, mistrzem nieczystości; rodząc się, będzie on miotał przekleństwa, będzie miał zęby niby diabeł wcielony, będzie wydawał przerażające krzyki, czynił dziwy i żywił się nieczystościami. Jego ojcem będzie B..... Będzie miał braci, którzy, nie będąc wprawdzie demonami wcielonymi jak on, będą dziećmi zła. W dwunastym roku życia wyróżnią się oni walnymi zwycięstwami, wkrótce każdy z nich stanie na czele zastępów wspieranych przez legiony piekła.
>
> Zmienią się pory roku, ziemia wydawać będzie złe plony, zakłóceniu ulegnie regularny ruch gwiazd, księżyc odbijać będzie tylko słabe czerwonawe światło, woda i ogień wprawią kulę ziemską w konwulsyjne ruchy i straszliwe trzęsienia, które pochłoną góry, miasta itp.
>
> Rzym zatraci wiarę i zostanie stolicą Antychrysta. Demoni powietrzni wraz z Antychrystem czynić będą wielkie dziwy na ziemi i w przestworzach, a ludzie coraz przewrotniejsi stawać się będą. Wszakże Bóg troszczył się będzie o swe wierne sługi i o ludzi dobrej woli, Ewangelia będzie wszędzie głoszona, wszystkie narody i ludy znać będą prawdę.
>
> Zwracam się z naglącym apelem do ziemi, wzywam prawdziwych uczniów Boga żyjącego i królującego w niebie, wzywam prawdziwych naśladowców Chrystusa, który stał się człowiekiem, jedynego prawdziwego Zbawiciela ludzi, wzywam swe dzieci, ludzi prawdziwie mi oddanych, którzy mi się oddali, abym ich prowadziła do swego Boskiego Syna, których niejako noszę na swych rękach, którzy mym duchem żyli, wzywam wreszcie apostołów ostatnich czasów wiernych uczniów Jezusa Chrystusa, którzy żyli w pogardzie świata i siebie samych, w ubóstwie, cierpieniu i pokorze, wśród wzgardy i w milczeniu, w modlitwie i umartwieniu, w czystości i w zjednoczeniu z Bogiem, nieznani światu, czas, aby wyszli i oświecili ziemię. Idźcie i okażcie się jako me umiłowane dzieci, jestem z wami i w was, aby tylko wasza wiara była światłem, które was oświecać będzie w dniach nieszczęść. Niech wasz zapał uczyni was jakby spragnionymi chwały i czci Jezusa Chrystusa. Walczcie dzieci światła! Wy nieliczni, którzy widzicie, bo oto czas czasów i koniec końców.
>
> Kościół zostanie zaciemniony, a świat będzie w przerażeniu. Ale Enoch i Eliasz, napełnieni Duchem Bożym, nauczać będą z mocą Bożą, a ludzie dobrej woli uwierzą w Boga i wiele dusz dozna pociechy, mocą Ducha Świętego robić będą wielkie postępy i potępią diabelskie błędy Antychrysta. Biada mieszkańcom ziemi! Będą bowiem krwawe wojny i głód, dżuma i inne zaraźliwe choroby, będzie padał przerażający grad zwierząt, grzmoty i pioruny wstrząsną miastami, a ziemia dygocąc pochłonie niektóre kraje; w przestworzach głosy słyszeć się dadzą, ludzie bić będą głową o ścianę, wzywając śmierci, a śmierć będzie ich kaźnią, krew płynąć będzie ze wszech stron. Któż zwyciężyć zdoła, jeśli Bóg nie skróci czasu próby? Ale krwią, łzami i modłami sprawiedliwych Bóg da się przejednać. Enoch i Eliasz zostaną zabici, pogański Rzym zniknie, ogień z nieba spadnie i pożre trzy miasta, świat cały osłupieje z przerażenia, a wielu da się uwieść, bo nie wielbili prawdziwego Chrystusa żyjącego wśród nich. Nadszedł czas, słońce się zaćmiło, jedynie wiara będzie żyła.
>
> Nadszedł czas - otchłań się otwiera. Oto bestia ze swymi poddanymi, zwąca się zbawicielem świata, oto król królów ciemności. Uniesiony pychą wzbija się on w przestworza, by nieba dosięgnąć, ale zmieciony zostaje tchnieniem świętego Michała Archanioła. Spada zatem, a ziemia, która już od trzech dni była nieustanną igraszką chaotycznych wirów, otwiera swe ogniste łono, a wtedy on i wszyscy jego zwolennicy na zawsze pochłonięci zostają w wiekuistych czeluściach piekła. Kiedy to się stanie, woda i ogień oczyszczą ziemię i strawią wszystkie dzieła pychy ludzkiej i wszystko zostanie odnowione, wówczas ludzie będą Bogu służyć i chwałę oddawać."

---

## IV

Następnie **Najświętsza Dziewica podyktowała mi, także po francusku, regułę nowego zakonu.**

Podyktowawszy mi regułę tego nowego zakonu, Najśw. Maryja Panna tak dalej zaczęła mówić (gwarą):

> "Jeżeli ludzie się nawrócą, kamienie i skały zamienią się w zboże, a ziemniaki zasadzone będą na każdym polu.
>
> Moje dzieci, czy dobrze odmawiacie pacierz?"

Na to odpowiedzieliśmy oboje: O! nie, proszę Pani, nie za bardzo.

> "Ach, moje dzieci, trzeba go dobrze odmawiać rano i wieczorem. Jeśli nie będziecie mogli więcej, odmówcie tylko Ojcze nasz i Zdrowaś Maryjo, a gdy będziecie mieli czas i możność, odmówcie więcej.
>
> Na mszę św. przychodzi tylko kilka starszych kobiet, inni całe lato pracują w niedzielę, zimą zaś, gdy nie wiedzą co robić, przychodzą do kościoła, ale tylko po to, aby szydzić z religii. W wielki post idą do wędliniarni jak psy.
>
> Nie widziałyście dzieci moje, zepsutego zboża."
>
> *Oboje odpowiedzieliśmy: Nie, proszę Pani. Wtedy Najświętsza Panna, zwróciwszy się do Maksymina, rzekła:* "Ależ ty, moje dziecko, powinieneś je widzieć, gdy razu pewnego przechodziłeś z ojcem w pobliżu Coin. Właściciel pola zwrócił się wtedy do twego ojca, aby zobaczył, jak psuje się pszenica. Wtedy ojciec wziął parę kłosów, roztarł je na dłoni i wszystko rozpadło się w pył. Następnie, gdy wracając do domu, mieliście do Corps tylko pół godziny marszu ojciec dał ci kawałek chleba mówiąc:
>
> Masz, moje dziecko, jedz tego roku, bo nie wiem, kto będzie jadł na przyszły rok, skoro pszenica tak się psuje." *Maksymin odpowiedział, że tak było istotnie, tylko on zapomniał o tym epizodzie.*
>
> *Najświętsza Maryja Panna zakończyła swe przemówienie słowami:* "No, moje dzieci, przekażecie te słowa całemu mojemu ludowi."

 Odchodząc, Piękna Pani przeszła potok, my zaś podążaliśrny za Nią oczyma, bo Ona przyciągała nas ku sobie swym blaskiem a jeszcze bardziej swą dobrocią, która mnie upajała. Gdy oddaliła się o dwa kroki od potoku, nie odwracając się do nas, powtórzyła:

> "No, moje dzieci, przekażecie te słowa całemu mojemu ludowi."

Następnie szła dalej aż do miejsca, gdzie niedawno ja weszłam, aby zobaczyć, gdzie są moje krowy. Jej stopy dotykały tylko wierzchołka traw wcale ich nie uginając. Wszedłszy na to wzniesienie, zatrzymała się, ja zaś w mig znalazłam się przed Nią, aby móc Ją dobrze; dobrze widzieć i aby zobaczyć, którą drogą zamierza iść. Co się ze mną działo, trudno mi opowiedzieć. W każdym razie zapomniałam o swoich krowach, o gospodarzach, u których byłam na służbie. Przywiązałam się zawsze i bezwarunkowo do swej Pani. O, chciałam żeby już nigdy, nigdy z Nią się nie rozstawać, szłam za Nią bez zastanawiania się, jedynie z postanowieniem słuchania Jej po wszystkie dni swego życia.

Zapomniałam nawet o swym raju. Ożywiało mnie tylko jedno pragnienie: służyć Jej we wszystkimi wydawało mi się, że zdołam wszystko zrobić, co mi poleci, byłam bowiem przekonana, że Ona ma wielką władzę. Patrzyła na mnie z czułą dobrocią, która mnie przyciągała do Niej, chciałabym z zamkniętymi oczyma rzucić się w Jej objęcia. Nie miałam jednak na: to dość czasu. Uniósłszy się powoli od ziemi na wysokość więcej niż jednego metra i pozostając tak chwileczkę w powietrzu, moja Piękna Pani popatrzyła w niebo, następnie na ziemię - na prawo, na lewo, potem na mnie, ale wzrokiem tak słodkim, tak miłym i tak dobrym, iż zdawało mi się, że przyciąga mnie Ona do swego wnętrza i że moje serce otwierało się do Jej Serca.

I podczas gdy serce moje tajało w słodkim uczuciu, piękna postać dobrej Pani powoli znikała, zdawało mi się, że światło w ruchu skupiało się i zagęszczało: wokół Niej, abym nie mogła długo Jej widzieć.

Tak więc światło zajmowało miejsce części ciała, które znikały z mych oczu, albo raczej wydawało mi się, że ciało mej Pani, roztapiając się; stawało się światłem. Kula tego światła wznosiła się łagodnym ruchem na prawo ode mnie. Nie mogę powiedzieć, czy masa światła zmniejszała się w miarę wznoszenia Pani, czy też oddalanie sprawiało, że widziałam zmniejszanie światła wraz z jej oddalaniem, wiem tylko to, że długo pozostawałam z podniesioną głową i oczyma utkwionymi w światło, nawet wtenczas gdy stale się oddalając i zmniejszając swą objętość, zniknęło wreszcie. Wówczas oderwawszy wzrok od firmamentu, rozglądam się wokół i widząc Maksymina patrzącego na mnie, mówię mu: Maksiu, to musi być dobry Bóg mojego ojca albo Matka Boska, albo jakaś wielka święta". A Maksymin, wznosząc rękę do nieba, odpowie: Ach, gdybym wiedział o tym!

---

## V

Tego dnia wieczorem opuściliśmy pastwisko nieco wcześniej aniżeli zwykle. Przybywszy do zagrody, zajęłam się przywiązywaniem krów i porządkowaniem wszystkiego w chlewie. Jeszcze tego nie dokończyłam, gdy gospodyni przychodzi do mnie i płacząc mówi: "Dlaczego, moje dziecko, nie przyjdziesz opowiedzieć mi, co wam się zdarzyło na górze?" Okazało się, że Maksymin, nie zastawszy swych gospodarzy, którzy jeszcze nie powrócili z pola, przybiegł do moich i opowiedział wszystko, co widział i słyszał. Odpowiedziałam jej: Chciałam wam to opowiedzieć, ale chciałam wpierw skończyć swoją robotę. W chwilę potem znalazłam się w mieszkaniu, a gospodyni odezwała się do mnie, mówiąc: "Opowiedz, coście widzieli; Maksuś, pastuszek Piotra Selme, wszystko mi opowiedział." 

Opowiadam; ale oto w połowie sprawozdania przyszli z pola inni członkowie rodziny, a gospodyni, która płakała słysząc skargi naszej tkliwej Matki, rzecze: "Ach, chcieliście jutro, w niedzielę zbierać z pola pszenicę; nie róbcie tego. Chodźcie posłuchać, co się dzisiaj zdarzyło temu dziecku i chłopakowi Piotra Selme: Następnie zwracając się do mnie, mówi: "Powtórz to; coś powiedziała. Zaczęłam, a gdy skończyłam, gospodarz powiedział: To Matka Boska albo może jakaś wielka święta przyszła z polecenia dobrego Boga, ale to tak wygląda, jak by sam dobry Bóg przyszedł. Trzeba robić to, co ta święta powiedziała. Jak ty to zrobisz, aby wszystko to powiedzieć Jej ludowi? Odpowiedziałam: "Powiedzcie mi, jak mam robić, a ja będę tak robić". On zaś, spojrzawszy na swą matkę, żonę i brata, dodał: "Trzeba o tym pomyśleć", następnie każdy zajął się swoimi sprawami.

Było już po kolacji. Gospodarze Maksymina przyszli do moich, aby im opowiedzieć, co usłyszeli od swego pastuszka, i aby się razem naradzić, co trzeba robić. Pastuszek też był z nimi. "Wydaje się nam - oświadczyli - że to Najświętszą Maryję Pannę przysłał Bóg; domyślamy się tego z Jej słów. Ona im poleciła, żeby te słowa przekazali całemu Jej ludowi. Trzeba więc chyba, żeby dzieci poszły w świat i opowiadały wszystkim, że muszą zachowywać przykazania dobrego Boga, inaczej wielkie nieszczęścia nas spotkają."

Po chwili milczenia mój gospodarz, zwracając się do mnie i do Maksymina, zapytał: "Czy wiecie, moje dzieci, co macie robić? Jutro wstańcie wcześnie, idźcie oboje do ks. proboszcza i opowiedzcie mu wszystko coście widzieli i słyszeli, powiecie mu dokładnie, jak się rzeczy miały, a on wam powie, co macie robić."

Na drugi dzień po objawieniu, 20 września, poszłam wcześnie rano z Maksyminem. Przybywszy na plebanię, pukam do drzwi. Pokojówka księdza proboszcza przyszła otworzyć i pyta, czego chcemy, odpowiadam jej (po francusku, mimo że nigdy nie posługiwałam się tym językiem): Chcieliśmy porozmawiać z ks. proboszczem. - A co chcecie mu powiedzieć? - zapytała - Proszę Pani - ciągnę dalej - chcemy mu powiedzieć, że wczoraj, gdyśmy paśli krowy na górze Baisses, po naszym obiedzie przyszła do nas jakaś piękna Pani... I tak dalej snując opowiadanie, doszliśmy do połowy przemówienia Najśw. Maryi Panny. Wtem z kościoła doszedł dźwięk dzwonka: było to ostatnie uderzenie przed mszą św. Wtedy ks. Perrin, proboszcz la Salette, który nas słyszał otworzył z hałasem drzwi i bijąc się w piersi, rzekł do nas przez łzy: «Moje dzieci, jesteśmy zgubieni, Bóg nas ukarze. Ach! mój Boże, to Matka Najświętsza wam się objawiła.» I wyszedł ze Mszą św. Popatrzyliśmy na siebie z Maksyminem i pokojówką; następnie Maksymin powiedział mi: Odchodzę do swego ojca w Corps. I tak rozstaliśmy się.

Ponieważ gospodarze nie polecili mi wrócić natychmiast po rozmowie z ks. proboszczem; sądziłam, że uczestnicząc we mszy św. nie postąpię źle. Poszłam więc do kościoła. Rozpoczęła się msza św. Po pierwszej Ewangelii ksiądz proboszcz odwraca się do ludu i usiłuje opowiedzieć swym parafianom objawienie; które wczoraj miało miejsce na jednej z ich gór, i napomina ich, by nie pracowali w niedziel jego głos był przerywany szlochem i cały lud był bardzo, bardzo wzruszony. Po mszy świętej powróciłam do swych gospodarzy. Tegoż dnia przyszedł p. Peytard, który jeszcze dziś jest mérem la Salette, aby mnie wypytać co do objawienia i przekonawszy się o prawdzie tego, co mu opowiedziałam, odszedł przekonany.

Pozostawałam u swych gospodarzy na służbie aż do Wszystkich Świętych. Następnie umieszczono mnie jako pensjonarkę u Sióstr Opatrzności Bożej w Corps, mojej rodzinnej wsi.

---

## VI

Najświętsza Maryja Panna była wysoka i zbudowana proporcjonalnie. Była tak lekka, że - jak mi się wydawało - najlżejsze tchnienie wiatru mogłoby Ją poruszyć, a jednak stała mocno i nieruchomo. Jej postawa budziła pełen szacunku lęk. Z jej oblicza bił majestat, lecz nie taki, jakim imponują wielcy tego świata. Majestat ten narzucał szacunek zmieszany z miłością, ale też przyciągał do Niej: Wzrok miała łagodny a przenikliwy. Jej, oczy zdawały się mówić do moich, ale moje odpowiedzi wypływały z głębokiego i żywego uczucia miłości do tego porywającego piękna, na którego widok rozpływało się serce.

Słodycz Jej spojrzenia, wyraz Jej niepojętej dobroci pozwalały zrozumieć i odczuć, że Ona przyciąga do siebie i że chce się dawać, był to wyraz miłości której nie da się opisać językiem ciała ani literami alfabetu.

Odzież Najświętszej Dziewicy była srebrnobiała i jaśniejąca. Jednakże nie było w niej nic materialnego: mieniąca się i opalizująca, składała się ze światła, dla jej opisu nie ma na ziemi ani wyrazów, ani porównań.

Najświętsza Dziewica była cała piękna i cała ukształtowana z miłości. Patrząc na Nią, tęskniłam do stopienia się z Nią. W Jej stroju jak w całej Jej osobie wszystko tchnęło majestatem; splendorem, wspaniałością niezrównanej Królowej. Wyglądała biała, niepokalana, krystaliczna, olśniewająca, niebiańska, świeża, nowa jak dziewica. Wydawało się, że słowo miłość stale wymykało się z Jej różanych, przeczystych ust. Pokazywała mi się jako Matka pełna dobroci, łaskawości, miłości do nas, współczucia, miłosierdzia.

Korona z róż, jaką miała na głowie, była tak piękna, tak promienna, że nie sposób jej sobie wyobrazić: wielobarwne róże nie były z ziemskich ogrodów. Był to zbiór kwiatów okalających głowę Najświętszej Panny w formie korony, ale róże zmieniały się i przemieszczały, przy czym ze środka każdej róży wypływało tak piękne światło, że zachwycało i nadawało różom olśniewającej piękności. Z różanej korony wznosiły się jakby złote gałązki, a wśród nich było mnóstwo drobniejszego kwiecia i brylantów. Całość tworzyła przepiękny diadem, który sam bardziej świecił aniżeli nasze ziemskie słońce.

Najświętsza Panna miała bardzo śliczny krzyż zawieszony na szyi. Krzyż ten wydawał się nie platerowany, lecz pozłacany, widywałam bowiem czasami przedmioty pozłacane z różnymi odcieniami złota, co dla moich oczu robiło o wiele przyjemniejsze wrażenie aniżeli zwykła złota blaszka. Na tym pięknym krzyżu, który cały promieniował światłem, był Chrystus, Pan nasz, z ramionami wyciągniętymi. Na obu krańcach poprzecznej belki krzyża znajdowały się: z jednej strony obcęgi, z drugiej młotek: Chrystus był naturalnego koloru ciała, ale jaśniał wielkim blaskiem, a światło bijące z całego Jego ciała wydawało się snopem bardzo jaśniejących grotów, które przeszywały mi serce, wzniecając w nim pragnienie stopienia się z Nim. Niekiedy Chrystus wydał się martwy, Jego głowa była pochylona, ciało jakby obwisłe, tak iżby upadło, gdyby go nie przytrzymywały gwoździe, którymi było przybite do krzyża.

Miałam dla Jezusa gorące współczucie i chciałabym światu całemu przedstawić Jego nieznaną miłość, wlać w dusze śmiertelników najczulszą miłość i wdzięczność najwyższą dla Boga, który bynajmniej nie potrzebował nas, aby być tym, czym jest, czym był i czym będzie zawsze; a jednak, niepojęta miłości do człowieka! - stał się człowiekiem i zechciał umrzeć - tak, umrzeć - aby lepiej wyryć w naszej duszy i w naszej pamięci wyobrażenie szalonej miłości, którą żywi dla nas. Och, jakże ja nieszczęśliwa, że nie mogę wyrazić miłości, jaką nasz dobry Zbawiciel żywi dla nas! Ale z drugiej strony jakże my szczęśliwi, że możemy lepiej czuć to, czego nie możemy wyrazić!

Innymi znów razy Chrystus wydawał się żyjący; głowę trzymał prosto, oczy miał otwarte i zdawał się wisieć na krzyżu własnowolnie. Czasem także zdawał się mówić, dowodzić, że wisiał na krzyżu dla nas, z miłości do nas, aby nas przyciągnąć do swej miłości; dowodzić, że zawsze ma nową miłość do nas, że Jego miłość od żłóbka do krzyża i aż do dzisiaj i na wieki jest zawsze ta sama.

Najświętsza Maryja Panna płakała niemal nieustannie, gdy do mnie mówiła, łzy Jej spływały powoli, jedna po drugiej, aż do kolan, następnie znikały jak iskry światła. Były one jaśniejące, nabrzmiałe miłością. Chciałam ją pocieszyć, aby już nie płakała. Wydawało mi się jednak, że Ona odczuwała potrzebę pokazania swych łez, aby lepiej pokazać swą miłość, o której ludzie zapominają. Chciałabym rzucić się w Jej ramiona i powiedzieć: "Moja dobra Matko, nie płacz! Ja chcę Cię kochać za wszystkich ludzi na całej ziemi". Ale Ona zdawała się mówić do mnie: Ach, tylu ich jest, którzy mnie nie znają!

Byłam między śmiercią a życiem, widząc z jednej strony tyle miłości, tyle pragnienia być kochana z drugiej tyle zimna i obojętności... O moja Matko, Matko Najświętsza, najmilsza, moja miłości, serce mego serca!

Łzy naszej tkliwej Matki bynajmniej nie umniejszały Jej majestatu Królowej i Pani, lecz - przeciwnie - podnosiły go; czyniły Ją piękniejszą, potężniejszą, pełniejszą miłości, bardziej macierzyńską i zachwycającą. Chciałabym pić te łzy, na których widok współczucie i miłość rozsadzały mi serce. Widzieć płacz Matki i to takiej! Matki, a nie przedsięwziąć wszelkich możliwych środków, aby Ją pocieszyć, przemienić Jej bóle w radość - czyż to da się wyobrazić? O Matko bardziej aniżeli dobra, jesteś utworzona ze wszystkich przywilejów, których Bóg zdolny jest udzielić. Tyś niejako wyczerpała wszechmoc Boga, jesteś dobra, a dobra dobrocią samego Boga. Bóg w tobie stał się większym, tworząc swoje największe arcydzieło na ziemi i niebie.

Najświętsza Dziewica miała żółty fartuszek. Żółty... cóż ja mówię? - jaśniejszy aniżeli połączony blask wielu słońc. Nie była to tkanina materialna, ale upostaciowanie chwały, a chwała ta była promienna i porywającej piękności. Wszystko w Najświętszej Pannie sprawiało, że usilnie i jakby z niepohamowaną mocą poślizgu dążyłam do mego Jezusa, aby Go adorować i kochać we wszystkich stanach Jego ziemskiego życia.

Dwa łańcuszki miała Najświętsza Dziewica: jeden dłuższy, drugi krótszy. Na krótszym wisiał krzyżyk, o którym już mówiłam. Łańcuszki te - gdyż ostatecznie trzeba te przedmioty jakoś nazwać - to raczej promienie chwały wielkiej jasności, mieniące się wszystkimi blaskami brylantu. Buciki - jakąż inną nadać im nazwę? - były srebrzystej promieniującej bieli, a wokół nich róże, róże zachwycająco piękne, rzucające promienie, które przykuwały wzrok: Na każdym buciku sprzączka ze złota, ale nie ziemskiego, lecz rajskiego.

Widok Dziewicy sam stanowił doskonały raj: Miała: Ona w sobie wszystko, co może zadowolić człowieka, zapomniałam bowiem o ziemi. Otaczały ją dwa światła. Pierwsze bliższe Niej; jaśniejące przepięknym i bardzo migotliwym blaskiem; docierało aż do nas.

Drugie, nieruchomo, to znaczy nie iskrzące się, ale o wiele jaśniejsze aniżeli nasze biedne ziemskie słońce, rozprzestrzeniało się wokół Pięknej Pani nieco dalej i również nas obejmowało swą sferą. Wszystkie te światła nie raziły w oczy i wcale nie nużyły wzroku.

Oprócz tych wszystkich świateł, tego przepychu blasków, smugi bądź też promienie światła wypływały z ciała Dziewicy, z Jej odzieży, zewsząd.

Jej słodki głos czarował; zachwycał, uspokajał, łagodził, usuwał wszystkie trudności, koił i karmił serce. Zdawało mi się, że chciałabym zawsze upajać się jego melodią, a serce moje tańczyło z radości i wyrywało się, by w nim się rozpłynąć. Oczu Przeczystej Dziewicy, naszej najczulszej Matki, niepodobna opisać ludzkim językiem. Aby o nich mówić, trzeba by być serafinem, trzeba by posługiwać się językiem Boga samego, tego Boga, z którego rąk wyszła jako niepokalana, jako arcydzieło Jego wszechmocy; Oczy przedostojnej Maryi Panny były tysiące i tysiące razy piękniejsze niż brylanty i wszystkie najbardziej wyszukane klejnoty, błyszczały jak dwa słońca; były łagodne łagodnością samą, czyste jak zwierciadła. W oczach tych widziało się raj. One przyciągały do Niej, świadczyły, że Ona chce dawać siebie i przyciągać innych do siebie.

Im dłużej na Nią patrzyłam, tym dłużej chciałam Ją widzieć; im dłużej Ją widziałam, tym bardziej Ją kochałam, a kochałam ze wszystkich sił.

Ach, te oczy!... Oczy pięknej Niepokalanej były jak brama Boża, brama z której widzi się wszystko to, co może upoić duszę. Gdy me oczy spotkały się z oczyma Matki Boga i mojej Matki, uczułam w sobie szczęśliwy przewrót miłości, a zapewnienie miłowania Jej i roztopienia się w Niej z miłości - samorzutnié zrodziło się we mnie. Gdyśmy patrzyły na siebie, nasze oczy rozmawiały z sobą na swój sposób, ja zaś tak Ją kochałam, że chciałabym ucałować Ją w te oczy, które rozczulały mą duszę i zdawały się ją przyciągać, aby ją stopić z Jej duszą. Jej oczy wprawiały całą mą istotę w słodkie drżenie i bałam się zrobić jakikolwiek ruch, który mógłby choć trochę być Jej nieprzyjemny.

Sam widok oczu Najczystszej z dziewic wystarczyłby za niebo dla błogosławionego, wystarczyłby, aby wprowadzić duszę w pełnię zamiarów Najwyższego wśród wydarzeń zachodzących w ciągu ziemskiego życia. Wystarczyłby, aby dusza ta nie ustawała w aktach czci, dziękczynienia, wynagrodzenia i przebłagania. Sam ten widok wprowadza duszę w Boga i sprawia, że staje się jakby umarłą za życia, że na wszystkie rzeczy tego świata - nawet na te, które się wydają najważniejsze - patrzy jak na dziecinne zabawki, że pragnie rozmów tylko o Bogu i o tym, co dotyczy Jego chwały.

Grzech jest jedynym złem, które ona widzi na ziemi. Umarłaby na skutek tego z bólu, gdyby Bóg jej nie podtrzymywał. Amen".

---

Castellamare, 21 listopada 1878 r.

Maria od Krzyża, ofiara Jezusowa, z domu Melania Calvat, pasterka z la Salette.

---

Nihil Obstat: Imprimatur datum Lycii ex Curia Episc. \
die 15 Nov. 1879 \
Vicarius Generalis - Carmelus Archus Cosma



