---
title: Contact
menu: true
weight: 6
---

L’association de Amis de Mélanie Calvat propose sur demande des exposés / conférences sur Mélanie Calvat et L’apparition de Notre-Dame à La Salette. Ses membres sont disposés à se déplacer et effectuer ces conférences gratuitement en France.

Vous pouvez nous contacter à l'adresse : {{< cloakemail "contact@melanie-de-la-salette.fr" >}}. Nous vous proposons de réaliser des conférences sur la Salette.

Notre association est active pour rééditer des livres sur la Salette et faire
connaître le message de la Salette et sa messagère. Si vous souhaitez nous
aider, vous pouvez nous contacter à l'adresse ci-dessus, et si vous souhaitez
nous faire un don, cela est possible par virement sur notre compte :

<center>Nom du titulaire&nbsp;: Les amis de Mélanie Calvat
  <br/>IBAN&nbsp;: FR76 1027 8089 3200 0217 2680 150
  <br/>BIC&nbsp;: CMCIFR2A</center>

Il est également possible de [faire un don par carte bancaire](https://donate.stripe.com/6oEdTm3PG0Ng2XedQQ).
