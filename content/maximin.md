---
menu: true
menutitle: Maximin
title: Histoire de Maximin
date: 2022-12-14T19:18:20.249+01:00
weight: 2
related:
  - /bibliotheque/maximin-giraud-par-maurice-canioni
---

> Bienhereux ceux qui souffrent persécution pour la justice, car le royaume des
> cieux est à eux.

<cite>Matthieu V,10</cite>

---

Maximin a beaucoup été calomnié et Mélanie aurait certainement souhaité le
défendre car elle le tenait en haute estime comme le montre la lettre qu'elle
écrivit à l'abbé Baillif le 20 février 1881 :

> Dans la souffrance il ne voulait pas s'apercevoir qu'il souffrait. Je crois
> qu'il a beaucoup souffert et toujours en silence ; en vérité je suis couverte
> de confusion quand je vois combien je suis éloignée de toute sa vie cachée en
> Dieu ; et si je parviens à arriver au ciel, je ne toucherai même pas les
> chevilles de ses pieds. Souvent je le prie de m'obtenir cette générosité d'âme
> qui me serait si nécessaire.

---

Pierre Maximin est né de Jean Giraud, son père, charron de profession, le 26
août 1835, et fut baptisé à Corps. Sa mère, Anne-Marie née Templier et
originaire de Corps avait 26 and à la naissance de Maximin mais quitta ce monde
très tôt lorsque Maximin avait un an et demi.

Son père se remaria avec Marie-Magdeleine Court que l'on appelait la *marâtre*
de Maximin, non qu'elle étant méchante avec lui, mais c'était le mot patois pour
parler d'une belle-mère. C'était une bonne chrétienne et Maximin indique dans
ses écrits son affection pour elle :

> pendant que je raconte ce que j'ai vu et entendu la veille, ma chère marâtre
> réfléchit et me regarde attentivement comme une personne qui veut croire, qui
> croit même mais a qui le fait paraît tellement surprenant qu'elle voudrait
> voir en moi un miracle a l'appui de mes paroles. Le récit terminé, elle
> m'embrasse, elle pleure.

Son père, moins bon chrétien, se convertit au récit répété de son fils. Ce qui
le décide c'est l'épisode ou la Belle Dame raconte l'épisode entre Maximin et
son père sur les épis qui se gâtent. Maximim n'aurait jamais pu se souvenir de
cela par lui même, lui qui n'avait pas bonne mémoire. Il fait ériger une croix
en bois de 1 mètre 50 sur le lieu de l'apparition.

Le bon évêque de Grenoble, Monseigneur de Bruillard confia Maximin en novembre
1946, l'année même de l'apparition, aux sœurs de la Providence à Corps pour son
instruction. Malgré son retard scolaire, il y met bonne volonté et progresse
rapidement.

Le petit Mémin, comme on l'appelait, était un garçon fort remuant mais sans
malice comme l'indique Monseigneur Clément Villecourt, évêque de la Rochelle,
qui l'a rencontré sur la montagne de la Salette :

> Maximin, d'un caractère vif mais sans aucun emportement ne peut ouvrir la
> bouche sans inspirer de l'intérêt par la suavité de sa parole et la candeur
> avec laquelle il s'exprime. *[...]* Il a toujours alors quelque chose à
> remuer. *[...]* Il aime les jeux et les amusements autant pour le moins que
> les autres enfants de son âge. Il ne prend aucune précaution pour dissimuler
> cet attrait... Il est généreux et désintéressé : il se dépouillerait de tout
> ce qu'il a pour vous le donner... Il est si pur qu'il n'a même pas l'idée du
> vice... Il va a la prière avec le même bonheur et le même empressementqu'il se
> porte aux jeux de l'enfance. Son attrait est de servir la messe. *[...]* On
> conçoit à peine qu'un enfant qui se montre avec un caractère naturellement
> volage, soit si ferme et si constant à garder le secret de ce qu'il ne doit
> pas dire...
>
> <cite>Maximin Giraud p. 32</cite>

Maximin est en effet d'une générosité sans pareille comme le témoigne mère
Saint-Thècle du couvent de la Providence :

> Oui, Maximin était très bon, généreux et aimait les pauvres. La première fois
> qu'il fut doté de deux paires de pantalons, il en était fier, heureux. Un jour
> il aperçut une pauvre femme à notre porte, demandant l'aumône, ayant à côté
> d'elle un petit garçon de l'âge de Mémin, couvert de haillons. L'enfant de la
> Salette court à sa petite chambrette, quitte ses vieux pantalons pour revêtir
> ceux du dimanche et revient tout joyeux, apportant les autres au petit pauvre
> auquel il avait dit d'attendre

Après sa première communion, encore au couvent de la Providence, va devoir
affronter les difficultés que le Bon Dieu avait prévu pour lui. Il va perdre son
demi frère Jean-François en janvier 1847, il avait neuf ans. Un an plus tard, sa
bonne marâtre le quitte à son tour et son père ne tarde pas à la suivre en
février 1849.

A ce moment, Monseigneur de Bruillard n'avait toujours pas rendu son jugement
sur l'apparition, et Maximin était pressé par de nombreux clercs afin de
découvrir son secret et le presser de toutes part à des fins politiques. C'est
dans ce contexte qu'il rencontra le curé d'Ars, par l'entremise de l'évêque
intriguant de Lyon qui l'accuse de menteur.

C'est ainsi que l'entrevue entre le petit Mémin et Saint Jean-Marie Vianney se
déroule. Le saint curé n'entant pas bien les réponses de l'enfant et les
interprète mal. Lorsqu'il demanda a Maximin si il avait menti, celui-ci avec sa
conscience pure avoua qu'il avait menti par omission à son curé de Corps ne lui
avouant pas qu'il n'aimait pas apprendre ses leçons mais le saint curé pensa
qu'il mentait sur l'apparition, doute qu'il n'a réussi a dissiper que par la
divine providence des années plus tard lorsqu'il demandera un signe.

Maximin cherchait des lumières concernant sa vocation. Il voulait se faire
prêtre missionnaire pour prêcher la sainte religion aux protestants dans les
montagnes, mais n'avait pas la certitude de ce qu'il devait finalement faire. Il
avait cependant saisi le principal comme l'indique un prêtre avec qui il parla :

> Avec votre caractère qui ne cherche que l'amusement, vous qui aimez tant à
> jouer, vous si léger et si volage, au lieu de prêcher vos sauvages, je crains
> fort que vous ne fassiez que rire avec eux et que vous n'en convertissiez
> guère. \
> — Tout le contraire ! \
> — Comment tout le contraire ? \
> — Puisqu'on prend plus de mouches avec le miel qu'avec le vinaigre, je ne les
> gronderai pas. Au lieu de leur montrer un visage austère et chagrin, je leur
> ferai aimer la religion bellement. je leur apprendrai a s'amuser bravement
> sans offenser Dieu depuis la messe jusqu'aux vêpres et depuis vêpres jusqu'à
> la prière (du soir). je leur apprendrai tout ce que les bons chrétiens doivent
> savoir. \
> — Les feriez-vous se confesser souvent ? \
> — Oh ! Mes sauvages ne feront pas de peché. Pour communier le Dimanche, ils
> iront à confesse le samedi, mais ils n'auront guère de péchés, allez !
>
>
> <cite>Maximin Giraud p. 50</cite>

C'est dans ces circonstances que Maximin entra au petit séminaire de la
Côte-Saint-André puis au grand séminaire à Dax. Cependant, la vocation
sacerdotale n'était sans doute pas pour lui car il y renonça après avoir
questionné son cœur et entouré de conseillers sages et bienveillants.

Nous pourrions penser que Maximin rata sa vie comme ne nombreuses personnes
n'hésitaient pas a publier, mais le bon Dieu ne promet à ses âmes chéries le
bonheur dans ce monde mais dans l'autre.

Maximin est lors embauché à la perception de la Tronche mais est renvoyé pour
insuffisance en calcul, il cherche un emploi chez un notaire de la capitale mais
son orthographe n'était pas parfaite, il est également renvoyé. À 24 ans,
l'année 1859, il se retrouve alors dans la capitale sans un sou en poche,
couchant dehors et faisant le métier de porte-faix. Il connaît alors la faim
mais l'intervention providentielle de Saint Joseph le remettra sur pieds après
une prière à Saint-Sulpice.

Il trouve alors un emploi à l'Hospice impérial de Vésinet qu'il finit par perdre
en raison de l'intolérance religieuse qui règne dans l'administration
napoléonienne. Cependant il trouve les ressources nécessaires pour parfaire ses
études au collège de Tonnerre dans l'Yonne. En 1861, à l'occasion d'une maladie
ou il est hospitalisé à l'hôpital Saint-Louis et soigné par le docteur Portalès,
il se découvre une attirance pour la vocation de médecin et entre à la faculté
de médecine la même année grâce au secours providentiel de M. et Mme Jourdain
qu'il rencontre à l'occasion d'une cérémonie en l'honneur de Notre-Dame de la
Salette et qui deviendront ses parents adoptifs.

Il est assidu et fait la joie de ses professeurs mais au cours de ses études, le
docteur Portalès qui l'a déjà soigné devra lui annoncer en 1863 une maladie
qui touche à son cœur, fruit des souffrances extrêmes qu'il endura à Paris
quatre ans plus tôt. Cela ne l'empêche cependant pas de poursuivre ses études.

Maximin finit par se rendre compte grâce aux conseils d'un de ses professeurs
qui le tenait en haute estime qu'il aurait bien du mal a exercer son métier de
médecin plus tard. Comme voyant de la Salette, nombre de personnes se presseront
chez lui pour guérir et la presse s'emparera de tous ses échecs pour accabler
l'Église. Cela lui montre que la médecine, bien que carrière prometteuse,
n'était pas pour lui et il quitte alors ses études.

Il s'abandonne alors à la Providence, et est rejeté de toute part, notamment par
l'évêque de Grenoble qui n'était plus Monseigneur de Bruillard, et par les
missionnaires du sanctuaire de la Salette. Sur l'inspiration des Trappistes et
des Chartreux, il a l'idée de s'associer pour devenir liquoriste mais il est
abusé par son associé et se retrouve plus endetté qu'au départ.

> Malheur aux Princes de l'Église qui ne seront occupés qu'à entasser richesse
> sur richesse, qu'a sauvegarder leur autorité et à dominer avec orgueil.
>
> <cite>Secret de Mélanie</cite>

En 1865, lors d'un appel à volontaires par le Pape Pie IX dont la papauté était
en danger, il répond comme bon nombre d'autres français et se fait zouave
pontifical le temps de cette mission auprès du Saint Père.

On a souvent accusé Maximin de d'ivrognerie, mais même si il aimait le bon vin,
et ne dédaignait pas un verre à table, il savait se restreindre et ne perdait
jamais la raison. Voici un témoignage de Henri le Chauff de Kerguennec, zouave
en même temps que lui :

> Hier, j'ai voulu constater par moi-même si vraiment il dépassait les limites
> de la tempérance et j'ai remarqué qu'un seul verre de mauvais vin lui déliait
> la langue et lui faisait monter le sang à la tête. C'est donc plutôt une
> affaire de tempérament ; peut être chez lui le cœur ne fonctionne-t-il pas
> trop régulièrement. Puis ce qui passait inaperçu chez un autre est observé
> forcément chez Maximin.

Début 1874, la maladie le terrasse. Cela commence par une crise d'oppression
comme ce que craignait le docteur Portalès qui l'avait soigné une douzaine
d'années plus tôt. Il se remet peu à peu avec les soins de ses parents adoptifs
à Corps, mais leur boutique d'objets de piété fonctionne mal et ils restent
pauvres. Sans pain ni viande, ni pour lui ni pour ses parents adoptifs, il ne
peut se payer les médicaments prescrits et dépérit peu à peu. Il demande de
l'aide mais obtient si peu !

> O mon très Révérand père ! si vous saviez comme je suis malheureux, vous
> viendrez a mon secours sans que je le demande. Pour moi, c'ést toujours si
> humiliant de demander !
>
> <cite>Lettre de Maximin au supérieur des missionnaires du sanctuaire</cite>

Malgré sa situation, la moindre amélioration de son état suffit à lui rendre sa
gaité naturelle. Il cherche secours auprès de l'évêque de Grenoble et obtient
péniblement 180 francs pour l'hiver. Le voyage de retour à Corps est pénible et
son état s'aggrave. Mme Jourdain le veille jour et nuit en cette fin d'hiver
1875 et il passe ses derniers jours ainsi entouré de ses plus proches et reçoit
les derniers sacrements avec une grande piété.

Il terminera sa vie en disant *« Que la très sainte volonté de Dieu
s'accomplisse en toutes choses ! »*. Ses obsèques ont lieu à Corps le 3 mars
1875, il n'avait alors même pas 40 ans.

---

Résumé tiré du [livre de Maurice Canioni : « Maximin Giraud, témoin de la Belle
Dame, Portrait véritable »]({{< relref "/bibliotheque/maximin-giraud-par-maurice-canioni" >}})
