---
menu: true
menutitle: Apôtres des derniers temps
title: Apôtres des derniers temps
weight: 4
related:
  - /bibliotheque/saint-louis-marie-traite-vraie-devotion
---

## Notre-Dame appelle ses dévots dans l'apparition 

> J’adresse un pressant appel à la terre : j’appelle les vrais disciples du Dieu vivant et régnant dans les cieux ; j’appelle les vrais imitateurs du Christ fait homme, le seul et vrai Sauveur des hommes ; j’appelle mes enfants, mes vrais dévots, ceux qui se sont donnés à moi pour que je les conduise à mon divin Fils, ceux que je porte pour ainsi dire dans mes bras, ceux qui ont vécu de mon esprit ; enfin j’appelle les Apôtres des derniers temps, les fidèles disciples de Jésus-Christ qui ont vécu dans un mépris du monde et d’eux-mêmes, dans la pauvreté et dans l’humilité, dans le mépris et dans le silence, dans l’oraison et dans la mortification, dans la chasteté et dans l’union avec Dieu, dans la souffrance et inconnus du monde. Il est temps qu’ils sortent et viennent éclairer la terre. Allez, et montrez-vous comme mes enfants chéris ; je suis avec vous et en vous, pourvu que votre foi soit la lumière qui vous éclaire dans ces jours de malheurs. Que votre zèle vous rende comme des affamés pour la gloire et l’honneur de Jésus-Christ. Combattez, enfants de lumière, vous, petit nombre qui y voyez ; car voici le temps des temps, la fin des fins.
>
> <cite>Sainte Vierge, 19 septembre 1846</cite>

---

## «La vue» des apôtres des derniers temps par Mélanie

Le texte de « LA VUE », ci-dessous, est tiré du livre «sœur Marie de la Croix, bergère de la Salette » écrit par Monsieur l'Abbé Gouin (pages 171 à 175, éditions Tequi de 1970).


> En même temps que la Très Sainte Vierge me donnait les Règles et me parlait des Apôtres des derniers temps, je voyais une immense plaine avec des monticules. Mes yeux voyaient tout. J’ignore si c’étaient les yeux du corps... Mais je serais mieux selon la vérité si je disais que je vis la terre au-dessous de moi, de manière que je voyais l’univers entier avec ses habitants, vaquant à leurs occupations, chacun selon son état (non pas tous par justice, mais bien par ambition. Et, par un juste châtiment de Dieu, ils étaient en guerre avec eux-mêmes).
>
> Je vis  donc cette immense plaine avec tous ses habitants. Dans certaines parties les hommes étaient blancs ; en d’autres, ils étaient couleur bois, ou bien un peu plus ou un peu moins foncés. Je vis en d’autres pays, des hommes qui étaient presque jaunes, couleur paille bien claire et avec des yeux rouges. En d’autres pays ils étaient tout noir comme du charbon. Je vis des pays où les habitants étaient très petits de taille ; et d'autres pays où ils étaient fort grands. Eh bien, je vis que les Missionnaires et les Religieuses étaient dans ces pays et dans toutes les parties du globe.
>
> Je vis les Apôtres des derniers temps avec leur costume. Ils avaient une soutane noire, pas très fine, et sans boutons ; des agrafes en tenaient lieu, à la soutane comme à la pèlerine. Leurs chapeaux étaient très grossiers et les cornes bien formées. *[Trois cornes, a-t-elle dit.]* Leurs ceintures étaient blanches, d'un tissu grossier. Elles étaient à peu près larges comme cette ligne
> <span style="display: inline-block; width: 112mm; margin: 0 auto; border-top: medium solid currentcolor"></span>
> *[La ligne tracée par Mélanie a 112 millimètres.]*
> et les bouts pendaient presque jusqu'au bas de la soutane. Sur un des bouts de la ceinture il y avait ces trois lettres, en couleur rouge : M.P.J. (Mourir pour Jésus). Sur l'autre bout il y avait ces trois lettres en couleur bleue *[Bleu du Ciel]* E.D.M. (Enfant de Marie).
>
> Ils portaient tous une croix assez grande, suspendue au cou par un gros cordon noir ; et le bout du pied de la croix entrait dans la ceinture, du côté gauche. Mais quand ils prêchaient ou faisaient fonction religieuse, elle était suspendue sur leur poitrine. A leur ceinture, du côté droit, était suspendu un chapelet et à ce chapelet il y avait une croix sans Christ. Je vis que les Apôtres des derniers temps avaient des souliers blancs (dans les longues courses ils les portaient noirs) avec une boucle dessus.
>
> Les RELIGIEUSES QUI ENTRAIENT LES PREMIERES DANS L'ORDRE de la Mère de Dieu étaient des religieuses de la Providence de Grenoble : j'en vis DEUX et une SEULE SOEUR CONVERSE. Elles furent PARMI LES PREMIERES qui, après avoir pris l'esprit de l'Ordre, en prirent le costume, en la fête de l'incarnation du divin Rédempteur. *[Elles ne seront pas les premières. Toutefois les premières n'auront pas encore le costume, quand celles de Corenc se présenteront]* Je vis qu'elles avaient la robe noire et grossière, faite presque comme un sac *[c'est-à-dire, sans taille]*, les manches larges. Leurs souliers étaient blancs, avec les boucles dessus. La ceinture, le chapelet et la Croix étaient comme ceux des Pères. Elles n'avaient pas de bonnet, mais une chose blanche qui encadrait la figure. Au-dessus était un voile noir, qui pendait assez bas par derrière. Elles avaient une espèce de pèlerine blanche.
>
> Je vis que les missionnaires prêchaient, confessaient, assistaient les mourants, donnaient des retraites : aux prêtres *[dans une conversation elle a dit aussi "Aux évêques"]*, aux rois et à ceux qui composaient leur cour, aux grands, aux soldats, aux employés, aux pauvres, aux enfants, aux religieux, aux religieuses, aux femmes et aux vierges. Je vis, en certains endroits, des Missionnaires auprès des malades, des pauvres, des prisonniers, et baptisant des enfants et des grandes personnes.
>
> En d’autres endroits, je vis les DISCIPLES des Apôtres des derniers temps : je compris bien clairement que ces messieurs que j'appelle les DISCIPLES FAISAIENT PARTIE DE L'ORDRE. C'étaient des hommes libres : des jeunes gens qui, ne se sentant pas appelés au sacerdoce, voulant cependant embrasser la vie chrétienne, faire leur salut, accompagnaient les Pères dans quelques missions et travaillaient de tout leur pouvoir à leur propre sanctification et au salut des âmes. Ils étaient très saints et très zélés pour la gloire de Dieu. Ces disciples étaient auprès des malades qui ne voulaient pas se confesser ; auprès des pauvres, des blessées, des réunions, des prisonniers, des sectes, etc., etc. J'en vis même qui mangeaient et buvaient avec des impies et plusieurs de ceux qui ne voulaient pas entendre parler de Dieu ni des prêtres. Et voilà que ces Anges terrestres tâchaient,  par tous les moyens imaginables, de leur parler, et de les amener à Dieu, et de sauver ces pauvres âmes, qui ont chacune la valeur du sang de Jésus-Christ, fou d'amour pour nous. Cette vue était bien claire, bien précise, et ne me laissait aucun doute sur ce que je voyais ; et j’admirais la grandeur de Dieu, son amour pour les hommes, et les saintes industries dont il usait pour les sauver tous. Et je voyais que son amour ne peut pas être compris sur la terre, parce qu’il dépasse tout ce que les hommes les plus saints peuvent concevoir.
>
> Je vis donc que l’Évangile de Jésus-Christ était prêché dans toute sa pureté par toute la terre et à tous les peuples. 
>
> Je vis que les Religieuses étaient occupées à toutes sortes d’œuvres spirituelles et corporelles, et s’étendaient, comme les Missionnaires par toute la terre.
>
> Avec elles il y avait des femmes et des filles remplies de zèle, qui aidaient les Religieuses dans leurs œuvres. Ces veuves et ces filles étaient des PERSONNES qui, sans vouloir SE LIER PAR LES VŒUX DE RELIGION, désiraient servir le bon Dieu, vaquer à leur salut et mener UNE VIE RETIREE DU MONDE. Elles étaient vêtues de noir et étaient très simples. *[Mélanie portait leur costume. Dans son intérieur elle avait un petit bonnet.]* Elles portaient aussi une croix sur la poitrine, comme les Disciples, mais un peu moins grande que celle des Missionnaires, et elle n'était pas extérieure.
>
> Je vis et je compris que les Apôtres des derniers temps et les Religieuses faisaient les trois voeux de Religion. De plus, ils faisaient la promesse de se donner et de donner à la Très Sainte Vierge, pour les âmes du purgatoire, en faveur de la conversion des pêcheurs, toutes leurs prières, toutes leurs pénitences, en un mot, toutes leurs œuvres méritoires. Les Disciples et les femmes faisaient aussi cette promesse ou oblation à la Très Sainte Vierge.
>
> Je vis que les Missionnaires vivaient en communauté, et qu'ils disaient l'office divin ensemble, au chœur. Dans quelques maisons ils n'étaient pas nombreux. Je vis que les Disciples qui savaient lire disaient l'Office de la Sainte Vierge dans leur chapelle.
>
> Je vis aussi que les Religieuses, ainsi que les femmes, disaient l'Office de la Sainte Vierge.
>
> Je compris en Dieu, que les Apôtres des derniers temps devaient marcher sur les traces des apôtres de la primitive Église de Jésus-Christ, avec cette différence que le supérieur général avait soin de rappeler (quand cela se pouvait faire), à chaque année, les membres de l'Ordre dans la Maison centrale, pour faire une retraite de dix jours. Et je vis que, quand les membres de l'Ordre étaient très éloignés, cette retraite se faisait dans chaque maison ; ou bien ils se rendaient dans une des Maisons centrales de la Province. Ces retraites se faisaient dans le but de se retremper dans la ferveur, et dans l’observance de la règle.
>
> Je vis que les Supérieurs changeaient et envoyaient des sujets dans une des Maisons de l'Ordre, établie exprès pour les infirmes et pour les membres qui avaient perdu leur première ferveur par l'influence et la contagion des grands de la terre, et qui s'étaient rendus mous et avaient perdu la charité et le zèle. Les malades étaient bien soignés dans cette maison. *[Vu la manière de parler de Mélanie, les infirmes et malades dont il est question dans cet alinéa sont les infirmes et malades spirituels (Sous réserve).]*
>
> Je vis que notre doux Sauveur regardait des ouvriers de cet ordre avec beaucoup de complaisance, parce qu’ils servaient le Bon Dieu avec un entier et complet dévouement, sans recherche d’eux-mêmes. Étant entièrement détachés de toute les choses de la terre, ils étaient totalement entre les mains de la Providence de Dieu, remplis de foi et de confiance en Dieu.
>
> Je vis les âmes du purgatoire comme en fête, pour les bienfaits qu’elles recevaient des Apôtres des derniers temps et des Religieuses ; et je vis que les âmes souffrantes du Purgatoire, qui étaient délivrées ou qui avaient encore quelque chose à expier, et qui en avaient le pouvoir,  priaient beaucoup, et que de nombreuses conversions se faisaient par leurs prières. Car je vis que Dieu voulait que les Missionnaires et les Religieuses de cet Ordre missent toutes leurs prières, pénitences et bonnes œuvres entre les mains de Marie, leur première Supérieure et leur Maîtresse, pour les âmes du purgatoire, en faveur de la conversion des pécheurs du monde entier.
>
> Je vis et je compris que le bon Dieu voulait que cet Ordre luttât contre tous les abus qui ont amenés la décadence du Clergé et de l’état religieux et la ruine de la Société chrétienne.
>
> Beaucoup d'Ordres et de Congrégations religieuses rentraient dans leur première ferveur, par les soins et les exemples des Pères ; ou bien se fondaient dans l'Ordre de la Mère de Dieu.
>
> Je vis que cet Ordre ne recevait jamais, jamais pour Missionnaires ni pour Religieuses des personnes dont les parents avaient besoin de la charité d’autrui, ou besoin de ce fils et de cette fille pour les servir. Et quand les parents de quelqu'un des membres étaient tombés dans la misère, la Communauté, par amour pour le quatrième Commandement, par prudence, par charité, et pour la tranquillité de ses membres, dont les parents étaient affligés, donnaient abondamment, selon ses besoins, à cette famille. Et cela se faisait avec une grande charité, avec une grande joie, et reconnaissance envers Dieu, de ce qu’il donnait à la Communauté l’occasion de soulager les membres de Jésus, qui s’est donné à nous tout entier.
>
> Je vis que les membres de l'Ordre de la Mère de Dieu faisaient tous leurs efforts pour se dépouiller entièrement de l'esprit du siècle corrompu, s'avancer dans l'amour de Dieu et acquérir les vertus de Notre-Seigneur Jésus-Christ. Ils avaient de très bas sentiments d'eux-mêmes. Ils étaient très unis entre eux, n'ayant ni ambition, ni envie, ni jalousie, ne désirant en toute chose que de plaire à leur Divin Maître ; ne désirant rien hors du Cœur de Jésus, où ils habitaient plus ou moins étroitement, selon que leur amour était plus pur et plus généreux. Cet amour de Jésus produisait en eux les fruits d'une grande obéissance, d'une profonde humilité et simplicité, d'une très grande mortification, d'un zèle très ardent, et d'un parfait abandon entre les mains du Divin Maître.
>
> Je vis que cet Ordre était comme le foyer de toutes les œuvres, et comme un autel perpétuel où la prière était incessante pour les divers besoins de la Sainte Église, pour les âmes tièdes et pour la conversion des pécheurs du monde entier.
>
> <cite>Sœur Marie de la Croix, la pauvre Bergère de la Salette.<br/>23 novembre 1876, Castellamare di Stabia.<br/>Et 5 janvier 1879, Rome.<br/>(Archives de la S. Congrégation des Religieux).</cite>

Mélanie Calvat a revu ce texte à Diou, Allier, le 24 mai 1899, sans rien y changer ; elle a donné à l'abbé Combe quelques explications orales qui sont consignées dans les notes entre crochets, inscrites au fil du texte. Dans son Exhortation aux curés et prédicateurs de Rome, S.S. Pie XII demande de découvrir des collaborateurs pour aller là où le prêtre ne peut pénétrer. Osservatore Romano du 28-2-54. Un Ordre Religieux est tout indiqué pour former "intellectuellement et spirituellement" ces collaborateurs laïques. Les Disciples de l'Ordre de la Mère de Dieu sauront mieux que quiconque ouvrir les "portes des âmes fermées à toute intervention sacerdotale."

---

## La règle

Nous ne donnons ici que quelques articles de la Règle de l'Ordre de la Mère de Dieu. En effet, Mélanie Calvat ne souhaitait pas donner la Règle au grand public ni même à tous les prêtres ; elle disait que seul les prêtres appelés et vraiment sincères quant à suivre cette Règle devaient l'avoir, et ce pour éviter la multiplication des essais de fondations qui ne contribueraient qu'à ce qu'elles soient « selon la mode de chacun et sans l'unité et l'obéissance nécessaire. »


> Art. 3 – Les membres de l’ordre s’appliqueront à étudier Jésus-Christ et à l’imiter, et plus Jésus sera connu, plus ils s’humilieront à la vue de leur néant, de leur faiblesse, de leur incapacité à faire un bien réel dans les âmes sans la grâce divine.
> 
> Art. 11 – Il y aura dans le sanctuaire le saint sacrement exposé le jour et la nuit pendant les mois de septembre, février et mai, où les membres de l’ordre se feront un bonheur de passer d’heureuses heures quand la charité ou le salut des âmes ne les retiendront pas ailleurs.
>
> Art. 22 – Mes missionnaires seront les Apôtres des derniers temps, ils prêcheront l’Évangile de Jésus-Christ dans toute sa pureté par toute la terre.
>
> Art. 23 – Ils auront un zèle infatigable, ils prêcheront la réforme des cœurs, la pénitence et l’observation de la loi de Dieu ; ils prêcheront sur la nécessité de la prière, sur le mépris des choses de la terre, sur la mort, le jugement, le paradis et l’enfer, sur la vie, la mort et la résurrection de Jésus-Christ. Ils fortifieront les hommes dans la foi afin que quand le démon viendra, un grand nombre ne soit pas trompé.
>
> Art. 31 – Chaque fois que les sujets se rencontreront, l’un dira : « que Jésus soit aimé de tous les cœurs ! », l’autre répondra : « ainsi soit-il. »
>
> Art. 33 – Tous les membres porteront une croix comme la mienne.
> *[C'est Notre-Dame qui donne cette Règle, cette croix est donc celle qu'Elle porte lors de l’apparition, avec le marteau à sa gauche et les tenailles à sa droite.]*

Au delà de ces articles directement donnés par la Sainte Vierge, les constitutions ont été rédigées par Mélanie lors de son séjour chez les Visitandines de Rome, sous le pontificat de Léon XIII. Selon toute vraisemblance, ces constitutions sont dans les archives Vaticanes.

---

## Avec Saint Louis-Marie Grignon de Monfort


Mélanie Calvat, à qui la Règle de l'Ordre de la Mère de Dieu a été donné par la Très Sainte Vierge et qui l'a ecrite par ordre du Pape en 1879, mentionne ceci dans une lettre au Chanoine de Brandt le 10 octobre 1888:

> Le Bienheureux Grignon de Montfort a vu de loin l'Ordre de la Mère de Dieu pour les Apôtres des derniers temps. Après la Très Sainte Vierge c'est notre protecteur, et nous devons imiter sa grande dévotion envers notre tendre Mère, sa donation corps et âme à <span style="font-variant: small-caps;">Marie</span>, vivant de l'esprit de <span style="font-variant: small-caps;">Marie</span>, nous laissant conduire par <span style="font-variant: small-caps;">Marie</span> ! - Et où nous mènera t-elle ? Là où Elle est, et nous serons bien.

Et cela dans une autre lettre à ce même Chanoine le 26 octobre de la même année :

> Aimons, imitons notre douce Mère, comme l'aima le Bienheureux Grignon de Montfort. Vous avez bien raison, mon très Révérend Père, de vous donner tout, corps et âme, à notre tendre Mère Marie. Jésus ne se donna-t-il pas tout entier à Marie, d'abord pendant neuf mois, faisant le muet, l'aveugle et le mort même ; et hors de sa prison amoureuse, puisqu'elle était toute formée d'amour, ne dépendait-il pas entièrement de sa divine Mère ?... C'est ainsi que nous devons être ; ne craignons jamais d'aimer trop Marie ; non, non, allons avec confiance à Marie, Elle sera notre lumière au milieu des ténèbres, notre compagne dans notre isolement des créatures, notre vraie consolatrice dans nos tribulations ; Marie nous sera tout. - Eh ! Qui peut dire les grandeurs de Marie ?... Qui peut même penser à sa haute dignité, à ses sublimes privilèges ? Marie surpasse la beauté des Anges, des Saints sur tous points ; Marie veut être aimée d'une manière toute spéciale par ses enfants, ses Apôtres. Le Bienheureux Grignon de Montfort dit que, vers la fin du monde, Marie se manifestera davantage. Cela ne veut pas dire qu'Elle se montrera visible, mais qu'Elle se manifestera dans ses Apôtres, par les vertus qu'en son honneur ils pratiqueront. Elle se formera ses saints, ses vrais enfants : ce sera le règne de Marie sur la terre. Et quel feu d'amour les embrasera ; quel zèle pour la gloire de Dieu ! Quelle vie simple, humble,... Je voudrais bien voir ce règne pacifique, mais terrible au démon.

Ce qui suit est tiré directement du "Traité de la vraie dévotion à la Sainte Vierge" écrit par Saint Louis Marie Grignon de Montfort.
On y trouve une description des Apôtres des derniers temps ; Apôtres qui, nous sommes pérsuadés, observeront la Règle de l'Ordre de la Mère de Dieu donnée par la Très Sainte Vierge Marie à Mélanie Calvat le 19 septembre 1846 :


> <p style="font-variant: small-caps;">2. La dévotion à Marie nécessaire particulièrement dans les derniers temps
>
> <p>55. Enfin Dieu veut que sa sainte Mère soit à présent plus connue, plus aimée, plus honorée que jamais elle n'a été: ce qui arrivera sans doute, si les prédestinés entrent, avec la grâce et lumière du Saint-Esprit, dans la pratique intérieure et parfaite qu je leur découvrirai dans la suite. Pour lors, ils verront clairement, autant que la foi le permet, cette belle étoile de la mer, et ils arriveront à bon port, malgré les tempêtes et les pirates, en suivant sa conduite; ils connaîtront les grandeurs de cette souveraine, et ils se consacreront entièrement à son service,comme ses sujets et ses esclaves d'amour; ils éprouveront ses douceurs et ses bontés maternelles, et ils l'aimeront tendrement comme ses enfants bien-aimés; ils connaîtront les miséricordes dont elle est pleine et les besoins où ils sont de son secours, et ils auront recours à elle en toutes choses comme à leur chère avocate et médiatrice auprès de Jésus-Christ; ils sauront qu'elle est le moyen le plus assuré, le plus aisé, le plus court et le plus parfait pour aller à Jésus-Christ, et ils se livreront à elle corps et âme, sans partage, pour être à Jésus-Christ de même.
>
> <p>56. Mais qui seront ces serviteurs, esclaves et enfants de Marie? Ce seront un feu brûlant, ministres du Seigneur qui mettront le feu de l'amour divin partout. Ce seront sicut sagittae in manu potentis, des flèches aiguës dans la main de la puissante Marie pour percer ses ennemis.
>
> Ce seront des enfants de Lévi, bien purifiés par le feu de grandes tribulations et bien collés à Dieu, qui porteront l'or de l'amour divin dans le coeur, l'encens de l'oraison dans l'esprit et la myrrhe de la mortification dans le corps, et qui seront partout la bonne odeur de Jésus-Christ aux pauvres et aux petits, tandis qu'ils seront une odeur de mort aux grands, aux riches et  orgueilleux mondains.
>
> <p>57. Ce seront des nues tonnantes et volantes par les airs au moindre souffle du Saint-Esprit, qui, sans s'attacher à rien, ni s'étonner de rien, ni se mettre en peine de rien, répandront la pluie de la parole de Dieu et de la vie éternelle; ils tonneront contre le péché, ils gronderont contre le monde, ils frapperont le diable et ses suppôts, et ils perceront d'outre en outre, pour la vie ou pour la mort, avec leur glaive à deux tranchants de la parole de Dieu, tous ceux auxquels ils seront envoyés de la part du Très-Haut.
>
> <p>58. Ce seront des apôtres véritables des derniers temps, à qui le Seigneur des vertus donnera la parole et la force pour opérer des merveilles et remporter des dépouilles glorieuses sur ses ennemis; ils dormiront sans or ni argent et, qui plus est, sans soin, au milieu des autres prêtres, et ecclésiastiques et clercs, inter medios cleros; et cependant auront les ailes argentées de la colombe, pour aller avec la pure intention de la gloire de Dieu et du salut des âmes, où le Saint-Esprit les appellera, et ils ne laisseront après eux, dans les lieux où ils auront prêché, que l'or de la charité qui est l'accomplissement de toute la loi.
>
> <p>59. Enfin, nous savons que ce seront de vrais disciples de Jésus-Christ, qui marchant sur les traces de sa pauvreté, humilité, mépris du monde et charité, enseignant la voie étroite de Dieu dans la pure vérité, selon le saint Evangile, et non selon les maximes du monde, sans se mettre en peine ni faire acception de personne, sans épargner, écouter ni craindre aucun mortel, quelque puissant qu'ils soit. Ils auront dans leur bouche le glaive à deux tranchants de la parole de Dieu; ils porteront sur leurs épaules l'étendard ensanglanté de la Croix, le crucifix dans la main droite, le chapelet dans la gauche, les sacrés noms de Jésus et de Marie sur leur coeur, et la modestie et mortification de Jésus-Christ dans toute leur conduite. Voilà de grands hommes qui viendront, mais que Marie fera par ordre du Très-Haut, pour étendre son empire sur celui des impies, idolâtres et mahométans. Mais quand et comment cela sera-t-il?... Dieu seul le sait: c'est à nous de nous taire, de prier, soupirer et attendre: Expectans expectavi. 
