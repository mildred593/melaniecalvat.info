---
title: Sa vision béatifique
draft: true
---

Elle voyait en permanence Dieu. Un Saint chanoine à Castellamare l'avait
probablement décelé avant que le bon abbé Combe ne découvre ce don chez Mélanie:

> Quand elle habitait à Castellamare, il y avait (j'ai compris à Naples) un
> saint chanoine qui ne pouvait dire sa messe en public, parce qu'il dansait
> devant l'autel comme David devant l'arche ! La première fois qu'il visita la
> petite communauté, il s'interrompit au milieu de sa conférence spirituelle
> pour demander vivement : « Qui est celle-là ? » il l'indiquait du doigt.
> Personne ne répondit. Elle faisait signe de ne pas répondre. Il insista « Qui
> êtes-vous ? » Elle se contenta de sourire à ce saint homme. Il feignit de
> s'impatienter en répétant : « Dites-moi qui vous êtes ? » Nouveau sourire pour
> toute réponse. Alors il lui lança son mouchoir vivement : « Couvrez votre
> visage, vous m'éblouissez ! » Gaiement elle mit le mouchoir dans sa poche en
> disant « Vous ne l'aurez plus. »
>
> Dans une autre visite il lui dit qu'il voulait la conduire à Marie Louise,
> pour voir deux soleils se précipiter l'un dans l'autre. mais il n'eut pas le
> temps car Marie Louise mourut peu après. La vénérable Marie Louise Ascione,
> née à Barra, diocèse de Naples, le 28 février 1799, est morte le 10 janvier
> 1875. sa cause fut introduite le 23 janvier 1885. Mélanie me parla souvent de
> cette servante de Dieu. En prononçant son nom, son visage s'illuminait. Elle
> souriait comme si elle l'eut vue auprès d'elle.
>
> <cite>[Journal de l'abbé Combe]({{< relref "/livres/dernières-années-de-sœur-marie-de-la-croix-bergère-de-la-salette" >}}), Mercredi 12 novembre 1902, p. 154</cite>

<!--more-->

TODO: citations

- découverte de ce don par l'abbé Combe

