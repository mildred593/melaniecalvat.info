---
menu: true
menutitle: Mélanie
title: Histoire de Mélanie
date: 2022-12-14T19:17:49.116+01:00
showindexlist: false
showinsubmenu: false
weight: 1
related:
  - /bibliotheque/lenfance-de-mélanie-écrite-par-elle-même
  - /bibliotheque/dernières-années-de-sœur-marie-de-la-croix-bergère-de-la-salette
---

Mélanie Calvat est née à Corps le 7 novembre 1831. Ses parents étaient très pauvres, probablement même les plus pauvres de ce village du Dauphiné situé à 1000m d'altitude.
C'était une famille nombreuse de dix enfants dont deux décédèrent en bas âge, Mélanie se trouvait être la troisième. Le père, scieur de long, partait travailler loin de la maison, là où il trouvait du travail, parfois à plusieurs jours de marche, et de ce fait ne revenait que de temps en temps à la maison familiale qui était une location.

Mélanie était d'un naturel plutôt taciturne et renfermé; elle ne parlait presque pas et était, depuis toute petite, très impressionnée par ce "Jésus en Croix" que lui montrait son papa lorsqu'il la prenait sur ses genou. Tout cela ne plaisait pas à sa mère qui, elle, aimait aller au divertissements et spectacles publics du village ; elle voulait emmener Mélanie à ces fêtes mais celle-ci n'y consentait jamais, pleurant, hurlant, se débattant et disant qu'elle voulait être silencieuse et imiter "Jésus qui avait donné sa vie pour nous" et qui, cloué au crucifix, "ne parlait pas".
Sa mère prit donc en quelque sorte, Mélanie en grippe. Lorsque le père de famille n'était pas là, la mère brimait Mélanie, disant à ses frères et sœurs de la rejeter, la chassant de la maison jusque pendant plusieurs jours etc...

C'est dans ces très tristes et rudes conditions que Mélanie passa les premières années de sa vie.
Puis, à partir de l'âge de six ans, sa mère l'envoya chaque année, du printemps à la fin de l'automne, en service dans diverses familles de fermiers de la vallée. Le travail était dur, les maîtres pas toujours bienveillants et Mélanie gardait le bétail et les enfants de fermiers et faisait toutes sortes de tâches domestiques fatigantes, dans les montagnes et loin de chez elle.

C'est alors qu'étant en service pas très loin de chez elle au village des Ablandins en 1846, elle fut témoin d'une Apparition de la Très Sainte Vierge Marie, un samedi 19 septembre en milieu d'après-midi.
Elle était ce jour là avec un autre enfant, Maximin Giraud, 11 ans, lui aussi originaire du village de Corps.
Cette apparition bouleversa la vie des deux enfants. Une enquête minutieuse de cinq années dirigée par l’Évêque de Grenoble, Monseigneur Bruillard, arriva à la conclusion que l'apparition était bien d'origine surnaturelle et que c'est la Vierge Marie qui était apparue aux deux enfants le 19 septembre 1846 sur la commune de La Salette à 1800 m d'altitude.

Lors de cette apparition, hormis un message donné aux deux enfants, la Vierge Marie donna à Mélanie un secret à ne révéler qu'après 1858 ainsi que la Règle d'un nouvel ordre religieux.
Ce rôle de messagère que lui avait donné la Sainte Vierge lui fut une croix durant toute sa vie. En effet, dès la reconnaissance de l'apparition par l’Église et la connaissance qu'il y avait un secret, le pouvoir politique en place et une partie du clergé français firent tout ce qu'ils purent pour faire taire Mélanie Calvat, la persécuter et la calomnier. Ce secret, bientôt révélé, s'avéra être, entre autre chose, une condamnation sans équivoque des mauvais membres du clergé ainsi que des politiciens corrompus et tournant le dos à Dieu. De plus, le long secret révélait les maux à venir avec beaucoup de prédictions diverses et se terminant à la mort de l'antéchrist ; inutile de dire à quel point les démons, les politiciens et membres du clergé attachés de prêt ou de loin à la franc-maçonnerie se déchaînèrent (et se déchaînent toujours) contre : Mélanie Calvat et son Secret ainsi que l'apparition de La Salette et le remède donné par la Sainte Vierge en la règle d'un  nouvel ordre religieux.

La pauvre bergère, poursuivie et calomniée en France trouva refuge en Italie où sa Sainteté le Pape Pie IX lui donna un directeur spirituel en la personne de Monseigneur Petagna, Évêque de Castellamare.
La conscience de Mélanie Calvat ne lui laissait cependant aucun repos puisque la Sainte Vierge lui avait bien dit de faire passer son message « à tout son peuple ». Mélanie écrivit donc tout un récit de l'apparition ainsi que son secret, trouva l'appui et l'imprimatur d’Évêques Italiens et fut ensuite soutenu par le Pape Léon XIII qui la garda à Rome pour qu'elle écrive la règle et les constitutions du nouvel ordre religieux appelé « Ordre de la Mère de Dieu » et qui soutint Mélanie dans la publication de son récit de l'apparition.
Malgré cela, une grande partie du clergé français appuyée par les politiciens franc-maçon fit constamment la guerre au secret donné par la Sainte Vierge et désobéirent au Pape en refusant de commencer le nouvel ordre religieux sur la montagne de La Salette.

Mélanie Calvat revint cependant en France à plusieurs reprises pour s'occuper de sa mère malade et travailler de diverses manières à la diffusion de son secret et de la vérité ainsi qu'à des tentatives pour commencer l' « Ordre de la Mère de Dieu ». Elle était, à ces occasions, plus ou moins obligée de se cacher pour ne pas être inquiétée, ne révélant pas son identité, ses courriers étaient suivis et ouverts, on cherchait à attenter à sa vie, on l’excommuniât dans un diocèse etc …. Bref, la vie de la bergère de La Salette fut un long calvaire.

Tout à la fin de sa vie, par volonté Divine, Mélanie Calvat vînt s'installer dans l'Allier auprès d'un curé français, l'abbé Combe. Là, ce curé, devenu son directeur spirituel, lui ordonna d'écrire le récit de sa petite enfance et pût observer la bergère dans sa vie quotidienne.
Le récit que fît Mélanie de sa petite enfance se trouva être complètement bouleversant. 	On y apprend que Mélanie Calvat eu, avant l'apparition de La Salette, une vie remplie d’événements surnaturelles,  que sa petite enfance si difficile fût jonchée de rencontre avec Notre Seigneur qui se présentait à elle sous les traits d'un petit enfant de sa taille et de son âge et qui l'instruisait sur tous les mystères de la religion catholique. On découvrit donc, par cet écrit, que Mélanie Calvat fût comme préparée à une vie crucifiée très spéciale en tant que messagère du Ciel.

L'abbé Combe relate aussi dans un de ses livres certaines particularités pour le moins extraordinaires de la petite bergère à savoir, par exemple, qu'elle avait les stigmates de la passion depuis un très jeune âge, qu'elle conversait régulièrement et familièrement avec Notre Seigneur et Notre-Dame, qu'elle voyait les anges gardiens, qu'elle avait parfois la vue à distance de certains événements etc.

Après ses cinq années passées dans l'Allier, Mélanie Calvat retourna en Italie où elle mourut le 14 décembre 1904. On retrouva son corps seulement le 15 décembre au matin après avoir enfoncé sa porte (ce qu'elle avait prédit) mais la veille au soir, après l'heure de l’Angélus, des voisins entendirent « comme des chants célestes et merveilleux qui provenait de son logement ». Tout porte à croire, pour ceux qui ont un tant soit peu étudié sa vie, qu'une cour céleste est venue la chercher ….. nous ne savons pas Qui en faisait partie.

---

