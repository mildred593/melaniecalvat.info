---
title: Sa Vue
draft: true
---

Mélanie était dotée par la Divine Providence d'une vue sur ce qui se passait ou
allait se passer. Elle savait par une vision directe ce qu'il en était de
certaines choses par l'autorisation toute spéciale de Dieu.
