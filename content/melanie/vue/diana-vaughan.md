---
title: Diana Vaughan
draft: true
---

> « — Mais pouvez-vous dire qu’on a changé Léo Taxil ? Pouvez-vous dire que les méchancetés des catholiques sont cause qu’il est retourné à la Franc-maçonnerie ? Comment ne voyez-vous pas qu’il s’est toujours moqué des catholiques ? Lui-même l’a dit en 1897, quand on le somma de produire sa Diana Vaughan, il leur a ri au nez. \
> — Diana Vaughan, mon Père, n’est pas un mythe. La courageuse femme qui avait confiance en lui, ne sachant pas qu’il était redevenu mauvais, se rendit réellement à Paris, et il la livra. \
> — Qu’est-ce que vous me racontez ! Vous l’avez vu la livrer ? \
> — Oui, mon Père. La nuit il est allé la chercher à la gare ; en route il lui dit : « J’ai des précautions à vous indiquer, entrons dans cette maison. »
> Quand elle mit le pied dans la première chambre à gauche, elle tomba dans une
> trappe. \
> — Alors, c’est plus qu’une fripouille, c’est un assassin ! \
> — Il ne l’a pas assassinée. Il fut payé pour la livrer, et on lui avait dit qu’on se contenterait de l’emprisonner. \
> — S’est-on borné à la séquestrer ? \
> — Les palladistes l’ont fait souffrir, ô ! Combien, mais celle-là n’apostasiera pas ! \
> — Vous avez vu tout cela ? \
> — Je l’ai vu se faire. »
>
> <cite>Mémoires de l'abbé Combe (p. 178, jeudi 25 juin 1903)</cite>
