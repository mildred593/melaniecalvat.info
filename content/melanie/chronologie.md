---
title: Chronologie
draft: true
---

### Sa naissance

Françoise Mélanie est née à Corps le 7 novembre 1831 de son père Pierre Calvat
(dit Matthieu) et de sa mère Julie, née Barnaud, native de Séchilienne. Ses
parents étaient très pauvres et son père devait travailler loin pour nourrir sa
famille. Mélanie ne le voyait pas souvent.

Elle était la quatrième d'une fraterie dix enfants, venue après les deux
premiers garçons et la fille aînée étant morte enfant, elle avait tout pour
faire la joie de sa mère qui désirait fort une fille pour partager avec elle ses
préoccupations.

### Son enfance

Elle ne voyait pas souvent son père, mais c'est lui qui lui témoignait le plus
d'affection. Pieux, il lui a facilement appris a prier et elle aimait à entendre
les histoires de Jésus crucifié. Sa mère aimait les occupations du monde et
cherchait a partager avec sa fille les spectacles mondains que Mélanie ne
supportait pas. Ce fut pour sa mère une grande souffrance et elle l'a répercuté
sur sa fille.

Nous aurions tant à dire sur son enfance merveilleuse mais nous devons laisser
la parole à Mélanie pour l'expliquer plus convenablement. Cette histoire se base
sur [son enfance écrite par elle-même]({{< relref "/livres/lenfance-de-mélanie-écrite-par-elle-même" >}}) sur l'ordre de l'abbé Combe.

Pour résumer, Dieu s'arrangea pour qu'elle soit méprisée pendant toute son
enfance. Elle avait l'interdiction de parler à la maison, lorsque son père
né´tait pas présent, et bien des fois elle fut chassée de la maison des jours
durant. Elle se réfugiait dans les bois et c'est là que le bon Dieu put faire
son éducation tout en préservant son innocence qu'elle garda jusqu'à sa mort.

En grandissant et dés qu'elle le put, elle si mit a travailler chez différents
patrons pour la saison entière. Elle passait des mois entiers sans voir sa
famille en compagnie du Bon Dieu et des animaux sauvages. C'est donc une âme
toute préparée par Notre Seigneur qui va recevoir la visite de sa mère sur la
montagne de la Salette le 18 septembre 1846. Elle avait 14 ans.

### Son éducation une fois le secret révélé

Le journal de l'abbé Combe permet de bien mieux nous éclairer sur la suite de sa
vie. L'apparition révélée, elle passa chez les sœurs de la Providence à Corps,
puis à Corenc, près de Grenoble.

> Après l'apparition, elle continua de garder ses troupeaux environ deux mois.
>
> Quinze jours avant Noël 1846, elle fut mise en pension à Corps chez les sœurs
> de la Providence. Elle y demeura quatre ans avec lesquelles elle pensa à se
> faire religieuse. Monseigneur de Bruillard l'envoya à Corenc. Au moment de
> faire ses vœux, Monseigneur Ginoulhiac, successeur de Monseigneur de
> Bruillard, y mit opposition parce qu'il prétendit qu'elle avait dit tout haut
> à la chapelle qu'il était fou.
>
> « Est-ce vrai que vous avez dit cela a votre évêque ? \
> — Mais non, mon Père. Un jour que Notre Seigneur me témoignait son amour, je
> m'écriai, sans penser qu'on m'entendait : “Seigneur, vous êtes fou, fou,
> fou !” Monseigneur Ginoulhiac a cru que j'avais dit ça de lui. »
>
> <cite>[Journal de l'abbé Combe]({{< relref "/livres/dernières-années-de-sœur-marie-de-la-croix-bergère-de-la-salette" >}}), Mercredi 12 novembre 1902, p. 152</cite>

### Au Carmel de Darlington

Suite à ses différents avec le successeur de Monseigneur de Bruillard, elle fut
envoyée le 20 septembre 1854 à Darlington, cloitrée dans un Carmel, probablement
afin de l'éloigner de la France et éviter la propagation de l'apparition déjà
gênante pour les prélats peu religieux. En effet, avec Napoléon et le concordat,
en France, les évêques sont nommés par le pouvoir politique. Nous comprenons
alors mieux les reproches faits par la Sainte Vierge aux ministres de son fils.

Au Carmel elle prit l'habit et les vœux, mais s'est refusé dans son cœur à
prendre le vœu de clôture, contraire avec la mission que lui avait donné la
Sainte Vierge de faire passer son message à tout son peuple.

En 1860 était venu le moment de révéler son secret. Toujours cloitrée, la
Providence trouva un moyen de la faire sortir de son couvent. Monseigneur
Ginoulhiac ne la voulant pas à Grenoble, elle parti pour Marseille le 19
septembre avec la bénédiction de l'évêque de Darlington en compagnie de
l'aumônier du couvent, une sœur professe et une novice.

<!-- TODO: citation nécessaire sur comment elle est sorti du couvent et a été
levée du vœu de clôture -->

### À Marseille

TODO

### À Castellamare

TODO: Renvoyée à Grenoble, elle fut retirée au monastère de la Visitation à Voiron le
temps des négociations entre évêques afin de la faire rejoindre Castellamare ou
Monseigneur Petagna l'attendait

TODO: suite...

