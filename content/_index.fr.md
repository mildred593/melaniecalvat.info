---
title: ''
cmstitleprefix: Page principale
---

L'apparition de Notre-Dame dite "de la Salette" à eu lieu dans les montagnes du Dauphiné à 1800m, sur la commune de la Salette dans le diocèse de Grenoble le 19 septembre 1846.

Deux enfants pauvres, Mélanie Calvat et Maximin Giraud, employés à ce moment chez des fermiers, ont été les témoins privilégiés de cette apparition.

Cette apparition, reconnue par l’Évêque du lieu en 1851 avec l'accord implicite du Pape Pie IX, est relativement méconnue eu égard à l'importance du message qu'y a donnée la Très Sainte Vierge Marie. Ce message se divise en trois parties distinctes:

1. **Ce qu'on appelle le "message public"** c'est à dire le message que la Vierge Marie a donnée aux deux enfants.

2. **Les "Secrets" –** il s'agit de deux secrets que la Vierge Marie a donné de manière séparée à l'un et à l'autre des enfants&nbsp;:

    Pour celui donné à Mélanie, Marie lui a dit de le rendre public à partir de 1858 soit 12 années après l'apparition&nbsp;; ce qu'elle fit par "petites touches" à partir de 1860 et enfin complètement par une publication de 1879 avec imprimatur d'un Évêque italien et l'accord implicite du Pape de l'époque&nbsp;: Léon XIII.

    Quant au secret de Maximin, il avait ordre par Marie de ne pas le révéler. Il obéît donc, et malgré les tentations multiples, il ne publia jamais ce secret. Il est à noter qu’une version écrite de ce secret circule ; qu’elle soit authentique ou pas, c’est contrevenir à la volonté de la Très Sainte Vierge Marie ainsi qu’à celle de Maximin que de chercher à la diffuser.

    Dans le même registre, Mélanie disait à ceux qui voulaient connaître à tout prix le contenu de la version du secret écrite pour le Pape Pie IX de ne pas avoir cette curiosité malsaine et que cette version ne les regardait pas ! On ne l’a pas écouté et ceux qui ont cherché ce secret de 1851 en ont été pour leurs frais puisque la providence à fait qu’ils ont un «&nbsp;FAUX SECRET&nbsp;» trouvé en 1999 dans les archives du Vatican. Faux secret qui a semé et continue de semer la zizanie autour du sujet et à discrédité le vrai message de Notre-Dame.

3. **La Règle d'un nouvel Ordre Religieux** dit "Ordre de la Mère de Dieu". En
   reçevant cette règle, elle avait en vision les apôtres des derniers temps en
   action, ce qu'elle appela «&nbsp;la Vue&nbsp;». Mélanie fut installée en 1878
   chez les Visitandines de Rome par le pape Léon XIII pour qu'elle rédige les
   constitutions. Elle y transcrira également la règle reçue directement de la
   Sainte Vierge et donna une version définitive de la Vue.

A la suite de l'apparition et selon le commandement de Notre-Dame, ces enfants vont "faire passer" le message public autour d'eux. Rapidement des milliers puis des millions de gens en France comme à l’étranger vont invoquer "Notre- Dame de la Salette" ainsi que venir sur les lieux de l'apparition y prier la Mère de Dieu. Il aura un grand engouement ainsi que des conversions et de nombreux miracles notamment venant de la source qui se mit à couler après l'apparition.

La vie des deux voyants va cependant être tumultueuse. Paradoxalement, on voudra forcer Maximin à dévoiler son secret et d'un autre côté on fera tout pour faire taire Mélanie, la discréditer et l'affubler de toutes sortes de vices et de défauts pour ainsi "faire taire la Très Sainte Vierge Marie" car certaines parties de son message dérangeaient et contrecarraient les plans des gens de pouvoir (et, il faut bien le dire, du Démon lui même).

En outre, à la suite d'un récit que Mélanie Calvat écrivit, par obéissance à son confesseur, à la fin de sa vie; on découvrit que Mélanie avait eu avant l'apparition de 1846 une enfance extrêmement pauvre et difficile à tous points de vue, mais aussi mystique et aussi proche de notre Seigneur que les plus grands saints de l'histoire de l’Église.

Ce sont tous ces différents sujets que nous avons voulus aborder à notre manière sur ce modeste site électronique, et ce, afin de contribuer à rétablir la vérité sur l'apparition de Notre-Dame à la Salette.

<span style="font-variant: small-caps;">Pour l'honneur de Notre-Dame !</span>
