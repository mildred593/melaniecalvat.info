Static Website
==============

Documentation
-------------

- Hugo: https://gohugo.io/documentation/
- Static CMS: https://staticjscms.netlify.app/docs/intro

Preview locally (with HCMS)
---------------------------

*Note: With HCMS, the stylesheet oad order is different and some styles may be
different*

```
npm run dev
```

If you hwant to have the content rebuilt automatically, you can run in parallel
`hugo --watch`

Preview locally (without HCMS)
---------------

Start Hugo webserver and access the website at http://localhost:1313

```
hugo serve
```


To use Static CMS Locally, also start the proxy server:

```
npm exec @staticcms/proxy-server
```

Access the Static CMS at http://localhost:1313/admin/

Tips
----

### Obfuscate e-mail address

Use simply:
```
{{< cloakemail "mailbox@example.org" >}}
```

Or if you prefer things complex:

```
<<<"mailbox@example.org" grep -o . | (printf "mailto:"; while read  s; do printf '%%%x' "'$s" ; done) | grep -o . | while read s; do printf '&#%d;' "'$s"; done; echo
```
