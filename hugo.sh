#!/bin/bash

if ! [[ -e hugo ]]; then
  HUGO_VERSION=0.127.0
  if ! [[ -e hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz ]]; then
    wget https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz
  fi
  tar zxvf hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz hugo
fi

exec ./hugo "$@"
