import { matter } from './matter.js';
import { marked } from 'marked';
import DOMPurify from 'dompurify';
import { saved_sources, page_content, combine, options } from './stores.js';
import { saved_source_store } from './sources.js';

// TODO: subscribe to DOM changes
page_content.update(content => {
  content.length = 0
  for(let node of document.querySelectorAll('[data-hcms-source]')) {
    content.push({
      node,
      source_path: node.dataset.hcmsSource
    })
  }
  return content
})

function restore_original(node) {
  if (! node.hcms_original) return

  for(const k in node.hcms_original) {
    node[k] = node.hcms_original[k]
  }

  node.hcms_original = null
}

function preview_item(opts, { source_path, node }){
  const unsubscribe = saved_source_store(source_path).subscribe(source => {
    if (!source) return;

    const { content, data, excerpt } = matter(source.data, {excerpt: true, excerpt_delimiter: '<!--more-->'})

    for(const node_content of node.querySelectorAll('[data-hcms-content]')) {
      if (!node_content.dataset.hcmsContent) continue

      if (!opts.preview) {
        restore_original(node_content)
        continue
      }

      const ask_summary = (node_content.dataset.hcmsContent == 'summary')
      const html = marked.parse(ask_summary ? excerpt : content)
      const cleaned = DOMPurify.sanitize(html)

      node_content.hcms_original ||= { innerHTML: node_content.innerHTML }
      node_content.innerHTML = cleaned
    }

    for(const node_param of node.querySelectorAll('[data-hcms-param]')) {
      if (!opts.preview) {
        restore_original(node_param)
        continue
      }

      node_param.hcms_original ||= { textContent: node_param.textContent }
      node_param.textContent = data[node_param.dataset.hcmsParam]
    }
  })

  return unsubscribe;
}

export function global_preview(){
  const cleanup = []
  const global_unsubscribe = combine(options, page_content).subscribe(([opts, contents]) => {
    for(const clean of cleanup) clean()
    cleanup.length = 0

    for(const content of contents) {
      const unsubscribe = preview_item(opts, content)
      cleanup.push(unsubscribe)
    }
  })

  return () => {
    for(const clean of cleanup) clean();
    cleanup.length = 0;
    global_unsubscribe();
  }
}

