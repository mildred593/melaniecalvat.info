import { writable, derived, get } from 'svelte/store';

export const saved_sources = writable({});


export const ui = writable({})

//
// Options
//


export const options = writable({
  preview: false,
  enabled: !! sessionStorage.getItem('hcms.enabled'),
  ...JSON.parse(sessionStorage.getItem('hcms.options') || '{}')
});

options.subscribe(opts => {
  sessionStorage.setItem('hcms.options', JSON.stringify(opts))
  if (opts.enabled) {
    sessionStorage.setItem('hcms.enabled', true)
  } else {
    sessionStorage.removeItem('hcms.enabled')
  }
})

export const page_content = writable([]);

// No need, we have a synchroneous function for that
export function storeValue(store) {
  return new Promise((accept, reject) => {
    store.subscribe(value => accept(value))()
  })
}

export { get, derived }

export function combine(stores) {
  if (stores.subscribe) {
    stores = Array.from(arguments)
  }

  if (stores instanceof Array) {
    return derived(stores, (x) => x)
  } else {
    const names = []
    const store_arr = []
    for(const s of Object.keys(stores)) {
      names.push(s)
      store_arr.push(stores[s])
    }
    return derived(store_arr, (args) => {
      const res = {}
      for(const i = 0; i < names.length; i++) {
        res[names[i]] = args[i]
      }
      return res
    })
  }
}
