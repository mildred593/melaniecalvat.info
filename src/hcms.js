import './style.css'
import 'purecss'
import App from './App.svelte'
import { load_saved_sources, continuously_save_sources } from './sources.js';
import { global_preview } from './global_previewer.js'
import { options, get, derived } from './stores.js'

import { Buffer } from './polyfills/Buffer.js'
window.global = window.global || window
window.Buffer = window.Buffer || Buffer

const app_div = document.createElement('div')
app_div.classList.add('hcms-interface')

derived(options, o => o.enabled).subscribe(enabled => {
  document.documentElement.classList.toggle('hcms-enabled', enabled)
  document.documentElement.classList.toggle('hcms', enabled)
  if (enabled) {
    console.log('[HCMS] enabled')
    document.body.append(app_div)
  } else {
    console.log('[HCMS] disabled')
    app_div.remove()
  }
})

function hcms_toggle(enable){
  options.update(opts => ({
    ...opts,
    enabled: (enable == null) ?
      ! opts.enabled :
      enable
  }))
}

load_saved_sources()
continuously_save_sources()
global_preview()

const app = new App({
  target: app_div,
})

window.hcms_toggle = hcms_toggle

export default app
