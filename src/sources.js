import { saved_sources } from './stores.js'
import { derived, get } from 'svelte/store';

export const hashAlgorithm = 'SHA-256';

export async function digest(content) {
  const encoder = new TextEncoder();
  const data = encoder.encode(content);
  const hash = await crypto.subtle.digest(hashAlgorithm, data);
  const hashArray = Array.from(new Uint8Array(hash));
  return hashArray.map((b) => b.toString(16).padStart(2, '0')).join('');
}

export async function get_manifest(){
  const resp = await fetch('/src/MANIFEST.git')
  const body = await resp.text()
  const res = {}
  for(const line of body.split("\n")) {
    if(!res['.git/HEAD']) {
      res['.git/HEAD'] = line.trim()
    } else if (line != '') {
      const [ _, hash, path ] = line.match(/^([^ ]*) (.*)$/)
      res[path] = hash
    }
  }
  return [res['.git/HEAD'], res]
}


export function saved_source_store(path) {
  return derived(saved_sources, sources => {
    if (! sources[path]) {
      return null
    } else {
      const { upstream, data } = sources[path]
      return { upstream, data }
    }
  })
}

export function get_saved_source(path) {
  const sources = get(saved_sources)
  
  if (! sources[path]) {
    return null
  } else {
    const { upstream, data } = sources[path]
    return { upstream, data }
  }
}

export async function save_source(path, upstream, data, cb) {
  saved_sources.update(sources => {
    sources[path] = {
      upstream,
      data,
      cb_saved: [cb]
    }
    return sources;
  })
}

export function load_saved_sources(){
  saved_sources.update(sources => {
    for(const key of Object.keys(localStorage)) {
      if (key.startsWith('hcms.source.data:')) {
        const path = key.match(/^[^:]*:(.*)$/)[1];
        const data = localStorage.getItem(key)
        const upstream = JSON.parse(localStorage.getItem(`hcms.source.upstream:${path}`) || '{}') || {}
        sources[path] = { upstream, data }
      }
    }
    return sources
  })
}

export function continuously_save_sources(){
  let prev_sources = {}
  saved_sources.subscribe(sources => {
    for(const path of Object.keys(prev_sources)) {
      if (!sources[path]) {
        localStorage.removeItem(`hcms.source.data:${path}`)
        localStorage.removeItem(`hcms.source.upstream:${path}`)
      }
    }
    prev_sources = {...sources}
    for(const path of Object.keys(sources)) {
      if (!sources[path]) continue
      const { upstream, data, cb_saved } = sources[path]
      localStorage.setItem(`hcms.source.data:${path}`, data)
      localStorage.setItem(`hcms.source.upstream:${path}`, JSON.stringify(upstream))
      if (cb_saved && cb_saved.length) {
        for(const cb of cb_saved) {
          if(cb) cb(path)
        }
        cb_saved.length = 0
      }
    }
  })
}
